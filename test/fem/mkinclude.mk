# Include file for the top level Makefile

# Prefix of this test
T_PREF :=FEM

# Note: it is important to write explicitly the path relative to the
# including Makefile, bacause that is where these variables will be
# used.
PWD :=fem
$(T_PREF)_DIR :=./$(PWD)

# Test executable
$(T_PREF)_TEXE := fem.test

# Build directory for this test
$(T_PREF)_TBUILDDIR := $(TBUILDDIR)/fem

# Build directory for the tested modules (needed for the .mod files)
TedBUILDDIR := $(TBUILDDIR)/../fem
$(T_PREF)_TedBUILDDIR := $(TedBUILDDIR)

# Object files for this test
TSRC_PF  := $(notdir $(wildcard $(PWD)/*.pf))
TSRC_F90 := $(TSRC_PF:.pf=.F90)
TOBJ     := $(TSRC_F90:.F90=.o)
$(T_PREF)_TF90 := $(TSRC_F90)
$(T_PREF)_TOBJ := $(TOBJ)

# FEMilaro libraries
$(T_PREF)_FMLLIBS := -lfem -lfml_general_utilities

# FEMilaro objects (not included in _FMLLIBS )
$(T_PREF)_FMLOBJS := 

# Dependece on other tests
$(T_PREF)_OTHERTESTINC := -I../$(FML_GENERAL_UTILITIES_DIR)
$(T_PREF)_OTHERTESTOBJ := $(patsubst %, ../$(FML_GENERAL_UTILITIES_DIR)/%, $(FML_GENERAL_UTILITIES_TOBJ))

# Accumulate the general lists
T_PREFS += $(T_PREF)
T_EXES += $$($(T_PREF)_TEXE)

# List here the dependencies among test modules.

mod_h2d_master_el_tests.o: \
  mod_sympoly_tests.o

mod_h2d_coefficients_tests.o: \
  mod_h2d_master_el_tests.o

mod_h2d_cgdofs_tests.o: \
  mod_h2d_master_el_tests.o

# List here some target specfic rules
