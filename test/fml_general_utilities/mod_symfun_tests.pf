module mod_symfun_tests

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_constructor, &
   mod_messages_destructor

 use mod_kinds, only: &
   mod_kinds_constructor, &
   mod_kinds_destructor, &
   wp

 use mod_symfun, only: &
   mod_symfun_constructor, &
   mod_symfun_destructor,  &
   c_symfun, t_funcont, &
   assignment(=),       &
   fnpack, snpack

 use pfunit_mod

!-----------------------------------------------------------------------

 implicit none

!-----------------------------------------------------------------------

 public

!-----------------------------------------------------------------------

 ! Define here the mock type to simulate a c_symfun object.
 type, extends(c_symfun) :: t_funmock
  logical ::    add_called = .false.
  logical ::   mult_called = .false.
  logical :: mult_s_called = .false.
  integer ::   pderj_calls = 0
 contains
  private
  ! from c_symfun:
  !   generic, public :: operator(+) => add
  !   generic, public :: operator(*) => mult, mult_s
  !   generic, public :: ev    => ev_scal, ev_1d
  !   generic, public :: pderj => pderj_1, pderj_k
  procedure, public, pass(f) :: show => funmock_show
  ! ifort bug: add should be private, see
  ! https://software.intel.com/en-us/forums/topic/563741
  procedure, public, pass(x) :: add    => funmock_add
  procedure, public, pass(x) :: mult   => funmock_mult
  procedure, public, pass(x) :: mult_s => funmock_mult_s
  procedure, pass(f) :: ev_scal => funmock_ev_scal
  procedure, pass(f) :: ev_1d   => funmock_ev_1d
  procedure, pass(f) :: pderj_1 => funmock_pderj_1
 end type t_funmock

 type, extends(t_funmock) :: t_funmock2
 contains
  private
  procedure, public, pass(x) :: add  => funmock2_add
  procedure, public, pass(x) :: mult => funmock2_mult
 end type t_funmock2

 type, extends(t_funmock) :: t_funmock3
 contains
  private
  procedure, public, pass(x) :: add  => funmock3_add
  procedure, public, pass(x) :: mult => funmock3_mult
 end type t_funmock3
 
 character(len=*), parameter :: &
   this_mod_name = 'mod_symfun_tests'

 real(wp), parameter :: toll = 10.0_wp*epsilon(1.0_wp)

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 @Before
 subroutine mod_symfun_tests_setup()
  character(len=*), parameter :: &
    this_sub_name = 'setup'
 
   call mod_messages_constructor()
   call mod_kinds_constructor()
   call mod_symfun_constructor()

 end subroutine mod_symfun_tests_setup

!-----------------------------------------------------------------------

 @After
 subroutine mod_symfun_tests_teardown()
  character(len=*), parameter :: &
    this_sub_name = 'tear-down'
 
   call mod_symfun_destructor()
   call mod_kinds_destructor()
   call mod_messages_destructor()
 
 end subroutine mod_symfun_tests_teardown

!-----------------------------------------------------------------------

 @Test
 subroutine assign_snpack__valid_input__works_non_recursively()

  type(t_funcont) :: res
  type(t_funmock) :: var

   res = var

   if(.not.allocated(res%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( f => res%f )
      type is(t_funmock)
       ! OK
      class default
       @assertTrue( .false. , 'Wrong result type for res%f.' )
     end select
   endif

 end subroutine assign_snpack__valid_input__works_non_recursively

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine assign_snpack__valid_input__works_recursively()

  type(t_funcont) :: res, wrap
  type(t_funmock) :: var

   wrap = var
   res = wrap

   if(.not.allocated(res%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( f => res%f )
      type is(t_funmock)
       ! OK
      class default
       @assertTrue( .false. , 'Wrong result type for res%f.' )
     end select
   endif

 end subroutine assign_snpack__valid_input__works_recursively

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine fnpack__valid_input__works()

  type(t_funcont) :: res
  type(t_funmock) :: var

   res = fnpack(var)

   if(.not.allocated(res%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( f => res%f )
      type is(t_funmock)
       ! OK
      class default
       @assertTrue( .false. , 'Wrong result type for res%f.' )
     end select
   endif

 end subroutine fnpack__valid_input__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine add__valid_homogeneous_input__works()

  ! This case does not require any type conversion

  type(t_funmock) :: x, y
  type(t_funcont) :: res

   res = x + y

   if(.not.allocated(res%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( f => res%f )
      type is(t_funmock)
       @assertTrue( f%add_called , 'Method add not called.' )
      class default
       @assertTrue( .false. , 'Wrong result type for res%f.' )
     end select
   endif

 end subroutine add__valid_homogeneous_input__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine add__valid_input_funmock_funcont__works()

  ! Here, y must be unpacked

  type(t_funmock) :: x, y
  type(t_funcont) :: yw, res

   yw = y

   !res = x + yw
   res = x%add( yw )

   if(.not.allocated(res%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( f => res%f )
      type is(t_funmock)
       @assertTrue( f%add_called , 'Method add not called.' )
      class default
       @assertTrue( .false. , 'Wrong result type for res%f.' )
     end select
   endif

 end subroutine add__valid_input_funmock_funcont__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine add__valid_input_funcont_funmock__works()

  ! Here, x must be unpacked, which implies going through the add
  ! method of t_funcont

  type(t_funmock) :: x, y
  type(t_funcont) :: yw, res

   yw = y

   !res = yw + x
   res = yw%add( x )

   if(.not.allocated(res%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( f => res%f )
      type is(t_funmock)
       @assertTrue( f%add_called , 'Method add not called.' )
      class default
       @assertTrue( .false. , 'Wrong result type for res%f.' )
     end select
   endif

 end subroutine add__valid_input_funcont_funmock__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine add__valid_input_funcont_funcont__works()

  ! Here, both terms require unpacking

  type(t_funmock) :: x
  type(t_funcont) :: xw, yw, res

   xw = x                       ! one level packaging
   allocate( yw%f , source=xw ) ! two levels packaging

   !res = xw + yw
   res = xw%add( yw )

   if(.not.allocated(res%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( f => res%f )
      type is(t_funmock)
       @assertTrue( f%add_called , 'Method add not called.' )
      class default
       @assertTrue( .false. , 'Wrong result type for res%f.' )
     end select
   endif

 end subroutine add__valid_input_funcont_funcont__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine add__valid_input_funmock_funmock2__works()

  ! Here, the first call to add does not know how to handle the
  ! t_funmock2 object; a permutation of the terms gives the expected
  ! result.

  type(t_funmock) :: x
  type(t_funmock2) :: y
  type(t_funcont) :: res

   !res = x + y
   res = x%add( y )

   if(.not.allocated(res%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( f => res%f )
      type is(t_funmock2)
       @assertTrue( f%add_called , 'Method add not called.' )
      class default
       @assertTrue( .false. , 'Wrong result type for res%f.' )
     end select
   endif

 end subroutine add__valid_input_funmock_funmock2__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine add__valid_input_funmock_funmock3__error()

  ! Here, the two types t_funmock and t_funmock3 are not compatible:
  ! after trying a permutation of the terms, and error is returned.

  type(t_funmock) :: x
  type(t_funmock3) :: y
  type(t_funcont) :: res

   !res = x + y
   res = x%add( y )

   if(.not.allocated(res%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( f => res%f )
      type is(t_funmock3)
       @assertEqual( 1 , f%ierr , 'Error flag not set.' )
      class default
       @assertTrue( .false. , 'Wrong result type for res%f.' )
     end select
   endif

 end subroutine add__valid_input_funmock_funmock3__error

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 ! Not testing mult since its implementation is identical to add

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine ev_scal__valid_input__works()

  type(t_funmock) :: x
  type(t_funcont) :: x_cnt

   x_cnt = x

   @assertEqual( 1.75_wp , x_cnt%ev( (/ 1.5_wp , 0.25_wp /) ) , toll )

 end subroutine ev_scal__valid_input__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine ev_1d__valid_input__works()

  type(t_funmock) :: x
  type(t_funcont) :: x_cnt
  real(wp) :: res(1)

   x_cnt = x
   res =  x_cnt%ev( reshape( (/ 1.5_wp , 0.25_wp /) , (/2,1/) ) )
   
   @assertEqual( 1.75_wp , res(1) , toll )

 end subroutine ev_1d__valid_input__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine pderj_1__valid_input__works()

  type(t_funmock) :: f
  type(t_funcont) :: df

   df = f%pderj(1)

   if(.not.allocated(df%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( f => df%f )
      type is(t_funmock)
       @assertEqual( 1 , f%pderj_calls ,'Wrong number of derivations.')
      class default
       @assertTrue( .false. , 'Wrong result type for df%f.' )
     end select
   endif

 end subroutine pderj_1__valid_input__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine pderj_k__valid_input__works()

  type(t_funmock) :: f
  type(t_funcont) :: df

   df = f%pderj(1, k=3 )

   if(.not.allocated(df%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( f => df%f )
      type is(t_funmock)
       @assertEqual( 3 , f%pderj_calls ,'Wrong number of derivations.')
      class default
       @assertTrue( .false. , 'Wrong result type for df%f.' )
     end select
   endif

 end subroutine pderj_k__valid_input__works

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 @Test
 subroutine pderj_1__valid_t_funcont_input__works()

  ! This test is similar to pderj_1__valid_input__works but here we
  ! call the method of a t_funcont object containing the mock object.

  type(t_funmock) :: f
  type(t_funcont) :: wrapf, df

   wrapf = f
   df = wrapf%pderj(1)

   if(.not.allocated(df%f)) then
     @assertTrue( .false. , 'Field f not allocated.' )
   else
     select type( f => df%f )
      type is(t_funmock)
       @assertEqual( 1 , f%pderj_calls ,'Wrong number of derivations.')
      class default
       @assertTrue( .false. , 'Wrong result type for df%f.' )
     end select
   endif

 end subroutine pderj_1__valid_t_funcont_input__works

!-----------------------------------------------------------------------

 ! Implementation of the mock methods

 elemental function funmock_add(x,y) result(z)
  class(t_funmock), intent(in) :: x
  class(c_symfun),  intent(in) :: y
  type(t_funcont) :: z

  type(t_funcont) :: ynp
  type(t_funmock) :: tmp

   ! t_funmock is aware only of itself and the generic t_funcont
   ! class; for any other method either exchanging the terms works or
   ! there is an error condition.
   select type(y)
    type is(t_funmock) ! OK
     tmp%add_called = .true.
     z = tmp
    type is(t_funcont) ! extract the contained object and retry
     z = x + y%f
    class default
     if(x%try_permutation) then
       ynp = y
       ynp%f%try_permutation = .false.
       !z = ynp + x
       z = ynp%add( x )
     else
       allocate( z%f , mold=x )
       call z%f%set_err(1) ! unknown type
     endif
   end select

 end function funmock_add
 
 elemental function funmock_mult(x,y) result(z)
  class(t_funmock), intent(in) :: x
  class(c_symfun),  intent(in) :: y
  type(t_funcont) :: z

  type(t_funcont) :: ynp
  type(t_funmock) :: tmp

   ! t_funmock is aware only of itself and the generic t_funcont
   ! class; for any other method either exchanging the terms works or
   ! there is an error condition.
   select type(y)
    type is(t_funmock) ! OK
     tmp%mult_called = .true.
     z = tmp
    type is(t_funcont) ! extract the contained object and retry
     z = x * y%f
    class default
     if(x%try_permutation) then
       ynp = y
       ynp%f%try_permutation = .false.
       !z = ynp * x
       z = ynp%mult( x )
     else
       allocate( z%f , mold=x )
       call z%f%set_err(1) ! unknown type
     endif
   end select

 end function funmock_mult
 
 elemental function funmock_mult_s(r,x) result(y)
  real(wp),         intent(in) :: r
  class(t_funmock), intent(in) :: x
  type(t_funcont) :: y

  type(t_funmock) :: tmp

   tmp%mult_s_called = .true.
   y = tmp

 end function funmock_mult_s

!-----------------------------------------------------------------------

 pure recursive function funmock_ev_scal(f,x) result(z)
  class(t_funmock), intent(in) :: f
  real(wp), intent(in) :: x(:)
  real(wp) :: z

   z = sum(x)
 
 end function funmock_ev_scal
 
!-----------------------------------------------------------------------

 pure recursive function funmock_ev_1d(f,x) result(z)
  class(t_funmock), intent(in) :: f
  real(wp), intent(in) :: x(:,:)
  real(wp) :: z(size(x,2))

   z = sum(x,dim=1)
   
 end function funmock_ev_1d
 
!-----------------------------------------------------------------------

 elemental function funmock_pderj_1(f,j) result(djf)
  class(t_funmock), intent(in) :: f
  integer,          intent(in) :: j
  type(t_funcont) :: djf

  type(t_funmock) :: tmp

   tmp%pderj_calls = f%pderj_calls + 1
   djf = tmp

 end function funmock_pderj_1

!-----------------------------------------------------------------------

 subroutine funmock_show(f)
  class(t_funmock), intent(in) :: f

   write(*,*) "t_funmock object in mod_symfun_tests:"
   write(*,*) "    add_called = " ,   f%add_called
   write(*,*) "   mult_called = " ,  f%mult_called
   write(*,*) " mult_s_called = " ,f%mult_s_called
   write(*,*) "   pderj_calls = " ,  f%pderj_calls

 end subroutine funmock_show

!-----------------------------------------------------------------------

 elemental function funmock2_add(x,y) result(z)
  class(t_funmock2), intent(in) :: x
  class(c_symfun),  intent(in) :: y
  type(t_funcont) :: z

  type(t_funcont) :: ynp
  type(t_funmock2) :: tmp

   ! As in funmock_add, with an additional case for t_funmock2
   select type(y)
    type is(t_funmock)  ! OK
     tmp%add_called = .true.
     z = tmp
    type is(t_funmock2) ! OK
     tmp%add_called = .true.
     z = tmp
    type is(t_funcont) ! extract the contained object and retry
     z = x + y%f
    class default
     if(x%try_permutation) then
       ynp = y
       ynp%f%try_permutation = .false.
       !z = ynp + x
       z = ynp%add( x )
     else
       allocate( t_funmock2 :: z%f )
       call z%f%set_err(1) ! unknown type
     endif
   end select

 end function funmock2_add

 elemental function funmock2_mult(x,y) result(z)
  class(t_funmock2), intent(in) :: x
  class(c_symfun),  intent(in) :: y
  type(t_funcont) :: z

  type(t_funcont) :: ynp
  type(t_funmock2) :: tmp

   select type(y)
    type is(t_funmock)  ! OK
     tmp%mult_called = .true.
     z = tmp
    type is(t_funmock2) ! OK
     tmp%mult_called = .true.
     z = tmp
    type is(t_funcont) ! extract the contained object and retry
     z = x * y%f
    class default
     if(x%try_permutation) then
       ynp = y
       ynp%f%try_permutation = .false.
       !z = ynp * x
       z = ynp%mult( x )
     else
       allocate( t_funmock2 :: z%f )
       call z%f%set_err(1) ! unknown type
     endif
   end select

 end function funmock2_mult

!-----------------------------------------------------------------------

 elemental function funmock3_add(x,y) result(z)
  class(t_funmock3), intent(in) :: x
  class(c_symfun),  intent(in) :: y
  type(t_funcont) :: z

  type(t_funcont) :: ynp
  type(t_funmock3) :: tmp

   ! As in funmock_add, with an additional case for t_funmock2
   select type(y)
    type is(t_funmock3) ! OK
     tmp%add_called = .true.
     z = tmp
    type is(t_funcont) ! extract the contained object and retry
     z = x + y%f
    class default
     if(x%try_permutation) then
       ynp = y
       ynp%f%try_permutation = .false.
       !z = ynp + x
       z = ynp%add( x )
     else
       allocate( t_funmock3 :: z%f )
       call z%f%set_err(1) ! unknown type
     endif
   end select

 end function funmock3_add

 elemental function funmock3_mult(x,y) result(z)
  class(t_funmock3), intent(in) :: x
  class(c_symfun),  intent(in) :: y
  type(t_funcont) :: z

  type(t_funcont) :: ynp
  type(t_funmock3) :: tmp

   select type(y)
    type is(t_funmock3) ! OK
     tmp%mult_called = .true.
     z = tmp
    type is(t_funcont) ! extract the contained object and retry
     z = x * y%f
    class default
     if(x%try_permutation) then
       ynp = y
       ynp%f%try_permutation = .false.
       !z = ynp * x
       z = ynp%mult( x )
     else
       allocate( t_funmock3 :: z%f )
       call z%f%set_err(1) ! unknown type
     endif
   end select

 end function funmock3_mult

!-----------------------------------------------------------------------

end module mod_symfun_tests

