module mod_orbits
!General comments: describes triagular orbits
!
! Orbits are as follows:
! class 1
!  center of the triangle
! class 2
!  axis y=0, parameter: x
! class 3
!  generic point, parameters: x,y
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_orbits_constructor, &
   mod_orbits_destructor,  &
   mod_orbits_initialized, &
   t_torbit,    &
   dimorbits,   &
   orbits,      &
   dimxstatus,  &
   xstatus,     &
   t_xsleg,     &
   xstatus_leg, &
   orbit_xy,    &
   orbit_dxdl,  &
   orbit_dydl,  &
   collect_xyw

 private

!-----------------------------------------------------------------------

! Module types and parameters

 ! public members
 integer, parameter :: &
   wrong_class = 1

 type t_torbit
   integer :: ierr = 0
   integer :: class ! possible values 1,2,3
   integer :: m     ! multiplicity: depends on class
   integer :: npar  ! number of dofs
   real(wp), pointer :: l(:) => null() ! dofs
   real(wp), pointer :: w => null()
 end type t_torbit

 type t_xsleg
   integer :: jo ! orbit index
   character(len=2) :: ptype
 end type t_xsleg

! Module variables

 ! public members
 integer :: dimxstatus, dimorbits
 real(wp), allocatable, target :: xstatus(:)
 type(t_xsleg), allocatable :: xstatus_leg(:)
 type(t_torbit), allocatable, protected :: orbits(:)

 logical, protected ::               &
   mod_orbits_initialized = .false.

 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_orbits'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_orbits_constructor(nc1,nc2,nc3,w,x,y)
  integer, intent(in) :: nc1, nc2, nc3
  real(wp), intent(in) ::  w(:), x(:), y(:)

  integer :: i, ishift, sshift, ss
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
       (mod_kinds_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_orbits_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   ! 1) allocate the xstatus
   dimxstatus = 1*nc1+2*nc2+3*nc3
   dimorbits = nc1+nc2+nc3
   allocate(xstatus(dimxstatus))
   allocate(xstatus_leg(dimxstatus))
   allocate(orbits(dimorbits))

   ! 2) construct the orbits
   do i=1,nc1
     call new_orbit(orbits(i),1,x(i),y(i),xstatus(i),w(i))
     xstatus_leg(i)%jo = i
     xstatus_leg(i)%ptype = 'w'
   enddo

   ishift = nc1
   sshift = nc1
   do i=1,nc2
     ss = sshift + 2*(i-1)
     call new_orbit(orbits(ishift+i),2,x(ishift+i),y(ishift+i), &
                    xstatus(ss+1),w(ishift+i),xstatus(ss+2:ss+2))
     xstatus_leg(ss+1)%jo = ishift+i
     xstatus_leg(ss+1)%ptype = 'w'
     xstatus_leg(ss+2)%jo = ishift+i
     xstatus_leg(ss+2)%ptype = 'lx'
   enddo

   ishift = ishift + nc2
   sshift = sshift + 2*nc2
   do i=1,nc3
     ss = sshift + 3*(i-1)
     call new_orbit(orbits(ishift+i),3,x(ishift+i),y(ishift+i), &
                    xstatus(ss+1),w(ishift+i),xstatus(ss+2:ss+3))
     xstatus_leg(ss+1)%jo = ishift+i
     xstatus_leg(ss+1)%ptype = 'w'
     xstatus_leg(ss+2)%jo = ishift+i
     xstatus_leg(ss+2)%ptype = 'lx'
     xstatus_leg(ss+3)%jo = ishift+i
     xstatus_leg(ss+3)%ptype = 'ly'
   enddo

   mod_orbits_initialized = .true.
 end subroutine mod_orbits_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_orbits_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_orbits_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   deallocate(orbits)
   deallocate(xstatus)
   deallocate(xstatus_leg)

   mod_orbits_initialized = .false.
 end subroutine mod_orbits_destructor

!-----------------------------------------------------------------------
 
 pure function orbit_xy(o) result(xy)
 ! exctract the coordinates of a single point of the orbit
  real(wp) :: xy(2)
  type(t_torbit), intent(in) :: o
 
  character(len=*), parameter :: &
    this_fun_name = 'orbit_xy'

   select case(o%class)
    case(1)
     xy = 0.0_wp
    case(2)
     xy(1) = o%l(1)
     xy(2) = 0.0_wp
    case(3)
     xy = o%l
  end select
 
 end function orbit_xy
 
!-----------------------------------------------------------------------
 
 pure function orbit_dxdl(xsleg) result(dxdl)
 ! compute dx/dl for a given parameter
  real(wp) :: dxdl
  type(t_xsleg), intent(in) :: xsleg
 
  character(len=*), parameter :: &
    this_fun_name = 'orbit_dxdl'

   if(xsleg%ptype(2:2).eq.'x') then
     dxdl = 1.0_wp
   else
     dxdl = 0.0_wp
   endif
 
 end function orbit_dxdl
 
!-----------------------------------------------------------------------
 
 pure function orbit_dydl(xsleg) result(dydl)
 ! compute dy/dl for a given parameter
  real(wp) :: dydl
  type(t_xsleg), intent(in) :: xsleg
 
  character(len=*), parameter :: &
    this_fun_name = 'orbit_dydl'

   if(xsleg%ptype(2:2).eq.'y') then
     dydl = 1.0_wp
   else
     dydl = 0.0_wp
   endif
 
 end function orbit_dydl
 
!-----------------------------------------------------------------------
 
 pure subroutine collect_xyw(x,y,w)
 ! extract the coordinates and the weigts of the quadrature nodes,
 ! considering the multiplicity of each orbit
  real(wp), intent(out), allocatable :: x(:), y(:), w(:)
 
  integer :: npoints, xyw_shift, i
  real(wp) :: xy(2)
  real(wp), parameter :: &
    sin120 = sqrt(real(3,wp))/2.0_wp, &
    cos120 = -0.5_wp
  real(wp), parameter :: &
    rot120(2,2) = reshape( (/ cos120 , -sin120 ,&
                              sin120 ,  cos120 /) , (/2,2/) )
  character(len=*), parameter :: &
    this_sub_name = 'collect_xyw'

   ! 1) allocate the output vectors according to the multiplicity of
   ! the various orbits
   npoints = sum(orbits%m)
   allocate(x(npoints))
   allocate(y(npoints))
   allocate(w(npoints))

   ! 2) collect the orbits
   xyw_shift = 0
   do i=1,size(orbits)
     select case(orbits(i)%class)
      case(1)
       x(xyw_shift+1) = 0.0_wp
       y(xyw_shift+1) = 0.0_wp
       w(xyw_shift+1) = orbits(i)%w
      case(2)
       ! first point
       x(xyw_shift+1) = orbits(i)%l(1)
       y(xyw_shift+1) = 0.0_wp
       w(xyw_shift+1) = orbits(i)%w
       ! second point
       x(xyw_shift+2) =  cos120*orbits(i)%l(1)
       y(xyw_shift+2) =  sin120*orbits(i)%l(1)
       w(xyw_shift+2) = orbits(i)%w
       ! third point
       x(xyw_shift+3) =  cos120*orbits(i)%l(1)
       y(xyw_shift+3) = -sin120*orbits(i)%l(1)
       w(xyw_shift+3) = orbits(i)%w
      case(3)
       ! first point
       x(xyw_shift+1) = orbits(i)%l(1)
       y(xyw_shift+1) = orbits(i)%l(2)
       w(xyw_shift+1) = orbits(i)%w
       ! second point
       xy = matmul(rot120,orbits(i)%l)
       x(xyw_shift+2) = xy(1)
       y(xyw_shift+2) = xy(2)
       w(xyw_shift+2) = orbits(i)%w
       ! third point
       xy = matmul(rot120,xy)
       x(xyw_shift+3) = xy(1)
       y(xyw_shift+3) = xy(2)
       w(xyw_shift+3) = orbits(i)%w
       ! fourth point
       x(xyw_shift+4) =  orbits(i)%l(1)
       y(xyw_shift+4) = -orbits(i)%l(2)
       w(xyw_shift+4) = orbits(i)%w
       ! fifth point
       xy(1) = x(xyw_shift+4)
       xy(2) = y(xyw_shift+4)
       xy = matmul(rot120,xy)
       x(xyw_shift+5) = xy(1)
       y(xyw_shift+5) = xy(2)
       w(xyw_shift+5) = orbits(i)%w
       ! sixth point
       xy = matmul(rot120,xy)
       x(xyw_shift+6) = xy(1)
       y(xyw_shift+6) = xy(2)
       w(xyw_shift+6) = orbits(i)%w
     end select
     xyw_shift = xyw_shift+orbits(i)%m
   enddo
 
 end subroutine collect_xyw
 
!-----------------------------------------------------------------------
 
 subroutine new_orbit(o,class,x,y,w,ww,l)
 ! create a new orbit of class class with point in x,y
  type(t_torbit), intent(out) :: o
  integer, intent(in) :: class
  real(wp), intent(in) :: x,y
  real(wp), intent(in) :: ww
  real(wp), intent(in), target :: w
  real(wp), intent(in), target, optional :: l(:)
 
  character(len=*), parameter :: &
    this_sub_name = 'new_orbit'

   o%class = class
   select case(o%class)
    case(1)
     o%m = 1
     o%npar = 0
     o%l => null()
     o%w => w
     o%w = ww
    case(2)
     o%m = 3
     o%npar = 1
     o%l => l
     o%l(1) = x
     o%w => w
     o%w = ww
    case(3)
     o%m = 6
     o%npar = 2
     o%l => l
     o%l(1) = x
     o%l(2) = y
     o%w => w
     o%w = ww
    case default
     o%ierr = wrong_class
   end select
 
 end subroutine new_orbit
 
!-----------------------------------------------------------------------
 
 pure function jac_dx(dx)
 ! Compute JF*Dx
  real(wp), intent(in) :: dx(:)
  real(wp) :: jac_dx(size(dx))
 
  character(len=*), parameter :: &
    this_fun_name = 'jac_dx'

   
   ! 2) questo jacobiano deve essere spostato in un modulo a parte che
   ! vede sia l'attuale mod_orbits che mod_pbasis
   ! 3) newtong lavora sullo stato del modulo mod_orbits, reso
   ! visibile attraverso il modulo jacobiano (dove viene spostata
   ! questa funzione)
   ! 6) i moduli dove si accede allo stato direttamente devono essere
   ! solo due: mod_orbits (che lo contiene) e mod_jacob, che lo usa
   jac_dx = xstatus
 
 end function jac_dx

!-----------------------------------------------------------------------
 
end module mod_orbits

