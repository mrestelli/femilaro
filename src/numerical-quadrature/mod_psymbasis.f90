module mod_psymbasis
!General comments: symbolic polynomial basis for the computation of
!the quadrature nodes.
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_sympoly, only: &
   mod_sympoly_initialized, &
   t_sympol,        &
   sympoly,         &
   operator(+),     &
   operator(-),     &
   operator(*),     &
   operator(**),    &
   red_sympoly,     &
   refel_int,       &
   pderj,           &
   show

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_psymbasis_constructor, &
   mod_psymbasis_destructor,  &
   mod_psymbasis_initialized, &
   dimbasis, &
   basis,    &
   dxbasis,  &
   dybasis,  &
   intbasis

 private

!-----------------------------------------------------------------------

! Module types and parameters

! Module variables

 ! public members
 logical, protected ::               &
   mod_psymbasis_initialized = .false.
 integer, protected ::               &
   dimbasis
 real(wp), allocatable, protected :: &
   intbasis(:)
 type(t_sympol), allocatable, protected :: &
   basis(:), dxbasis(:), dybasis(:)
 ! private members
 real(wp) :: pas_pb1(3,3), & ! x^2 + y^2 (matrix form)
             pas_pb2(4,4)    ! x^3 - 2xy^2
 type(t_sympol), save :: pb1, pb2  ! symbolic form
 character(len=100) :: message
 character(len=*), parameter :: &
   this_mod_name = 'mod_psymbasis'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_psymbasis_constructor(order)
  integer, intent(in) :: &
    order  ! order of the quadrature formula

  integer :: m, n, i, j
  type(t_sympol) :: ptemp
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
       (mod_kinds_initialized.eqv..false.)    .or. &
       (mod_sympoly_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_psymbasis_initialized.eqv..true.) then
   endif
   !----------------------------------------------

   ! 1) count the basis functions
   dimbasis = 0
   m = 0
   mdo1 : do
     n = 0
     ndo1 : do
       if((2*m+3*n).gt.order) exit ndo1
       dimbasis = dimbasis+1
       n = n+1
     enddo ndo1
     m = m+1
     if((2*m).gt.order) exit mdo1
   enddo mdo1
   allocate(basis(dimbasis))
   allocate(dxbasis(dimbasis))
   allocate(dybasis(dimbasis))
   allocate(intbasis(dimbasis))

   ! 2) compute the basis functions
   pas_pb1 = 0.0_wp
   pas_pb1(3,1) = 1.0_wp
   pas_pb1(1,3) = 1.0_wp
   pb1 = sympoly(pas_pb1)
   pas_pb2 = 0.0_wp
   pas_pb2(4,1) =  1.0_wp
   pas_pb2(2,3) = -3.0_wp
   pb2 = sympoly(pas_pb2)
   i = 0
   m = 0
   mdo2 : do
     n = 0
     ndo2 : do
       if((2*m+3*n).gt.order) exit ndo2
       i = i+1
       basis(i) = red_sympoly((pb1**m)*(pb2**n))
       n = n+1
     enddo ndo2
     m = m+1
     if((2*m).gt.order) exit mdo2
   enddo mdo2

   ! 3) sort the basis from low to high order
   do i=1,dimbasis
     do j=i+1,dimbasis
       if(basis(i)%deg.gt.basis(j)%deg) then
         ptemp = basis(j)
         basis(j) = basis(i)
         basis(i) = ptemp
       endif
     enddo
   enddo

   ! 4) orthonormalization
   do i=1,dimbasis
     do j=1,i-1
       basis(i) = basis(i) - refel_int(basis(i)*basis(j)) * basis(j)
     enddo
     basis(i) = red_sympoly( &
                sqrt(1.0_wp/refel_int(basis(i)**2)) * basis(i) )
   enddo

   ! 5) derivatives of the basis
   do i=1,dimbasis
     dxbasis(i) = pderj(basis(i),1)
     dybasis(i) = pderj(basis(i),2)
     intbasis(i) = refel_int(basis(i))
   enddo

   write(message,'(A,I3)') &
     'Construced polynomial basis with dimension',dimbasis
   call info(this_sub_name,this_mod_name,message)

   mod_psymbasis_initialized = .true.
 end subroutine mod_psymbasis_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_psymbasis_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_psymbasis_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   deallocate(basis)
   deallocate(dxbasis)
   deallocate(dybasis)
   deallocate(intbasis)

   mod_psymbasis_initialized = .false.
 end subroutine mod_psymbasis_destructor

!-----------------------------------------------------------------------

end module mod_psymbasis

