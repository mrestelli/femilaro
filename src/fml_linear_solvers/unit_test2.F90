!**************************************************************
!  Copyright INRIA
!  Authors : 
!     CALVI project team
!  
!  This code SeLaLib (for Semi-Lagrangian-Library) 
!  is a parallel library for simulating the plasma turbulence 
!  in a tokamak.
!  
!  This software is governed by the CeCILL-B license 
!  under French law and abiding by the rules of distribution 
!  of free software.  You can  use, modify and redistribute 
!  the software under the terms of the CeCILL-B license as 
!  circulated by CEA, CNRS and INRIA at the following URL
!  "http://www.cecill.info". 
!**************************************************************

! Test the linear solvers. This test tries all the linear solvers
! defined in  mod_linsolver  and uses the function
! working_implementation()  to check which solvers are really
! implemented and which ones are simply dummy versions.
!
! We run here a parallel test with two processors.
!
! Compared to the  test_fml_linear_solvers1  test, here the linear
! system solution is returned using the abstract type  c_stv  . This
! form is more general than the one in  test_fml_linear_solvers1  but
! requires defining all the operations required for a c_stv object, so
! that the module  linsolver_test_problem2  turns out to be more
! complicated than the  linsolver_test_problem1  one. More precisely,
! the type  t_x  used for the system unknown is defined in the module
! linsolver_test_problem2  together with the corresponding operations.


!-----------------------------------------------------------------------
! This module defines the liner system: see the comments in
! mod_linsolver as well as in the specific solver modules.
!
! See further "program test_fml_linear_solvers2" for the use of the
! linear solvers.
module linsolver_test_problem2

#include "sll_working_precision.h"

 use sll_collective, only: &
  sll_collective_t, sll_collective_bcast, mpi_sum, &
  sll_collective_allreduce

 use sll_femilaro_intf, only: &
  error, warning, info, &
  t_col, transpose, matmul, &
  c_stv, &
  c_linpb, c_itpb, c_mumpspb, c_pastixpb, c_umfpackpb

 implicit none

 private

 ! see the comment about the include file in  test_fml_linear_solvers2
 public :: i32, f64
 public :: &
   wp, mmmt, rhs, t_x, new_t_x, clear_t_x, &
   t_linpb_mumps, t_linpb_it, t_linpb_umfpack, t_linpb_pastix

 ! Get the kind wp of the real working precision: this is useful in
 ! the definition of the literal constants; notice that this will
 ! provide either  wp=f32  or  wp=f64, accordind to the selected
 ! precision in sll_real
 sll_real :: sll_real_example
 integer, parameter :: wp = kind(sll_real_example)

 type, extends(c_stv) :: t_x
  sll_real, allocatable :: x(:)
  ! the following are private types used by some solvers
  sll_int,  private, allocatable :: l2g(:) ! local to global
  type(sll_collective_t), private, pointer :: mpi_comm
  sll_int,  private :: mpi_id, mpi_nd
  sll_real, private, allocatable :: xloc(:), xglob(:)
  logical,  private, allocatable :: mask_duplicated(:)
 contains
  procedure, pass(x) :: incr => x_incr
  procedure, pass(x) :: tims => x_tims
  procedure, pass(z) :: copy => x_copy
  procedure, pass(x) :: scal => x_scal
  !> Compiler bug (see comments in \c mod_state_vars)
  procedure, pass(x) :: source => x_source
  procedure, pass(x) :: source_vect => x_source_vect
 end type t_x

 !> See \c mod_iterativesolvers_base for details.
 type, extends(c_itpb) :: t_linpb_it
 contains
  procedure, nopass :: pres => nop_res
  procedure, nopass :: pkry => a_times_x
 end type t_linpb_it

 !> See \c mod_mumpsintf for details.
 type, extends(c_mumpspb) :: t_linpb_mumps
 contains
  procedure, nopass :: xassign => t_x_xassign
 end type t_linpb_mumps

 !> See \c mod_pastixintf for details.
 type, extends(c_pastixpb) :: t_linpb_pastix
 contains
  procedure, nopass :: xassign => t_x_xassign
 end type t_linpb_pastix

 !> See \c mod_umfintf for details.
 type, extends(c_umfpackpb) :: t_linpb_umfpack
 contains
  procedure, nopass :: xassign => t_x_xassign
 end type t_linpb_umfpack

 type(t_col), save, target :: mmmt
 sll_real, allocatable, target :: rhs(:)

contains

 !> Create a new \c t_x object
 !!
 !! The field \c mask_duplicated is constructed in order to ensure
 !! that each shared entry is kept only on the processor with lower
 !! rank.
 subroutine new_t_x(x,l2g,ng,comm,mpi_id,mpi_nd)
  sll_int, intent(in) :: l2g(:) !< local to global, zero based
  sll_int, intent(in) :: ng     !< global dimension
  type(sll_collective_t), pointer :: comm !< MPI communicator
  sll_int, intent(in) :: mpi_id, mpi_nd !< MPI rank and size
  type(t_x), intent(out) :: x
   
  sll_int :: id, i
  sll_real, allocatable :: ibuf(:) ! there is no integer bcast

   ! local data
   allocate(x%x(size(l2g)))

   ! MPI buffers and data
   allocate(x%l2g  (0:size(l2g)-1)); x%l2g = l2g
   x%mpi_comm => comm
   x%mpi_id   = mpi_id
   x%mpi_nd   = mpi_nd
   allocate(x%xloc (0:ng-1))
   allocate(x%xglob(0:ng-1))

   allocate(x%mask_duplicated(0:size(l2g)-1))
   x%mask_duplicated = .true. ! keep all the elements
   allocate(ibuf(0:ng-1))
   do id=0,x%mpi_nd-2 ! loop on the processors (skip the last one)
     ! processor id sends its elements
     if(x%mpi_id.eq.id) then
       ibuf = -1.0_wp ! invalid value
       ibuf(x%l2g) = 1.0_wp ! proc. id wants to keep these values
     endif
     call sll_collective_bcast( x%mpi_comm , ibuf , int(ng,i32) , &
                                int(id,i32) )
     ! processors with higher ranks mask their own values
     if(x%mpi_id.gt.id) then
       do i=0,size(l2g)-1
         if(int(ibuf(x%l2g(i))).eq.1) x%mask_duplicated( i ) = .false.
       enddo
     endif
   enddo
   deallocate(ibuf)

 end subroutine new_t_x

 subroutine clear_t_x(x)
  type(t_x), intent(inout) :: x

   deallocate(x%x,x%l2g,x%xloc,x%xglob,x%mask_duplicated)
 end subroutine clear_t_x

 subroutine x_incr(x,y)
  class(c_stv), intent(in)    :: y
  class(t_x),   intent(inout) :: x

   select type(y); type is(t_x)
    x%x = x%x + y%x
   end select
 end subroutine x_incr

 subroutine x_tims(x,r)
  sll_real,   intent(in)    :: r
  class(t_x), intent(inout) :: x

   x%x = r*x%x
 end subroutine x_tims

 subroutine x_copy(z,x)
  class(c_stv), intent(in)    :: x
  class(t_x),   intent(inout) :: z

   select type(x); type is(t_x)
    z%x = x%x
   end select
 end subroutine x_copy

 subroutine x_source(y,x)
  class(t_x),                intent(in)  :: x
  class(c_stv), allocatable, intent(out) :: y

   allocate(t_x::y)
   select type(y); type is(t_x)
    allocate(y%x(size(x%x)))
    y%x = x%x
   end select
 end subroutine x_source
 subroutine x_source_vect(y,x,m)
  sll_int, intent(in) :: m
  class(t_x),                intent(in)  :: x
  class(c_stv), allocatable, intent(out) :: y(:)

  sll_int :: i

   allocate(t_x::y(m))
   select type(y); type is(t_x)
    do i=1,m
      allocate(y(i)%x(size(x%x)))
      y(i)%x = x%x
    enddo
   end select
 end subroutine x_source_vect

 subroutine t_x_xassign(x,s,x_vec)
  sll_real,       intent(in) :: x_vec(:)
  class(c_linpb), intent(inout) :: s
  class(c_stv),   intent(inout) :: x

   select type(x); type is(t_x)
    ! In principle, one would need a select case to access the type of
    ! the linear problem s; however, in this simple case it doesn't
    ! matter and we can avoid it.
    x%x = x_vec
   end select
 end subroutine t_x_xassign

 !> Residual (no preconditioning)
 !!
 !! See the comments in \c a_times_x concerning the parallel
 !! implementation.
 subroutine nop_res(r,x)
  class(c_stv), intent(in) :: x
  class(c_stv), intent(inout) :: r

   select type(x); type is(t_x); select type(r); type is(t_x)
    
    ! local residual, assuming that x has the correct values
    r%x = rhs - matmul(x%x,mmmt)

    ! reduction
    r%xloc        = 0.0_wp ! local solution in the send buffer
    r%xloc(r%l2g) = r%x
    call sll_collective_allreduce( r%mpi_comm, r%xloc,        &
             int(size(r%xloc),i32), int(mpi_sum,i32), r%xglob )
    r%x = r%xglob(r%l2g)   ! global solution from the recv buffer

   end select; end select
 end subroutine nop_res

 !> Krylov vector  \f$ r = M^T * x \f$  (no preconditioning)
 !!
 !! Some complications arise because of the parallel execution.
 !!
 !! According to the specifications of mod_iterativesolvers_base, we
 !! need to ensure that duplicated vector elements are copies of the
 !! same value. Here, we assume that this is true for x and we have to
 !! ensure it for r. This can be done as follows:
 !! <ul>
 !!  <li> each processor computes the matrix vector product, using the
 !!  local matrix
 !!  <li> a reduction operation is used to collect all the local
 !!  contribution.
 !! </ul>
 subroutine a_times_x(r,x)
  class(c_stv), intent(in) :: x
  class(c_stv), intent(inout) :: r

   select type(x); type is(t_x); select type(r); type is(t_x)

    ! compute the local matrix-vector product (for serial execution,
    ! this would be enough)
    r%x = matmul(x%x,mmmt)

    ! reduction
    r%xloc        = 0.0_wp ! local solution in the send buffer
    r%xloc(r%l2g) = r%x
    call sll_collective_allreduce( r%mpi_comm, r%xloc,        &
             int(size(r%xloc),i32), int(mpi_sum,i32), r%xglob )
    r%x = r%xglob(r%l2g)   ! global solution from the recv buffer

   end select; end select
 end subroutine a_times_x

 !> Scalar product
 !!
 !! Duplicated values introduce here some complications: to compute
 !! the correct values, one has to skip the duplicated values so that
 !! they are considered only on one processor.
 function x_scal(x,y) result(s)
  class(t_x),   intent(in) :: x
  class(c_stv), intent(in) :: y
  sll_real :: s

  sll_real :: sloc(1), sglob(1)

   select type(y); type is(t_x)
    sloc = sum( x%x*y%x , mask=x%mask_duplicated )
    call sll_collective_allreduce( x%mpi_comm, sloc, 1_i32, &
                                    int(mpi_sum,i32), sglob )
    s = sglob(1)
   end select
 end function x_scal
end module linsolver_test_problem2
!-----------------------------------------------------------------------


!-----------------------------------------------------------------------
! Here is the unit test
program test_fml_linear_solvers2

! In general, one should include this file:
!  #include "sll_working_precision.h"
! however this is the C preprocessor which knowns nothing about
! fortran modules and programs, and since this file has been aready
! included once it would not be included again. For this reason, we
! take the required information from linsolver_test_problem2.
 use linsolver_test_problem2, only: i32, f64

 use sll_collective, only: &
  mpi_thread_multiple, sll_world_collective, &
  sll_collective_barrier, sll_collective_reduce, mpi_land

 use sll_femilaro_intf, only: &
  sll_femilaro_intf_constructor, &
  sll_femilaro_intf_destructor,  &
  error, warning, info, &
  write_octave, read_octave, &
  t_col, transpose, matmul, &
  new_tri, tri2col, clear,  &
  c_stv, &
  c_linpb, gmres
   
 use linsolver_test_problem2, only: &
  wp, mmmt, rhs, t_x, new_t_x, clear_t_x, &
  t_linpb_it, t_linpb_mumps, t_linpb_pastix, t_linpb_umfpack

 implicit none

 character(len=*), parameter :: &
   this_prog_name = 'test_fml_linear_solvers2'
 
 logical :: all_test_passed(1), g_all_test_passed(1)
 sll_int :: mpi_nd, mpi_id
 character(len=1000) :: message

 !> global dimension of the system
 sll_int :: problem_global_size
 !> local to global indexes
 sll_int, allocatable, target :: gij(:)
 !> linear problem
 class(c_linpb), allocatable :: linpb
 !> system unknown
 type(t_x) :: x
 !> exact solution
 sll_real, allocatable :: xex(:)

  !---------------------------------------------------------------------
  ! General setup
  call sll_femilaro_intf_constructor( &
    init_mpi = .true. , & ! includes MPI initialization
    mpi_required_thread_support = mpi_thread_multiple , &
    init_pastix  = .true. ,  &
    init_umfpack = .false. , &
    init_mumps   = .false. )

  mpi_nd = int( sll_world_collective%size ,kind(mpi_nd))
  mpi_id = int( sll_world_collective%rank ,kind(mpi_id))
  if(mpi_nd.ne.2) call error(this_prog_name,"",    &
      'This test should be run with two processors')

  !---------------------------------------------------------------------
  ! Define the linear system
  ! - each process builds its own part of the matrix; the complete
  !  matrix is the sum of the local ones
  ! - the local matrices are transposed: this is good for the
  !  iterative solvers
  problem_global_size = 5
  build_matrix: select case(mpi_id)
   case(0)
    ! local matrix
    ! M0 = [ 1   0   8   3
    !        0   2   0   0
    !        0   1   3   0
    !        0   1   0   4 ]
    mmmt = transpose( tri2col( new_tri(             &
       n = 4, m = 4,                                &
      ti = (/ 1 , 2 , 3 , 4 , 1 , 3 , 1 , 4 /) - 1, & ! zero based indx
      tj = (/ 1 , 2 , 2 , 2 , 3 , 3 , 4 , 4 /) - 1, &
      tx = (/1.0_wp,2.0_wp,1.0_wp,1.0_wp,8.0_wp,3.0_wp,3.0_wp,4.0_wp/) &
                    ) ) )
    ! local to global indexing
    allocate(gij(0:3)); gij = (/ 1 , 2 , 3 , 4 /) - 1 ! zero based
    ! local right-hand-side
    allocate(rhs(4)); rhs = (/ 37.0_wp , 4.0_wp , -28.0_wp , 5.0_wp /)
    allocate(xex(4)); xex = (/ 1.0_wp , 2.0_wp , 3.0_wp , 4.0_wp /)
   case(1)
    ! local matrix
    ! M1 = [ -3   0  -6
    !         0  -2   0
    !         2   0  -1 ]
    mmmt = transpose( tri2col( new_tri(             &
       n = 3, m = 3,                                &
      ti = (/ 1 , 3 , 2 , 1 , 3 /) - 1,             & ! zero based indx
      tj = (/ 1 , 1 , 2 , 3 , 3 /) - 1,             &
      tx = (/-3.0_wp,2.0_wp,-2.0_wp,-6.0_wp,-1.0_wp/) &
                    ) ) )
    ! local to global indexing
    allocate(gij(0:2)); gij = (/ 3 , 4 , 5 /) - 1 ! zero based
    ! local right-hand-side
    allocate(rhs(3)); rhs = (/ 0.0_wp , 5.0_wp , 1.0_wp /)
    allocate(xex(3)); xex = (/ 3.0_wp , 4.0_wp , 5.0_wp /)
  end select build_matrix
  call new_t_x(x, gij,problem_global_size, sll_world_collective, &
               mpi_id,mpi_nd)
  ! The resulting global matrix is
  !
  ! M = [ 1   0   8   3   0
  !       0   2   0   0   0
  !       0   1   0   0  -6
  !       0   1   0   2   0
  !       0   0   2   0  -1 ]
  !
  ! with RHS and solution
  !
  !       [ 37]            [1]
  !       [  4]            [2]
  ! rhs = [-28]        x = [3]
  !       [ 10]            [4]
  !       [  1]            [5]

  !---------------------------------------------------------------------
  ! Test the linear solvers
  ! - for each solver, compute the residual
  all_test_passed = .true.
  call sll_collective_barrier( sll_world_collective )

  !---------------------------------------------------------------------
  ! GMRES solver
  allocate(t_linpb_it::linpb)
  if(linpb%working_implementation()) then
    call sll_collective_barrier( sll_world_collective )
    x%x = 0.0_wp ! initial guess
    select type(linpb); type is(t_linpb_it)
      linpb%abstol    = .true.
      linpb%tolerance = 1.0e-12_wp
      linpb%nmax      = 5  ! size of the Krylov space
      linpb%rmax      = 10 ! restarts
      linpb%solver    => gmres
      linpb%mpi_comm  = int(sll_world_collective%comm)
      linpb%mpi_id    = int(mpi_id)
    end select
    call linpb%factor('analysis')
    call linpb%factor('factorization')
    call linpb%solve(x)
    call linpb%clean()

    write(message,'(a,e23.15)') &
      "Done GMRES; error ", norm2( xex - x%x )
    call info(this_prog_name,"",message)
    if( norm2( xex - x%x ) .gt. 1.0e-8 ) all_test_passed = .false.
    call sll_collective_barrier( sll_world_collective )
  endif
  deallocate(linpb)


  !---------------------------------------------------------------------
  ! MUMPS solver
  allocate(t_linpb_mumps::linpb)
  if(linpb%working_implementation()) then
    call sll_collective_barrier( sll_world_collective )
    x%x = 0.0_wp ! initial guess (not used by MUMPS)
    select type(linpb); type is(t_linpb_mumps)
      linpb%distributed    = .true.
      linpb%poo            = 3 ! SCOTCH
      linpb%transposed_mat = .true.
      linpb%gn             = problem_global_size
      linpb%m              => mmmt
      linpb%rhs            => rhs
      linpb%gij            => gij
      linpb%mpi_comm       = int(sll_world_collective%comm)
    end select
    call linpb%factor('analysis')
    call linpb%factor('factorization')
    call linpb%solve(x)
    call linpb%clean()

    write(message,'(a,e23.15)') &
      "Done MUMPS; error ", norm2( xex - x%x )
    call info(this_prog_name,"",message)
    if( norm2( xex - x%x ) .gt. 1.0e-8 ) all_test_passed = .false.
    call sll_collective_barrier( sll_world_collective )
  endif
  deallocate(linpb)


  !---------------------------------------------------------------------
  ! PaStiX solver
  allocate(t_linpb_pastix::linpb)
  if(linpb%working_implementation()) then
    call sll_collective_barrier( sll_world_collective )
    x%x = 0.0_wp ! initial guess (not used)
    select type(linpb); type is(t_linpb_pastix)
      linpb%transposed_mat = .true.
      linpb%gn             = problem_global_size
      linpb%m              => mmmt
      linpb%rhs            => rhs
      linpb%gij            => gij
      linpb%mpi_comm       = int(sll_world_collective%comm)
    end select
    call linpb%factor('analysis')
    call linpb%factor('factorization')
    call linpb%solve(x)
    call linpb%clean()

    write(message,'(a,e23.15)') &
      "Done PaStiX; error ", norm2( xex - x%x )
    call info(this_prog_name,"",message)
    if( norm2( xex - x%x ) .gt. 1.0e-8 ) all_test_passed = .false.
    call sll_collective_barrier( sll_world_collective )
  endif
  deallocate(linpb)


  !---------------------------------------------------------------------
  ! Clean-up
  deallocate(rhs,gij)
  call clear(mmmt)
  call clear_t_x(x)

  !---------------------------------------------------------------------
  ! Done
  call sll_collective_reduce( sll_world_collective , all_test_passed , &
                 1_i32 , int(mpi_land,i32) , 0_i32 , g_all_test_passed )
  if(mpi_id.eq.0) then
    if(g_all_test_passed(1)) write(*,*) '#PASSED'
  endif

  call sll_femilaro_intf_destructor() ! includes mpi_finalize

end program test_fml_linear_solvers2
!-----------------------------------------------------------------------

