!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!>\brief
!!
!! Simplified interface to MaPHyS.
!!
!! \n
!!
!! This module provides a simplified interface to the <a
!! href="https://project.inria.fr/maphys/">MaPHyS</a> hybrid
!! (direct/iterative) solver, according to the general layout given in
!! \c mod_linsolver_base.
!!
!! Since the MaPHyS interface resembles the MUMPS one, this module is
!! similar to \c mod_mumpsintf.
!!
!! \note All the indexes should be zero-based; the conversion to
!! 1-based indexes is done inside this module and should be invisible
!! to the end user.
!!
!! \note Concerning the two matrix formats, namely the distributed and
!! the centralized ones, see \ref mumps_dist_mat "this section".
!<----------------------------------------------------------------------
module mod_maphysintf

!-----------------------------------------------------------------------

 use mod_utils, only: &
   t_realtime, my_second

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_sparse, only: &
   mod_sparse_initialized, &
   ! sparse types
   t_col, t_tri,&
   col2tri,     &
   clear

 use mod_mpi_utils, only: &
   mod_mpi_utils_initialized, &
   mpi_reduce, wp_mpi, mpi_sum, &
   mpi_bcast

 use mod_state_vars, only: &
   mod_state_vars_initialized, &
   c_stv

 use mod_output_control, only: &
   mod_output_control_initialized, &
   elapsed_format, base_name

 use mod_linsolver_base, only: &
   mod_linsolver_base_initialized, &
   c_linpb

 ! Select here the required precision
 !use smph_maphys_mod, only: &
 !  mph_maphys_t      => smph_maphys_t,     &
 !  mph_maphys_driver => smph_maphys_driver
 use dmph_maphys_mod, only: &
   mph_maphys_t      => dmph_maphys_t,     &
   mph_maphys_driver => dmph_maphys_driver

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_maphysintf_constructor, &
   mod_maphysintf_destructor,  &
   mod_maphysintf_initialized, &
   c_maphyspb

 private

!-----------------------------------------------------------------------

 ! An example variable, used to read the MaPHyS kinds
 type(mph_maphys_t) :: t_dmph_example

 ! MaPHyS double precision
 integer, parameter :: mph_wp = kind( t_dmph_example%values )

 !> Linear MaPHyS solver problem
 !!
 !! This type describes a linear system from the MaPHyS viewpoint.
 type, extends(c_linpb), abstract :: c_maphyspb
  logical :: distributed !< see <tt>icntl(43)</tt>
  !> matrix size
  integer :: gn
  !> system matrix
  !!
  !! This is the global matrix if <tt>distributed.eqv..true.</tt>,
  !! otherwise the local one. In both cases, the indexes are 0-based,
  !! as always with \c t_col objects. If the matrix is distributed,
  !! the field \c gij is used to map local nodes to the global ones.
  type(t_col), pointer :: m
  !> local right-hand side
  real(wp), pointer :: rhs(:)
  !> local to global map, 0-based indexes (as in \c mod_sparse)
  integer, pointer :: gij(:)
  !> solve \f$A^Tx=b\f$
  !!
  !! Notice that MaPHyS does not have an option to solve the
  !! transposed system, so transposition is done when defining the
  !! MaPHyS matrix.
  logical :: transposed_mat = .false.
  !> MPI communicator
  integer :: mpi_comm
  !> proc. rank in \c mpi_comm
  integer :: mpi_id
  !> Write the matrix to a file
  !!
  !! This corresponds to setting <tt>mphs\%WRITE_MATRIX> as discussed
  !! in section 7 of the MaPHyS user guide. When setting this field,
  !! the matrix will be written at the following analysis phase.
  !!
  !! Multiple calls with this field set will overwrite the matrix
  !! file.
  logical :: write_mat = .false.
  !> local to global map, 1-based (<tt>l2g_map = gij+1</tt>)
  integer, allocatable, private :: l2g_map(:)
  !> MaPHyS internal system representation (see
  !! <tt>*mph_maphys_type.F90</tt>)
  type(mph_maphys_t), private :: mph
  logical, private :: sys_set = .false. !< internal consistency check
 contains
  procedure, pass(s) :: factor => maphys_factor
  procedure, pass(s) :: solve  => maphys_solve
  procedure, pass(s) :: clean  => maphys_clean
  procedure, nopass :: working_implementation => maphys_wi
  !> Convert the MaPHyS solution into a \fref{mod_state_vars,c_stv}
  !! object (must be overridden when extending this type).
  procedure, nopass :: xassign => xassign_dummy
 end type c_maphyspb

 real(t_realtime) :: t0, t1
 character(len=1000) :: message
 logical, protected :: &
   mod_maphysintf_initialized = .false.
 character(len=*), parameter :: &
   this_mod_name = 'mod_maphysintf'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_maphysintf_constructor(init_maphys)
  logical, intent(in), optional :: init_maphys

  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
          (mod_kinds_initialized.eqv..false.) .or. &
         (mod_sparse_initialized.eqv..false.) .or. &
      (mod_mpi_utils_initialized.eqv..false.) .or. &
 (mod_output_control_initialized.eqv..false.) .or. &
 (mod_linsolver_base_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_maphysintf_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   ! Notice: MaPHyS does not require any library initialization

   mod_maphysintf_initialized = .true.
 end subroutine mod_maphysintf_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_maphysintf_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_maphysintf_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_maphysintf_initialized = .false.
 end subroutine mod_maphysintf_destructor

!-----------------------------------------------------------------------

  pure function maphys_wi()
   logical :: maphys_wi
    maphys_wi = .true.
  end function maphys_wi

!-----------------------------------------------------------------------

 !> Factor matrix \c m
 subroutine maphys_factor(s,phase)
  class(c_maphyspb), intent(inout) :: s
  character(len=*), intent(in), optional :: phase
 
  integer, pointer :: loc_i(:), loc_j(:)
  type(t_tri), target :: m_tri
  character(len=*), parameter :: dump_file = '.maphys-mat-dump'
  integer :: ierr
  character(len=*), parameter :: &
    this_sub_name = 'maphys_factor'
 
   ! Check whether only factorization is required
   factorization_if: if(present(phase)) then
     if(trim(phase).eq.'factorization') then

       if(.not.s%sys_set) call error(this_sub_name,this_mod_name, &
          'Trying to factorize a matrix which is not yet defined.')

       ! Update the matrix coefficients
       if(s%distributed) then ! all procs
         m_tri = col2tri(s%m)
         s%mph%values = real(m_tri%tx,mph_wp)
         call clear(m_tri)
       else ! only root proc
         if(s%mpi_id.eq.0) then
           m_tri = col2tri(s%m)
           s%mph%values = real(m_tri%tx,mph_wp)
           call clear(m_tri)
         endif
       endif

       t0 = my_second()
       s%mph%job = 2 ! factorization
write(*,*) "Driv", s%mph%job 
       call mph_maphys_driver( s%mph )
       s%mph%job = 3 ! preconditioning
write(*,*) "Driv", s%mph%job 
       call mph_maphys_driver( s%mph )
       t1 = my_second()
       write(message,elapsed_format)                                   &
              'Completed MaPHyS factorization: elapsed time ',t1-t0,'s.'
       call info(this_mod_name,this_sub_name,message)

       return
     endif
   endif factorization_if

   if(s%sys_set) call warning(this_sub_name,this_mod_name, &
       'For this matrix an analysis has already been done!')

   ! Initialize an instance of the package
   if(.not.s%sys_set) then
     s%mph%comm = s%mpi_comm
     call mpi_comm_rank( s%mpi_comm , s%mpi_id , ierr )

     s%mph%job = -1 ! initialization
     t0 = my_second()
write(*,*) "Driv", s%mph%job 
     call mph_maphys_driver( s%mph )
     t1 = my_second()
     write(message,elapsed_format)                                   &
           'Completed MaPHyS initialization: elapsed time ',t1-t0,'s.'
     call info(this_mod_name,this_sub_name,message)
   endif

   ! Set output to stdout: typically unit 6
   s%mph%icntl(1) = 6
   s%mph%icntl(2) = 6
   s%mph%icntl(3) = 6

   ! according to the documentation, write_problem must be set before
   ! the analysis phase
   if(s%write_mat) &
         s%mph%write_matrix = trim(base_name)//dump_file

   distributed_if: if(s%distributed) then

     ! Allocate and set the matrix
     if(.not.s%sys_set) &
       allocate(s%l2g_map( lbound(s%gij,1) : ubound(s%gij,1) ))
     s%l2g_map = s%gij+1

     m_tri = col2tri(s%m)

     s%mph%sym = 0 ! general, unsymmetric
     s%mph%n   = s%gn
     s%mph%nnz = m_tri%nz
     if(.not.s%sys_set) allocate(s%mph%rows(m_tri%nz))
     if(.not.s%sys_set) allocate(s%mph%cols(m_tri%nz))
     ! take into account transposition
     if(.not.s%transposed_mat) then
       loc_i => m_tri%ti
       loc_j => m_tri%tj
     else
       loc_i => m_tri%tj
       loc_j => m_tri%ti
     endif
     s%mph%rows = s%l2g_map(lbound(s%l2g_map,1) + loc_i )
     s%mph%cols = s%l2g_map(lbound(s%l2g_map,1) + loc_j )

     ! the real kind can be different
     if(.not.s%sys_set) allocate(s%mph%values(m_tri%nz))
     s%mph%values = real(m_tri%tx,mph_wp)

     ! Allocate the MaPHyS arrays for rhs and sol; notice that we do
     ! not simply point these pointers to s%rhs and the solution
     ! because their kinds could be different.
     if(.not.s%sys_set) allocate(s%mph%rhs(m_tri%m))
     if(.not.s%sys_set) allocate(s%mph%sol(m_tri%n))

     call clear(m_tri)

     s%mph%icntl(43) = 2 ! distributed matrix
   else
     ! the matrix is defined only on the master process
     if(s%mpi_id.eq.0) then

       m_tri = col2tri(s%m)

       s%mph%sym = 0 ! general, unsymmetric
       s%mph%n   = m_tri%n
       s%mph%nnz = m_tri%nz
       if(.not.s%sys_set) allocate(s%mph%rows(m_tri%nz))
       if(.not.s%sys_set) allocate(s%mph%cols(m_tri%nz))
       ! take into account transposition
       if(.not.s%transposed_mat) then
         loc_i => m_tri%ti
         loc_j => m_tri%tj
       else
         loc_i => m_tri%tj
         loc_j => m_tri%ti
       endif
       s%mph%rows = loc_i+1
       s%mph%cols = loc_j+1

       if(.not.s%sys_set) allocate(s%mph%values(m_tri%nz))
       s%mph%values = real(m_tri%tx,mph_wp)

       if(.not.s%sys_set) allocate(s%mph%rhs(m_tri%m))
       if(.not.s%sys_set) allocate(s%mph%sol(m_tri%n))

       call clear(m_tri)

     endif
     s%mph%icntl(43) = 1 ! centralized matrix
   endif distributed_if

   if(present(phase)) then
     if(trim(phase).eq.'analysis') then
       s%mph%job = 1 ! analysis
     else
     call error(this_sub_name,this_mod_name, &
            'Unknown phase "'//trim(phase)//'".')
     endif
   else
     s%mph%job = 5 ! analysis and factorization
   endif
   t0 = my_second()
write(*,*) "Driv", s%mph%job 
   call mph_maphys_driver( s%mph )
   t1 = my_second()
   write(message,elapsed_format) 'Completed MaPHyS analysis/analysis'//&
                          '-and-factorization: elapsed time ',t1-t0,'s.'
   call info(this_mod_name,this_sub_name,message)
   call write_mat_info() ! print an info message if it is required

   s%sys_set = .true.

 contains

  subroutine write_mat_info()

   ! The default value is ''; if mph%write_matrix differs from this,
   ! MaPHyS dumps the matrix 
   character(len=20), parameter :: &
     maphys_no_write = ''

    if( trim(s%mph%write_matrix) .ne. maphys_no_write ) then
      call mpi_barrier(s%mpi_comm,ierr)
      call info(this_sub_name,this_mod_name,                    &
       'Matrix dumped in file "'//trim(s%mph%write_matrix)//'".')
    endif

  end subroutine write_mat_info

 end subroutine maphys_factor
 
!-----------------------------------------------------------------------
 
 !> Solve the linear system
 subroutine maphys_solve(s,x,x1d)
  class(c_maphyspb), intent(inout) :: s
  class(c_stv),      intent(inout), optional :: x
  real(wp),          intent(inout), optional :: x1d(:)

  character(len=*), parameter :: &
    this_sub_name = 'maphys_solve'

   if(.not.s%sys_set) call error(this_sub_name,this_mod_name, &
   'Trying to solve a system while the matrix is not defined.')

   ! set up the rhs
   if(s%distributed) then
     s%mph%rhs = real( s%rhs , mph_wp )
     s%mph%icntl(23) = 1 ! use the initial guess
     s%mph%sol = 0.0_mph_wp ! ??? real( s%rhs , mph_wp )
   else
     if(s%mpi_id.eq.0) then ! no need for rhs on other procs.
       s%mph%rhs = real( s%rhs , mph_wp )
       s%mph%icntl(23) = 1 ! use the initial guess
       s%mph%sol = 1.0_mph_wp ! ??? real( s%rhs , mph_wp )
     endif
   endif
                                        s%mph%icntl(23) = 1 ! use the initial guess
   
   t0 = my_second()
   s%mph%job = 4 ! solve
write(*,*) "Driv", s%mph%job 
write(*,*) " icntl ",s%mph%icntl
   call mph_maphys_driver( s%mph )
   t1 = my_second()
   write(message,elapsed_format)                           &
          'Completed MaPHyS solve: elapsed time ',t1-t0,'s.'
   call info(this_mod_name,this_sub_name,message)

   if(s%distributed) then
     if(present(x)) call s%xassign(x,s,real(s%mph%sol,wp))
     if(present(x1d)) x1d = real(s%mph%sol,wp)
   else
     call error(this_sub_name,this_mod_name,'To do')
   endif

 end subroutine maphys_solve
 
!-----------------------------------------------------------------------
 
 subroutine maphys_clean(s)
  class(c_maphyspb), intent(inout) :: s

  character(len=*), parameter :: &
    this_sub_name = 'maphys_clean'
 
   if(s%sys_set) then

     s%mph%job = -2 ! clean-up
write(*,*) "Driv", s%mph%job 
     call mph_maphys_driver( s%mph )

     if(s%distributed) then
       deallocate(s%mph%sol)
       deallocate(s%mph%rhs)
       deallocate(s%mph%values)
       deallocate(s%mph%cols)
       deallocate(s%mph%rows)
       deallocate(s%l2g_map)
     else
       if(s%mpi_id.eq.0) then
         deallocate(s%mph%sol)
         deallocate(s%mph%rhs)
         deallocate(s%mph%values)
         deallocate(s%mph%cols)
         deallocate(s%mph%rows)
       endif
     endif

     s%sys_set = .false.
   endif

 end subroutine maphys_clean
 
!-----------------------------------------------------------------------

 !> Convert the MaPHyS solution into a \fref{mod_state_vars,c_stv}
 !! object
 !!
 !! This operation is specific for a given \fref{mod_state_vars,c_stv}
 !! object and can not be defined here. This subroutine is provided
 !! for cases when only the 1D array output is required.
 subroutine xassign_dummy(x,s,x_vec)
  real(wp),       intent(in) :: x_vec(:)
  class(c_linpb), intent(inout) :: s
  class(c_stv),   intent(inout) :: x

  character(len=*), parameter :: this_sub_name = "xassign_dummy"
  character(len=*), parameter :: &
    err_msg(6) = (/ &
   "Please, consider that you have to provide a real implementation ",&
   "for this function if you want to use the c_stv argument of      ",&
   "maphys_solve.                                                   ",&
   "This function in mod_maphysintf is provided only to simplify the",&
   "implementation for cases where only the 1D array argument is    ",&
   "used.                                                           " &
                 /)

   call error(this_sub_name,this_mod_name,err_msg)
 end subroutine xassign_dummy

!-----------------------------------------------------------------------

end module mod_maphysintf

