!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!>\brief
!!
!! RHS for the compressible Navier-Stokes equations, DG elements
!!
!! \n
!!
!! The RHS is computed with two loops: the first one loops on the
!! elements, the second one on the sides. The boundary conditions are
!! enforced in the side loop. The basis is assumed to be orthogonal,
!! so that the mass matrix is diagonal.
!!
!! \section dissfluxes Dissipative fluxes
!! 
!! Turbulent models can be very complex: see \c mod_turb_flux and \c
!! mod_dgcomp_ode for a detailed description of the models considered
!! here. In this module, we deal with two kinds of arrays:
!! <ul>
!!  <li> arrays referring to the prognostic variables: dynamic and
!!  thermodynamic variables and tracers
!!  <li> arrays referring to the diffusive fluxes of the prognostic
!!  variables: all the prognostic variables except density.
!! </ul>
!! The gradients required for the evaluation of the dissipative
!! fluxes are computed in this module but they are defined in \c
!! mod_turb_flux, and depend on the chosen turbulence model.
!!
!! \subsection dissfluxes_details Implementation details
!!
!! The dissipative fluxes are always computed calling
!! <tt>t_dgcomp_ode\%turbmod\%flux</tt>, either directly or
!! indirectly:
!! <ul>
!!  <li> <tt>dgcomp_tens</tt> \f$\mapsto\f$
!!   <ul>
!!    <li> <tt>tens_elem_loop</tt> \f$\mapsto\f$
!!    <tt>turbmod\%flux</tt> for the elements (internal nodes)
!!    <li> <tt>tens_side_loop</tt> \f$\mapsto\f$
!!    <tt>turbmod\%flux</tt> for the internal sides
!!    <li> <tt>bside_send_flux</tt> \f$\mapsto\f$ elemside_viscflux
!!    (which calls in turn <tt>turbmod\%flux</tt>) for the ddc sides 
!!    <li> <tt>update_fluxbcs</tt> \f$\mapsto\f$ elemside_viscflux
!!    (which calls in turn <tt>turbmod\%flux</tt>) for the remaining
!!    boundary sides.
!!   </ul>
!! </ul>
!!
!! \section communications MPI communications
!!
!! The MPI communication buffers and data structures are defined if
!! there is at least one ddc side. This is controlled by the module
!! variable <tt>ddc\%ddc</tt>. The following points are considered in
!! the MPI communications:
!! <ul>
!!  <li> communications are performed only with those subdomains which
!!  share at least one ddc side with the present one (i.e. no
!!  communications with zero data length)
!!  <li> the sent and the received information have the same size and
!!  structure, so that send and receive buffers have the same
!!  dimension
!!  <li> the side quadrature points in the send and receive buffers
!!  are always ordered according to the receiver ordering.
!! </ul>
!!
!! \note All the MPI communications rely on the module variable \c
!! ddc. Each subroutine accessing this variable should be documented,
!! explaining whether it makes a self contained use of it or whether
!! it uses \c ddc to pass or receive information to other subroutines.
!!
!! Concerning the communication associated with the dissipative
!! fluxes, we notice that only the prognostic variables and the
!! dissipative fluxes must be communicated, while the discrete
!! gradients do <em>not</em> require any communication. This happens
!! because the discrete gradients can be evaluated separately on each
!! processor given the values of the prognostic variables.
!! Summarizing, since the communication of the prognostic variables is
!! required in any case by the inviscid model, we see that the viscous
!! model results in the additional communication of the dissipative
!! fluxes.
!!
!! \note This has the interesting consequence that the amount of
!! communication does not depend on the chosen turbulence model, and
!! that one should not worry at all about communications in the
!! implementation of the turbulence models.
!!
!! \section dgcomp_rhs_diagnostics Diagnostics
!!
!! As a general rule, the diagnostics are computed in the same
!! functions where the corresponding terms are computed. This allows
!! for a consistent evaluation of the diagnostics compared to the
!! dynamics. In particular, all the diagnostics should be obtained
!! with a call to \c dgcomp_tens, so that the correct work flow can be
!! performed (communications, module variable settings and so on).
!!
!! Presently, we consider two types of diagnostics: section based ones
!! and field ones. Each of them can be obtained passing the
!! corresponding optional argument to \c dgcomp_tens.
!<----------------------------------------------------------------------
module mod_dgcomp_rhs

!-----------------------------------------------------------------------

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 !$ use omp_lib

 !$ use mod_omp_utils, only: &
 !$   mod_omp_utils_initialized, &
 !$   detailed_timing_omp, &
 !$   omput_push_key,    &
 !$   omput_pop_key,     &
 !$   omput_start_timer, &
 !$   omput_close_timer, &
 !$   omput_write_time

 use mod_mpi_utils, only: &
   mod_mpi_utils_initialized, &
   mpi_integer, wp_mpi,  &
   mpi_status_size,      &
   mpi_isend, mpi_irecv, &
   mpi_waitall, mpi_alltoallv

 use mod_perms, only: &
   mod_perms_initialized, &
   t_perm, idx, operator(*)

 use mod_linal, only: &
   mod_linal_initialized, &
   invmat

 use mod_sympoly, only: &
   mod_sympoly_initialized, &
   t_sympol,     &
   assignment(=),&
   f_to_sympoly, &
   me_int

 use mod_base, only: &
   mod_base_initialized, &
   t_base

 use mod_grid, only: &
   mod_grid_initialized, &
   t_grid, t_ddc_grid,  &
   affmap,              &
   el_courant

 use mod_bcs, only: &
   mod_bcs_initialized, &
   t_bcs,                     &
   b_dir,   b_neu,   b_ddc

 use mod_sections, only: &
   mod_sections_initialized, &
   t_section_collection

 use mod_dgcomp_testcases, only: &
   mod_dgcomp_testcases_initialized, &
   ntrcs, t_phc, phc, bar_mass_flow, &
   coeff_dir, coeff_norm,  &
   coeff_visc, coeff_neu,  &
   coeff_f

 use mod_atm_refstate, only: &
   mod_atm_refstate_initialized, &
   t_atm_refstate_e, atm_ref_e, &
   t_atm_refstate_s, atm_ref_s
 
 use mod_dgcomp_flowstate, only: &
   mod_dgcomp_flowstate_initialized, &
   update_flowstate, &
   mass_flow, kinetic_energy

 use mod_turb_flux, only: &
   mod_turb_flux_initialized, &
   c_turbmod, c_turbmod_progs, c_turbmod_diags, t_turb_diags 

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_dgcomp_rhs_constructor, &
   mod_dgcomp_rhs_destructor,  &
   mod_dgcomp_rhs_initialized, &
   t_bcs_error, &
   dgcomp_tens, & ! tendency computation
   compute_courant, & !additional_diagnostics
   update_time_step, &
   update_mean
 private

!-----------------------------------------------------------------------

! Module types and parameters

 ! public members

 !> This type is used to indicate that an error has occurred in the
 !! computation of the boundary condition
 type t_bcs_error
   logical :: lerr = .false.
   character(len=1000) :: message
 end type t_bcs_error

 ! private members

 !> communication interface with each neighbouring subdomain
 type t_nd
   integer :: id  !< subdomain id
   integer :: nns !< number of shared sides
   integer, allocatable :: ie(:)      !< side elements
   integer, allocatable :: isl(:)     !< local side index
   integer, allocatable :: p_s2s(:)   !< side to side permutation
   real(wp), allocatable :: pb(:,:,:) !< basis traces
   !> @name MPI buffers and requests
   ! !! Counters, buffers and requests for the prognostic variables and
   ! !! the dissipative fluxes (prefix \c f*)
   !! @{
   integer :: cnt, fnt
   real(wp), allocatable :: recbuf(:,:,:), senbuf(:,:,:)
   real(wp), allocatable :: fecbuf(:,:,:), fenbuf(:,:,:)
   integer :: recreq, senreq
   !! @}
   !> flux local buffer: this is analogous to \c fecbuf, except that
   !! it stores the <em>local</em> normal fluxes for the ddc sides;
   !! the reason for having this buffer is that these fluxes must be
   !! computed and sent to the neighbouring subdomains (in \c
   !! bside_send_flux), and then they are used to compute the
   !! numerical fluxes in this subdomain (in \c update_fluxbcs), so it
   !! makes sense storing them somewhere
   real(wp), allocatable :: focbuf(:,:,:)
 end type t_nd

 !> collect all the module domain decomposition information
 !!
 !! \note Buffers should be module variables when using nonblocking
 !! communications, to minimize problems with temporaries.
 type t_ddc
   logical :: ddc  !< <tt>.true.</tt> if there are ddc sides
   integer :: comm !< MPI communicator
   integer :: nnd  !< number of neighbouring subdomains
   type(t_nd), allocatable :: nd(:) !< neighbouring subdomains (nnd)
   !> This field is used to read the receive buffers, associating to
   !! each local boundary side a position in the receive buffers; the
   !! first row gives the subdomain index, while the second the local
   !! side position. The array is allocated for all the boundary
   !! sides, but it is defined only for the ddc sides.
   integer, allocatable :: s2nd(:,:)
 end type t_ddc

! Module variables

 ! public members
 logical, protected ::               &
   mod_dgcomp_rhs_initialized = .false.
 ! private members
 logical :: ldg_allocated
 integer :: &
   uuu_size, & !< size of the prognostic variable arrays
   dfu_size    !< size of the dissipative flux arrays
 !> boundary values and normal fluxes (these latters with respect to
 !! the side normal)
 !! @{
 real(wp), allocatable :: uuu_bcs(:,:,:), nflux_bcs(:,:,:)
 real(wp), allocatable :: gamma0(:,:,:), gammad(:,:,:)
 !! @}
 
 type(t_ddc) :: ddc

 !Mean variable used by hybrid method
 
 real(wp), allocatable :: uuu_mean(:,:,:)
 real(wp), allocatable :: tau_mean_elem(:,:,:,:), &
                          tau_mean_side(:,:,:,:,:), &
                          uu_square_mean_e(:,:,:,:), &
                          uu_square_mean_s(:,:,:,:,:)
 
 real(wp) :: nstep
 
 character(len=*), parameter :: &
   this_mod_name = 'mod_dgcomp_rhs'

 interface reflect_vect
   module procedure reflect_vect_1n, reflect_vect_var
 end interface reflect_vect

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 !> Module setup
 !!
 !! The MPI functions are used if the optional argument \c comm is
 !! present. Notice that this is necessary if there are \c b_ddc type
 !! boundary conditions.
 subroutine mod_dgcomp_rhs_constructor(grid,ddc_grid,viscous_flow, &
                                       comm,base)
  type(t_grid),     intent(in) :: grid
  type(t_ddc_grid), intent(in) :: ddc_grid
  logical,          intent(in) :: viscous_flow
  integer,          intent(in), optional :: comm
  type(t_base),     intent(in), optional :: base
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

  integer :: nnd, id, ins, i, ierr 
  integer, allocatable :: nd2nd(:), offset(:), offse2(:), &
    senbuf(:,:), recbuf(:,:)

   !Consistency checks ---------------------------
   if( (mod_kinds_initialized.eqv..false.) .or. &
    (mod_messages_initialized.eqv..false.) .or. &
!$     ( (detailed_timing_omp.eqv..true.).and. &
!$       (mod_omp_utils_initialized.eqv..false.) ) .or. &
   (mod_mpi_utils_initialized.eqv..false.) .or. &
       (mod_perms_initialized.eqv..false.) .or. &
       (mod_linal_initialized.eqv..false.) .or. &
     (mod_sympoly_initialized.eqv..false.) .or. &
        (mod_base_initialized.eqv..false.) .or. &
        (mod_grid_initialized.eqv..false.) .or. &
         (mod_bcs_initialized.eqv..false.) .or. &
    (mod_sections_initialized.eqv..false.) .or. &
(mod_dgcomp_testcases_initialized.eqv..false.) .or. & 
(mod_atm_refstate_initialized.eqv..false.) .or. &
(mod_dgcomp_flowstate_initialized.eqv..false.) .or. &
   (mod_turb_flux_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_dgcomp_rhs_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   ! size of primal and dual variables
   uuu_size = 2+grid%d+ntrcs
   if(viscous_flow) then
     dfu_size = 1+grid%d+ntrcs
   else
     dfu_size = 0
   endif

   ! boundary condition
   allocate( uuu_bcs(uuu_size,base%ms,grid%ni+1:grid%ns) , &
           nflux_bcs(dfu_size,base%ms,grid%ni+1:grid%ns) )

   ! domain decomposition boundary conditions
   ddc%ddc = .false.
   ddc_if: if(present(comm)) then
     ddc%ddc  = .true.

     ddc%comm = comm
     ddc%nnd  = count(ddc_grid%nns_id.gt.0)
     allocate( ddc%nd(ddc%nnd) , ddc%s2nd(2,grid%ni+1:grid%ns) )
     allocate( nd2nd(0:ddc_grid%nd-1) ) ! work array
     nnd = 0 ! counter
     do id=0,ddc_grid%nd-1
       if(ddc_grid%nns_id(id).gt.0) then
         nnd = nnd+1
         nd2nd(id) = nnd
         ddc%nd(nnd)%id  = id
         ddc%nd(nnd)%nns = ddc_grid%nns_id(id)
         allocate( ddc%nd(nnd)%ie   (ddc%nd(nnd)%nns) , &
                   ddc%nd(nnd)%isl  (ddc%nd(nnd)%nns) , &
                   ddc%nd(nnd)%p_s2s(ddc%nd(nnd)%nns) , &
      ddc%nd(nnd)%pb(base%pk,base%ms,ddc%nd(nnd)%nns) , &
 ddc%nd(nnd)%recbuf(uuu_size,base%ms,ddc%nd(nnd)%nns) , &
 ddc%nd(nnd)%senbuf(uuu_size,base%ms,ddc%nd(nnd)%nns) , &
 ddc%nd(nnd)%fecbuf(dfu_size,base%ms,ddc%nd(nnd)%nns) , &
 ddc%nd(nnd)%fenbuf(dfu_size,base%ms,ddc%nd(nnd)%nns) , &
 ddc%nd(nnd)%focbuf(dfu_size,base%ms,ddc%nd(nnd)%nns) )
         ddc%nd(nnd)%cnt = uuu_size*base%ms*ddc%nd(nnd)%nns
         ddc%nd(nnd)%fnt = dfu_size*base%ms*ddc%nd(nnd)%nns
       endif
     enddo

     ! now we only have to fill ie, isl, p_s2s, pb and s2nd
     allocate(offset(ddc%nnd)); offset = 0 ! work array
     allocate(offse2(0:ddc_grid%nd-1)); offse2 = 0
     do i=1,ddc_grid%nd-1
       offse2(i) = offse2(i-1) + ddc_grid%nns_id(i-1)
     enddo
     allocate(senbuf(2,ddc_grid%nns),recbuf(2,ddc_grid%nns))
     do ins=1,ddc_grid%nns
       id = nd2nd( ddc_grid%ns(ins)%id ) ! local position in ddc%nd
       offset(id) = offset(id) + 1
       ddc%nd(id)%ie (offset(id)) = grid%s(ddc_grid%ns(ins)%i)%ie(1)
       ddc%nd(id)%isl(offset(id)) = grid%s(ddc_grid%ns(ins)%i)%isl(1)
     ddc%nd(id)%p_s2s(offset(id)) = ddc_grid%ns(ins)%p_s2s
       ! we need two permutations: from element to local side and from
       ! local side to neighbouring side
      ddc%nd(id)%pb(:,:,offset(id)) = base%pb(:,                     &
          base%stab(                                                 &
    grid%e(ddc%nd(id)%ie(offset(id)))%pi(ddc%nd(id)%isl(offset(id))) &
                    , : ),                                           &
                      ddc%nd(id)%isl(offset(id))     )
      ddc%nd(id)%pb(:,:,offset(id)) = ddc%nd(id)%pb(:,          &
          base%stab(ddc%nd(id)%p_s2s(offset(id)),:) , offset(id))

       senbuf(:,offse2(ddc_grid%ns(ins)%id)+offset(id)) =          &
                             (/ ddc_grid%id , ddc_grid%ns(ins)%in /)
     enddo
     ! to fill s2nd, each subdomain informs the neighbours about the
     ! order which it will use to send the data
     call mpi_alltoallv(                                      &
            senbuf, 2*ddc_grid%nns_id, 2*offse2, mpi_integer, &
            recbuf, 2*ddc_grid%nns_id, 2*offse2, mpi_integer, &
                        ddc%comm,ierr)
     offset = 0
     do i=1,ddc_grid%nns
       ddc%s2nd(1,recbuf(2,i)) = nd2nd(recbuf(1,i))
       offset(nd2nd(recbuf(1,i))) = offset(nd2nd(recbuf(1,i))) + 1
       ddc%s2nd(2,recbuf(2,i)) = offset(nd2nd(recbuf(1,i))) 
     enddo
     
     ! deallocate work arrays
     deallocate( nd2nd , offset , offse2 , senbuf , recbuf )
   endif ddc_if

   ! Dissipative fluxes
   ldg_allocated = .false.
   ldg_if: if(viscous_flow) then
     call ldg_coefficients(base)
     ldg_allocated = .true.
   endif ldg_if

   ! Allocate and initialize mean variables
   
   allocate(uuu_mean(uuu_size, base%pk,grid%ne))
   allocate(tau_mean_elem(grid%d,grid%d,base%m,grid%ne))
   allocate(tau_mean_side(grid%d,grid%d,base%ms,grid%ns,2))      
   allocate(uu_square_mean_e(grid%d,grid%d,base%m,grid%ne))  
   allocate(uu_square_mean_s(grid%d,grid%d,base%ms,grid%ns,2))
 
   uuu_mean(:,:,:) = 0.0_wp
   tau_mean_elem(:,:,:,:) = 0.0_wp
   tau_mean_side(:,:,:,:,:) = 0.0_wp
   uu_square_mean_e(:,:,:,:) = 0.0_wp
   uu_square_mean_s(:,:,:,:,:) = 0.0_wp  
 
   mod_dgcomp_rhs_initialized = .true.
 end subroutine mod_dgcomp_rhs_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_dgcomp_rhs_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_dgcomp_rhs_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   deallocate( uuu_bcs , nflux_bcs )

   if(ddc%ddc) deallocate( ddc%nd , ddc%s2nd )

   if(ldg_allocated) deallocate( gamma0 , gammad )

   mod_dgcomp_rhs_initialized = .false.
 end subroutine mod_dgcomp_rhs_destructor

!-----------------------------------------------------------------------
 
 !> Setup the coefficients for the gradient computation.
 !!
 !! For viscous flows, the LDG method computes at each time step the
 !! gradient \f$\mathcal{G}q\f$ of a variable \f$q\f$ solving
 !! \f{equation}{
 !!   \int_K\mathcal{G}q\cdot\underline{\varphi}\,dx = 
 !!   -\int_Kq\nabla\cdot\underline{\varphi}\,dx + 
 !!   \int_{\partial K}
 !!   \hat{q}\underline{n}\cdot\underline{\varphi}\,d\sigma.
 !! \f}
 !! Denoting by \f$\left(\mathcal{G}q\right)_i \f$ the vector
 !! coefficients of \f$\mathcal{G}q\f$, so that
 !! \f{displaymath}{
 !!  \mathcal{G}q = \sum_i \left(\mathcal{G}q\right)_i \varphi_i,
 !! \f}
 !! we obtain
 !! \f{equation}{
 !!  \left(\mathcal{G}q\right)_i = \sum_k \left(M^{K,-1}\right)_{ik}
 !!  \left[
 !!   -\int_Kq\nabla\varphi_k\,dx + 
 !!   \int_{\partial K}
 !!   \hat{q}\varphi_k\underline{n}\,d\sigma \right],
 !! \f}
 !! where
 !! \f{equation}{
 !!  M^K_{ij} = \int_K\varphi_i\varphi_j\,dx = det(B)\hat{\Xi}_{ij}.
 !! \f}
 !! The two contributions are now handled separately, since they are
 !! computed with an element and a side loop, respectively. For the
 !! first contribution we have
 !! \f{equation}{
 !!  - \sum_k \left(M^{K,-1}\right)_{ik} \int_Kq\nabla\varphi_k\,dx =
 !!  B^{-T}\sum_l\hat{\underline{\Gamma}}^0_{il}\, q(x_l)
 !! \f}
 !! with
 !! \f{equation}{
 !!   \hat{\underline{\Gamma}}^0_{il} =
 !!   -\hat{w}_l\sum_k\hat{\Xi}^{-1}_{ik}
 !!   \nabla\hat{\varphi}_k(\hat{x}_l).
 !! \f}
 !! Concerning the second contribution, we have
 !! \f{equation}{
 !!  \sum_k \left(M^{K,-1}\right)_{ik} \int_{\partial K}
 !!  \hat{q}\varphi_k\underline{n}\,d\sigma = \sum_{e\in\partial K}
 !!  \frac{|e|}{det(B)}\underline{n}\sum_l
 !!  \hat{\Gamma}^{\partial}_{il}\,\hat{q}(x_l)
 !! \f}
 !! with
 !! \f{equation}{
 !!   \hat{\Gamma}^{\partial}_{il} = \frac{\hat{w}^s_l}{|\hat{e}|}
 !!   \sum_k\hat{\Xi}^{-1}_{ik} \hat{\varphi}_k(\hat{x}_l).
 !! \f}
 !! \note In this module we assume that the basis is orthonormal, so
 !! that \f$\hat{\Xi}\f$ is the identity matrix; nevertheless we write
 !! here the complete expression, since it doesn't imply any overhead.
 subroutine ldg_coefficients(base)
  type(t_base), intent(in) :: base
 
  integer :: i, j, k, l
  real(wp), allocatable :: xi(:,:), xi_i(:,:)
  character(len=*), parameter :: &
    this_sub_name = 'ldg_coefficients'

   allocate( gamma0(base%me%d,base%pk,base%m             ) , &
             gammad(          base%pk,base%ms,base%me%d+1) )

   ! work array
   allocate( xi(base%pk,base%pk) , xi_i(base%pk,base%pk) )
   do i=1,base%pk
     do j=1,i ! the mass matrix is symmetric
       xi(i,j) = me_int( base%me%d , f_to_sympoly(base%p_s(i)*base%p_s(j)) )
       if(j.ne.i) xi(j,i) = xi(i,j)
     enddo
   enddo
   call invmat(xi,xi_i)

   ! compute gamma0
   do i=1,base%pk
     do l=1,base%m
       gamma0(:,i,l) = 0.0_wp
       do k=1,base%pk
         gamma0(:,i,l) = gamma0(:,i,l) + xi_i(i,k)*base%gradp(:,k,l)
       enddo
       gamma0(:,i,l) = -base%wg(l)*gamma0(:,i,l)
     enddo
   enddo

   ! compute gammad
   do j=1,base%me%d+1
     do l=1,base%ms
       gammad(:,l,j) = base%wgs(l)/base%me%voldm1      &
                       * matmul( xi_i , base%pb(:,l,j) )
     enddo
   enddo
 
   ! cleanup
   deallocate(xi,xi_i)

 end subroutine ldg_coefficients
 
!-----------------------------------------------------------------------
 
 !> Tendencies computations
 !!
 !! The NS equations are solved in the following form:
 !! \f{equation}{
 !!  \begin{array}{rcl}
 !!   \displaystyle
 !!   \frac{d}{dt}\int_K\rho\varphi\,dx & = &
 !!   \displaystyle
 !!    \int_K \underline{U}\cdot\nabla\varphi\,dx 
 !!   - \int_{\partial K}\hat{\underline{U}}\cdot \underline{n}
 !!   \varphi\,d\sigma \\[3mm]
 !!   \displaystyle
 !!   \frac{d}{dt}\int_K E \varphi\,dx & = &
 !!   \displaystyle
 !!    \int_K \frac{H}{\rho}\underline{U}\cdot\nabla\varphi\,dx 
 !!   - \int_{\partial K}\widehat{\frac{H}{\rho}\underline{U}}\cdot
 !!   \underline{n} \varphi\,d\sigma-
 !!   \mathcal{D}_K\underline{F}^d_E  \\[3mm]
 !!   \displaystyle
 !!   \frac{d}{dt}\int_K U_{i_d} \varphi\,dx & = &
 !!   \displaystyle
 !!    \int_K \left[ \frac{U_{i_d}}{\rho}\underline{U} +
 !!   p'\underline{e}_{i_d}\right]\cdot\nabla\varphi\,dx
 !!   - \int_{\partial K}\widehat{\left[
 !!   \frac{U_{i_d}}{\rho}\underline{U} +
 !!   p'\underline{e}_{i_d}\right]} \cdot \underline{n}
 !!   \varphi\,d\sigma- \mathcal{D}_K\underline{F}^d_{U_{i_d}}
 !!   + \int_K \rho'g_{i_d}\varphi\,dx
 !!  \end{array}
 !! \f}
 !! where the divergence of the diffusive (or turbulent) flux for a
 !! generic quantity \f$q\f$ is defined as
 !! \f{equation}{
 !!   \mathcal{D}_K\underline{F}^d_q = 
 !!    -\int_K \underline{F}^d_q\cdot \nabla\varphi\,dx 
 !!   + \int_{\partial K}\widehat{\underline{F}^d_q}\cdot
 !!   \underline{n} \varphi\,d\sigma.
 !! \f}
 !! The energy and momentum diffusive/turbulent fluxes are computed in
 !! \c mod_turb_flux, and the (numerical) gradients required for such
 !! computation are obtained as described in \c ldg_coefficients.
 !!
 !! Tracers are treated in conservative form, so that for a tracer
 !! \f$C\f$ we have
 !! \f{equation}{
 !!  \begin{array}{rcl}
 !!   \displaystyle
 !!   \frac{d}{dt}\int_K C \varphi\,dx & = &
 !!   \displaystyle
 !!    \int_K \frac{C}{\rho}\underline{U}\cdot\nabla\varphi\,dx 
 !!   - \int_{\partial K}\widehat{\frac{C}{\rho}\underline{U}}\cdot
 !!   \underline{n} \varphi\,d\sigma-
 !!   \mathcal{D}_K\underline{F}^d_C.
 !!  \end{array}
 !! \f}
 !! The concentration is then \f$c=C/\rho\f$; use of equal order basis
 !! functions ensures that constant solutions of the form \f$c\equiv
 !! 1\f$ are always preserved.
 !!
 !! \note The presence of the diffusive terms has two overheads: two
 !! additional loops are required for the precomputation of the
 !! gradients, an element loop and a side loop; additional terms are
 !! present in the evolution equations.
 !!
 !! The integrals are computed evaluating the (nonlinear) terms in the
 !! quadrature nodes and then using the quadrature formula. Due to the
 !! strong nonlinearities, this solution is simpler comparing to
 !! expanding the integrals in terms of the basis functions, as it
 !! done, for instance, in \c mod_cg_ns_linsistem for the
 !! incompressible NS system. For the computation of the numerical
 !! fluxes, the simplest option is the <em>Rusanov</em> flux, for
 !! which we have
 !! \f{equation}{
 !!  \begin{array}{rcl}
 !!   \displaystyle
 !!   \hat{\underline{U}} & = &
 !!   \displaystyle
 !!   \frac{1}{2}\left( \underline{U}^l +
 !!   \underline{U}^r \right) + \frac{|\lambda|}{2}\left(
 !!   \rho^l\underline{n}^l + \rho^r\underline{n}^r \right)
 !!  \end{array}
 !! \f}
 !! where the superscripts \f$l\f$ and \f$r\f$ denote the two values
 !! at the element interface and
 !! \f{equation}{
 !!  \lambda = \max_{l,r}\left(|\underline{u}\cdot\underline{n}| +
 !!  a\right).
 !! \f}
 !! Denoting by \f$\hat{\underline{F}}\f$ a generic numerical flux, the
 !! computation of the boundary terms is done as follows:
 !! <ul>
 !!  <li> compute the (single valued) quantity
 !!  \f$\hat{F}_{n^e} = \hat{\underline{F}}\cdot\underline{n}^e\f$,
 !!  where \f$\underline{n}^e\f$ is the intrinsic side normal (i.e.
 !!  the normal of the first connected element)
 !!  <li> compute
 !!  \f{equation}{
 !!   (\underline{n}^{K^l}\cdot\underline{n}^e) \int_{\partial K^l\cap
 !!   e}\hat{F}_{n^e}\varphi^l\,d\sigma
 !!  \f}
 !!  and the similar quantity for \f$K^r\f$.
 !! </ul>
 !!
 !! \note This subroutine also directly takes care of computing the
 !! tendency of the integrated mass flow
 !! \f$\underline{\mathcal{M}}_{\mathcal{I}}\f$, as discussed in the
 !! documentation of \c t_dgcomp_stv.
 !!
 !! \section ddc_comm MPI communications
 !!
 !! Communications are required with the following pattern
 !! <ul>
 !!  <li> send/receive the boundary values of the prognostic
 !!  variables
 !!  <li> send/receive the boundary values of the dissipative
 !!  fluxes.
 !! </ul>
 !!
 !! \note Subroutines called in this subroutine make use of the module
 !! variable \c ddc; such use, however, is self-contained.
 !!
 !! \section dgcomp_rhs_tens_diagnostics Diagnostics
 !!
 !! This subroutine can be used also for the computation of the flow
 !! diagnostics, rather than the tendencies. This happens when the
 !! optional argument \c sc and/or \c td are present. In this case, \c
 !! tens is not used and it is safe to pass an actual argument which
 !! component \c u is not allocated.
 !!
 !! \section dgcomp_rhs_impl_details Implementation details
 !!
 !! In principle, this subroutine could have an interface very similar
 !! to \c dgcomp_ode_rhs, with the addition of \c sc and \c td, and
 !! using \c t_dgcomp_stv, \c t_dgcomp_ods and \c t_dgcomp_ode instead
 !! of the abstract types. However, this would introduce a circular
 !! dependence, so we unpack the types and pass each field as separate
 !! argument.
 !!
 !! The variables related with the turbulent model have the
 !! allocatable attribute, since they can be deallocated (for
 !! instance, for the inviscid case), which would be illegal for non
 !! allocatable arguments.
 subroutine dgcomp_tens(tens, im_tens, turbmod_tens, err ,        &
                        grid, base, bcs, viscous_flow, turbmod,   &
                        t, uuu, im, turbmod_progs, turbmod_diags, &
                        term, sc, td)
  ! fields from the t_dgcomp_stv tnd object
  real(wp),               intent(out  ) :: tens(:,:,:), im_tens(:)
  class(c_turbmod_progs), intent(inout), allocatable :: turbmod_tens
  type(t_bcs_error),      intent(out  ) :: err
  ! fields from the t_dgcomp_ode object
  type(t_grid),     intent(in) :: grid
  type(t_base),     intent(in) :: base
  type(t_bcs ),     intent(in) :: bcs
  logical,          intent(in) :: viscous_flow
  class(c_turbmod), intent(in), allocatable :: turbmod
  ! present status
  real(wp),               intent(in) :: t
  real(wp),               intent(in) :: uuu(:,:,:), im(:)
  class(c_turbmod_progs), intent(in), allocatable :: turbmod_progs
  ! diagnostic variables
  class(c_turbmod_diags), intent(inout), allocatable :: turbmod_diags
  ! other arguments
  integer,                    optional, intent(in) :: term(2)
  type(t_section_collection), optional, intent(inout), target :: sc
  type(t_turb_diags),         optional, intent(inout)         :: td

  integer :: ie
  logical :: compute_tens, compute_sc, compute_td
  character(len=*), parameter :: &
    this_sub_name = 'dgcomp_tens'

   compute_sc = present(sc)
   compute_td = present(td)
   compute_tens = (.not.compute_sc) .and. (.not.compute_td)

   !--------------------------------------------------------------------
   !0) Preliminary step: update the flow state (used by some test cases)
   call update_flowstate(uuu,base,grid)
   !--------------------------------------------------------------------

   !--------------------------------------------------------------------
   !1) Gradient computations (if required)
   if(viscous_flow) then

     !------------------------------------------------------------------
     !1.1) Update turbmod_diags%grad
     call dgcomp_grad(turbmod_diags%grad,turbmod,grid,base,bcs,uuu,err)
     !------------------------------------------------------------------

     !------------------------------------------------------------------
     !1.2) Update turbulence model coefficients (use the gradients)
     call turbmod%compute_coeff_diags(turbmod_diags , grid,base,uuu , &
                                      turbmod_progs, atm_ref_e)
     !------------------------------------------------------------------

     !------------------------------------------------------------------
     !1.3) Second communication (uuu communicated in dgcomp_grad)
     if( compute_tens .or. compute_sc ) then
       if(ddc%ddc) call bside_send_flux(uuu,grid,base , turbmod, &
                                      turbmod_progs,turbmod_diags)
     endif
     !------------------------------------------------------------------

   else

     !------------------------------------------------------------------
     !1.1) First communication
     if( compute_tens .or. compute_sc ) then
       if(ddc%ddc) call bside_send(uuu)
     endif
     !------------------------------------------------------------------

   endif
   !--------------------------------------------------------------------

   !--------------------------------------------------------------------
   !3) Initialization
   if(compute_tens) &
     tens = 0.0_wp
   !--------------------------------------------------------------------
   
   !--------------------------------------------------------------------
   !4) Element loop
   if( compute_tens .or. compute_td ) &
     call tens_elem_loop(tens , grid,base,uuu,im,viscous_flow ,  &
                         turbmod,turbmod_progs,turbmod_diags , td)

   if( (.not.compute_tens) .and. (.not.compute_sc) ) return
   !--------------------------------------------------------------------

   !--------------------------------------------------------------------
   !5) Side loop
   ! Note: the receive receives the variable if the flow is inviscid
   ! and the flux if the flow is viscous.
   if(ddc%ddc) call bside_recv()
   if(viscous_flow) then
     call update_fluxbcs(nflux_bcs,grid,base,bcs,uuu , turbmod,&
                         turbmod_progs,turbmod_diags)
   else
     call update_bcs(uuu_bcs,grid,base,bcs,uuu,err)
   endif
   call tens_side_loop(tens , grid,base,uuu,viscous_flow ,     &
                       turbmod,turbmod_progs,turbmod_diags , sc)

   if(.not.compute_tens) return
   !--------------------------------------------------------------------

   !--------------------------------------------------------------------
   !6) Include diagonal mass matrix
   !$omp parallel do schedule(static) &
   !$omp   private(ie) shared(tens,grid) default(none)
   do ie=1,grid%ne
     tens(:,:,ie) = tens(:,:,ie)/grid%e(ie)%det_b
   enddo
   !$omp end parallel do
   !--------------------------------------------------------------------

   !--------------------------------------------------------------------
   !7) Integrated mass flow: this operation is identical on all
   ! processors, thanks to the fact that the mass_flow is already
   ! up-to-date.
   im_tens = mass_flow - bar_mass_flow
   !--------------------------------------------------------------------

 end subroutine dgcomp_tens
 
!-----------------------------------------------------------------------

 !> Gradient computation
 !!
 !! This function computes the (DG) gradient of the state \c uuu.
 !!
 !! For the definition of the gradient, we follow <a
 !! href="http://dx.doi.org/10.1137/S0036142900371003">[Castillo, Cockburn,
 !! Perugia, Sch&ouml;tzau, 2000]</a> and set
 !! \f{displaymath}{
 !!  \qquad c_{12} = c_{22} = 0.
 !! \f}
 !! In practice, as far as the gradient is concerned, the LDG method
 !! considered here coincides with the Bassi-Rebay method.
 !!
 !! Note that this function updates/changes various module variables:
 !! <ul>
 !!  <li> by calling \c update_bcs, it updates the module variable \c
 !!  uuu_bcs
 !!  <li> it (indirectly) makes a self-contained use of the module
 !!  variable \c ddc.
 !! </ul>
 subroutine dgcomp_grad(grad,turbmod,grid,base,bcs,uuu,err)
  class(c_turbmod),  intent(in) :: turbmod
  type(t_grid),      intent(in) :: grid
  type(t_base),      intent(in) :: base
  type(t_bcs),       intent(in) :: bcs
  real(wp),          intent(in) :: uuu(:,:,:)
  real(wp),          intent(out) :: grad(:,:,:,:)
  type(t_bcs_error), intent(out) :: err

   !--------------------------------------------------------------------
   !0) Start communication
   if(ddc%ddc) call bside_send(uuu)
   !--------------------------------------------------------------------

   !--------------------------------------------------------------------
   !1) Initialization
   grad = 0.0_wp
   !--------------------------------------------------------------------

   !--------------------------------------------------------------------
   !2) Gradient element loop
   call grad_elem_loop(grad,turbmod,grid,base,uuu)
   !--------------------------------------------------------------------

   !--------------------------------------------------------------------
   !3) Complete communication and update bcs
   if(ddc%ddc) call bside_recv()
   call update_bcs(uuu_bcs,grid,base,bcs,uuu,err)
   !--------------------------------------------------------------------

   !--------------------------------------------------------------------
   !4) Gradient side loop
   call grad_side_loop(grad,turbmod,grid,base,uuu)
   !--------------------------------------------------------------------

 end subroutine dgcomp_grad

!-----------------------------------------------------------------------

 !> Start the nonblocking communication of \c uuu
 !!
 !! \note Thus subroutine uses the send and receive buffers of the
 !! module variable \c ddc.
 subroutine bside_send(uuu)
  real(wp), intent(in) :: uuu(:,:,:)

  integer :: id, i, ie, ierr

   ! loop on the neighbours
   neigh_do: do id=1,ddc%nnd

     !1) receive the data
     call mpi_irecv(      &
       ddc%nd(id)%recbuf, & ! buffer
       ddc%nd(id)%cnt,    & ! count
       wp_mpi,            & ! data type
       ddc%nd(id)%id,     & ! source
       1,                 & ! tag
       ddc%comm,          & ! communicator
       ddc%nd(id)%recreq, & ! request handle
       ierr )

     !2) prepare and send the data
     do i=1,ddc%nd(id)%nns
       ie = ddc%nd(id)%ie(i)
       ddc%nd(id)%senbuf(:,:,i) =                                   &
                         matmul( uuu(:,:,ie) , ddc%nd(id)%pb(:,:,i) )
     enddo
     call mpi_isend(      &
       ddc%nd(id)%senbuf, & ! buffer
       ddc%nd(id)%cnt,    & ! count
       wp_mpi,            & ! data type
       ddc%nd(id)%id,     & ! destination
       1,                 & ! tag
       ddc%comm,          & ! communicator
       ddc%nd(id)%senreq, & ! request handle
       ierr )

   enddo neigh_do

 end subroutine bside_send

!-----------------------------------------------------------------------

 !> Start the nonblocking communication of the complete dissipative
 !! fluxes
 !!
 !! This subroutine is similar to \c bside_send, except that it
 !! considers the dissipative fluxes. Such fluxes are not stored in a
 !! global variable analogous to \c uuu, but they are computed on the
 !! fly for each ddc boundary side.
 !!
 !! \note This subroutine uses the send and receive buffers of the
 !! module variable \c ddc.
 subroutine bside_send_flux(uuu,grid,base , turbmod,progs,diags)
  real(wp), intent(in) :: uuu(:,:,:)
  type(t_grid), intent(in) :: grid
  type(t_base), intent(in) :: base
  class(c_turbmod),       intent(in) :: turbmod
  class(c_turbmod_progs), intent(in), allocatable :: progs
  class(c_turbmod_diags), intent(in) :: diags

  integer :: id, i, ie, is, isl, l, ll, ierr
  real(wp) :: fb(grid%d,dfu_size,base%ms), fbn(dfu_size,base%ms)

   ! loop on the neighbours
   neigh_do: do id=1,ddc%nnd

     !1) receive the data
     call mpi_irecv(      &
       ddc%nd(id)%fecbuf, & ! buffer
       ddc%nd(id)%fnt,    & ! count
       wp_mpi,            & ! data type
       ddc%nd(id)%id,     & ! source
       2,                 & ! tag
       ddc%comm,          & ! communicator
       ddc%nd(id)%recreq, & ! request handle
       ierr )

     !2) prepare and send the data
     do i=1,ddc%nd(id)%nns
       ie  = ddc%nd(id)%ie(i)
       isl = ddc%nd(id)%isl(i)
       is  = grid%e(ie)%is(isl)
       call elemside_viscflux(fb , ie,isl , uuu(:,:,ie),grid,base , &
                              turbmod,progs,diags)

       ! Compute the normal fluxes
       do l=1,base%ms
         fbn(:,l) = matmul( grid%e(ie)%n(:,isl) , fb(:,:,l) )
       enddo

       ! Store the normal fluxes in focbuf and fenbuf. Notice that
       ! there are some differences concerning these two buffers:
       ! * the sides have a different ordering: focbuf is ordered as
       !   fecbuf, accorind to ddc%s2nd, while fenbuf can be indexed
       !   with i
       ! * the quadrature nodes have a different order: focbuf follows
       !   the local order, while fenbuf uses the neighbouring order
       ! * the normal has a different orientation: outward for the
       !   local or neighbouring subdomain
       do l=1,base%ms
         ! we use that  ddc%s2nd(1,is) == id
         ddc%nd(id)%focbuf(:,l,ddc%s2nd(2,is)) =  fbn(:,l )
         ! reorder the nodes and use -n
         ll = base%stab(ddc%nd(id)%p_s2s(i),l)
         ddc%nd(id)%fenbuf(:,l,       i      ) = -fbn(:,ll)
       enddo
     enddo
     call mpi_isend(      &
       ddc%nd(id)%fenbuf, & ! buffer
       ddc%nd(id)%fnt,    & ! count
       wp_mpi,            & ! data type
       ddc%nd(id)%id,     & ! destination
       2,                 & ! tag
       ddc%comm,          & ! communicator
       ddc%nd(id)%senreq, & ! request handle
       ierr )

   enddo neigh_do

 end subroutine bside_send_flux

!-----------------------------------------------------------------------

 !> Wait all the pending communications in \c ddc
 !!
 !! This subroutines waits for all the send requests of
 !! <tt>ddc\%nd\%senreq</tt> and all the receive requests of
 !! <tt>ddc\%nd\%recreq</tt>. Notice that it doesn't matter here
 !! whether such requests concern the primal buffers \c recbuf and \c
 !! senbuf or the flux buffers \c fecbuf and \c fenbuf.
 subroutine bside_recv()
  integer :: ierr
  integer :: mpi_stat(mpi_status_size,ddc%nnd)

   call mpi_waitall( ddc%nnd , ddc%nd%recreq , mpi_stat , ierr )
   call mpi_waitall( ddc%nnd , ddc%nd%senreq , mpi_stat , ierr )

 end subroutine bside_recv

!-----------------------------------------------------------------------
 
 pure &
 !$ subroutine omp_dummy_2(); end subroutine omp_dummy_2
 subroutine tens_elem_loop(tens , grid,base,uuu,im,viscous_flow, &
                           turbmod,progs,diags , td)
  type(t_grid),           intent(in)    :: grid
  type(t_base),           intent(in)    :: base
  real(wp),               intent(in)    :: uuu(:,:,:), im(:)
  logical,                intent(in)    :: viscous_flow
  class(c_turbmod),       intent(in), allocatable :: turbmod
  class(c_turbmod_progs), intent(in), allocatable :: progs
  class(c_turbmod_diags), intent(in), allocatable :: diags
  real(wp),               intent(inout) :: tens(:,:,:)
  type(t_turb_diags),     intent(inout), optional :: td

  logical :: compute_tens, compute_td
  integer :: ie, l, id, it !, i, j
  real(wp) :: &
    bit(grid%m,grid%d), wg(base%m), gradp(grid%d,base%pk,base%m),      &
    ugradp(base%pk), uuug(size(uuu,1),base%m), rho(base%m), e(base%m), &
    u(grid%d,base%m), ekin(base%m), p_p(base%m), p(base%m), hh(base%m),&
    uu(grid%d,base%m), tnde(size(uuu,1),base%pk), cc(ntrcs,base%m),    &
    rgcv, mg, f(grid%d,base%m)
  real(wp) :: fem(grid%d,dfu_size,base%m)
  ! Hybrid terms 
  real(wp) :: ht(grid%d+1,base%pk)
  ! fem term  
  real(wp) :: fem_hyb_pk(dfu_size,base%pk)
  
  character(len=*), parameter :: &
    this_sub_name = 'tens_elem_loop'

   compute_td = present(td)
   compute_tens = .not.compute_td
   if( compute_td .and. (.not.viscous_flow) ) return ! nothing to do

   rgcv = phc%rgas / phc%cv
   mg   = -phc%gravity
   !$omp do schedule(static)
   elem_do: do ie=1,grid%ne

     !------------------------------------------------------------------
     !1) Preliminary computations

     ! gradient of the basis functions (includes the quad. weighs)
     bit = transpose( grid%e(ie)%bi )
     wg = grid%e(ie)%det_b * base%wg
     do l=1,base%m
       ! notice: this should be changed when d.ne.m
       gradp(:,:,l) = wg(l) * matmul( bit , base%gradp(:,:,l) )
     enddo

     ! evaluate the solution at the quadrature nodes
     uuug = matmul( uuu(:,:,ie) , base%p )
     rho = uuug(1,:) + atm_ref_e(:,ie)%rho
     e   = uuug(2,:) + atm_ref_e(:,ie)%e
     u   = uuug(2+1:2+grid%d,:)

     ! kinetic energy
     ekin = 0.5_wp * sum( u**2 , 1 ) / rho
     ! pressure perturbation
     p_p  = rgcv*( uuug(2,:) - ekin - atm_ref_e(:,ie)%phi*uuug(1,:) )
     ! total pressure
     p    = p_p + atm_ref_e(:,ie)%p
     ! specific enthalpy
     hh   = (e+p)/rho
     ! velocity
     do id=1,grid%d
       uu(id,:) = u(id,:)/rho
     enddo
     ! tracers
     do it=1,ntrcs
       cc(it,:) = uuug(2+grid%d+it,:)/rho
     enddo
     !------------------------------------------------------------------

     !------------------------------------------------------------------
     !2) Inviscid fluxes
     tnde = 0.0_wp ! element tendencies
     do l=1,base%m

       ! precompute U \cdot \nabla\phi (gradp includes quad. weighs!)
       ugradp = matmul( u(:,l) , gradp(:,:,l) )

       ! density: U \cdot \nabla\phi
       tnde(1,:) = tnde(1,:) + ugradp
       ! energy: hU \cdot \nabla\phi
       tnde(2,:) = tnde(2,:) + hh(l)*ugradp
       ! momentum: uU \cdot \nabla\phi + p'\partial_{x_i}\phi
       do id=1,grid%d
         tnde(2+id,:) = tnde(2+id,:) &
           + uu(id,l)*ugradp + p_p(l)*gradp(id,:,l)
       enddo
       ! gravity forcing in the momentum equation
       tnde(2+grid%d,:) = tnde(2+grid%d,:) &
         + wg(l) * mg*uuug(1,l)*base%p(:,l)
       ! tracers
       do it=1,ntrcs
         tnde(2+grid%d+it,:) = tnde(2+grid%d+it,:) + cc(it,l)*ugradp
       enddo

     enddo
     !------------------------------------------------------------------

     !------------------------------------------------------------------
     !3) Viscous fluxes
     if(viscous_flow) then
       
       ht(:,:)= 0.0_wp
       ! compute the viscous fluxes
       call turbmod%flux(fem,ie,affmap(grid%e(ie),base%xig),base%p,   &
                         grid,uuug,atm_ref_e(:,ie),rho,p,uu,cc,progs, &
                         diags,td,uuu_mean(:,:,ie),                   &
                         tau_mean_elem(:,:,:,ie),                     &
                         uu_square_mean_e(:,:,:,ie),ht(:,:))

       ! if we are only interested in td, we are done for this elem.
       if(.not.compute_tens) cycle elem_do

       ! compute the tendencies
       do l=1,base%m
         ! all the variables except density
         fem_hyb_pk = matmul(transpose(fem(:,:,l)),gradp(:,:,l))
         tnde(2:,:) = tnde(2:,:) + fem_hyb_pk
       enddo

     endif
     !------------------------------------------------------------------

     !------------------------------------------------------------------
     !4) Driving force
     if(associated(coeff_f)) then

       f = coeff_f(affmap(grid%e(ie),base%xig),mass_flow,im)
       ! include density and quad. weighs
       do id=1,grid%d
         f(id,:) = wg*rho*f(id,:)
       enddo

       ! momentum equation
       tnde(2+1:2+grid%d,:) = tnde(2+1:2+grid%d,:)       &
                             + matmul(f,transpose(base%p))
       ! energy equation
       tnde(2,:) = tnde(2,:) + matmul(base%p,sum(f*uu,1))

     endif
     !------------------------------------------------------------------
     
     !------------------------------------------------------------------    
     !5) Extra terms
   ! ht is non-zero only for hybrid method (with variable RANS/LES
   ! belnding factor) 
   ! Clipping for stability reasons
   !    do i=2,4
   !      do j=1,base%pk
   !        ht(i,j)=minval((/ht(i,j),0.3*fem_hyb_pk(i,j)/))
   !        ht(i,j)=maxval((/ht(i,j),-0.1*fem_hyb_pk(i,j)/))  
   !      enddo    
   !    enddo
 
     ! continuity equation
   !  tnde(1,:) = tnde(1,:)!+ ht(1,:)!no HT for stability reasons
     ! momentum equation
   !  tnde(2+1:2+grid%d,:) = tnde(2+1:2+grid%d,:) !+ ht(2:,:)
     !------------------------------------------------------------------    
                                
     !------------------------------------------------------------------    
     !6) Accumulate the contributions in the global array
     tens(:,:,ie) = tens(:,:,ie) + tnde
     !------------------------------------------------------------------    

   enddo elem_do
 
 end subroutine tens_elem_loop
 
!-----------------------------------------------------------------------

 pure &
 !$ subroutine omp_dummy_3(); end subroutine omp_dummy_3
 subroutine grad_elem_loop(grad,turbmod,grid,base,uuu)
  class(c_turbmod), intent(in) :: turbmod
  type(t_grid),     intent(in) :: grid
  type(t_base),     intent(in) :: base
  real(wp),         intent(in) :: uuu(:,:,:)
  real(wp),         intent(inout) :: grad(:,:,:,:)

  integer :: ie, iv, l
  real(wp) :: uue(size(grad,2),base%m), bit(grid%d,grid%d), &
    gq(grid%d,base%pk)

   !$omp parallel do schedule(static) &
   !$omp   private( ie , uue , bit , iv , gq , l ) &
   !$omp   shared( grid , base , uuu , atm_ref_e , gamma0 , grad ) &
   !$omp   default(none)
   elem_do: do ie=1,grid%ne

     ! evaluate the solution in the quadrature nodes
     call turbmod%compute_grad_diags( uue, matmul(uuu(:,:,ie),base%p), &
                                      atm_ref_e(:,ie) )

     ! we also need bi^T
     bit = transpose( grid%e(ie)%bi )

     do iv=1,size(grad,2) ! variable loop
       gq = 0.0_wp
       do l=1,base%m
         gq = gq + gamma0(:,:,l) * uue(iv,l)
       enddo
       grad(:,iv,:,ie) = grad(:,iv,:,ie) + matmul( bit , gq )
     enddo

   enddo elem_do
   !$omp end parallel do

 end subroutine grad_elem_loop

!-----------------------------------------------------------------------
 
 pure &
 !$ subroutine omp_dummy_4(); end subroutine omp_dummy_4
 subroutine tens_side_loop(tens , grid,base,uuu,viscous_flow, &
                           turbmod,progs,diags , sc)
  type(t_grid),           intent(in)    :: grid
  type(t_base),           intent(in)    :: base
  real(wp),               intent(in)    :: uuu(:,:,:)
  logical,                intent(in)    :: viscous_flow
  class(c_turbmod),       intent(in), allocatable :: turbmod
  class(c_turbmod_progs), intent(in), allocatable :: progs
  class(c_turbmod_diags), intent(in), allocatable :: diags
  real(wp),               intent(inout) :: tens(:,:,:)
  type(t_section_collection), intent(inout), target, optional :: sc
 
  ! all the computations are done using the normal of the first
  ! element, and then the proper sign is introduced with the following
  ! parameter
  real(wp), parameter :: sigma(2) = (/ 1.0_wp , -1.0_wp /)
  logical :: with_stat
  integer :: is, ie_s(2),is_loc(2), ie, i_perm, it, l, iv
  real(wp) :: pb(base%pk,base%ms,2), n(grid%m), wgsa(base%ms), &
    uuug(size(uuu,1),base%ms,2), rho(base%ms,2), e(base%ms,2), &
    u(grid%d,base%ms,2), ekin(base%ms,2), p_p(base%ms,2),      &
    p(base%ms,2), hh(base%ms,2), uu(grid%d,base%ms,2),         &
    cc(ntrcs,base%ms,2), aun(base%ms,2),                       &
    fne(size(uuu,1),base%ms), lambda(base%ms), &
    tnd(size(uuu,1),base%pk,2), rgcv, gamma
  real(wp) :: fem(grid%d,dfu_size,base%ms,2)
  ! Hybrid terms
  real(wp) :: ht(grid%d,base%ms,2)
  character(len=*), parameter :: &
    this_sub_name = 'tens_side_loop'
 
   rgcv = phc%rgas / phc%cv
   gamma = phc%gamma
   with_stat = present(sc)

   internal_side_do: do is=1,grid%ni
     if(with_stat) then
       if(sc%s2ss(is)%ns.eq.0) cycle internal_side_do
     endif

     !------------------------------------------------------------------
     !1) Preliminary computations

     ! the two elements of the side
     ie_s = grid%s(is)%ie

     ! side local indexes on the two elements
     is_loc = grid%s(is)%isl

     ! base function evaluations on the side quad. nodes
     do ie=1,2 ! loop on the two elements of the side
       ! index of the element -> side permutation (perm. pi)
       i_perm = grid%e(ie_s(ie))%pi(is_loc(ie))
       pb(:,:,ie) = & ! reorder on second index (side quad. node)
         base%pb(:, & ! first index unchanged
          base%stab(i_perm,:), & ! second index: reordered with pi
          is_loc(ie)) ! third index: local side
     enddo
     ! the normal of the first element is used as the side normal
     n = grid%e(ie_s(1))%n(:,is_loc(1))

     ! rescale the quadrature weights
     wgsa = (grid%s(is)%a/base%me%voldm1) * base%wgs

     !------------------------------------------------------------------

     !------------------------------------------------------------------
     !2) Compute the numerical fluxes (include quad. weights)

     !2.1) evaluate the solution in the quadrature nodes
     do ie=1,2
       uuug(:,:,ie) = matmul( uuu(:,:,ie_s(ie)) , pb(:,:,ie) )
     enddo
     call side_diags(rho, e, u, ekin, p_p, p, hh, uu, cc, aun, lambda, &
                     is, uuug, grid%d, rgcv, gamma, n, base%ms )

     !2.2) evaluate the normal numerical flux fne
     call side_numflux(fne,grid%d,base%ms,n,u,hh,uu,p_p,cc,lambda,uuug)
     if(with_stat) call update_side_section(sc,matmul(fne,wgsa),is,1)

     ! add the viscous terms
     viscous_flow_if: if(viscous_flow) then
     
       ht = 0.0_wp
         do ie=1,2
         ! compute the viscous fluxes
         call turbmod%flux( fem(:,:,:,ie),                             &
                ie_s(ie),affmap(grid%s(is),base%xigs),pb(:,:,ie),      &
                grid,uuug(:,:,ie),atm_ref_s(:,is),                     &
                rho(:,ie),p(:,ie),uu(:,:,ie),cc(:,:,ie),progs,diags,   &
                uuu_mean=uuu_mean(:,:,ie_s(ie)),                       &
                tau_mean=tau_mean_side(:,:,:,is,ie),                   &
                uu_square_mean=uu_square_mean_s(:,:,:,is,ie),          &
                ht=ht(:,:,ie))
       enddo

       if(with_stat) then
         ! This is not very nice, but this way we don't have to add
         ! specific variables, which is useful in case this part must
         ! be reworked.
         !call update_side_section(sc,                  &
         !  matmul( reshape( matmul(n, reshape(         &
         !    0.5_wp*sum(fem,dim=4) ,                   &
         !   (/size(fem,1),size(fem,2)*size(fem,3)/))), &
         !  (/size(fem,2),size(fem,3)/) ),wgsa) , is,2)
         ! An alternative form
         call update_side_section(sc,                            &
           (/ (dot_product(                                      &
             matmul( n , 0.5_wp*(fem(:,it,:,1)+fem(:,it,:,2)) ), &
           wgsa), it=1,dfu_size) /) , is,2)
       else
         do it=1,dfu_size ! energy, momentum and tracers
           ! Formally we use here a Bassi-Rebay numerical flux, however
           ! a jump penalization term is implicitly added in the
           ! inviscid numerical flux: see the comments in side_numflux.
           fne(1+it,:) = fne(1+it,:) + &
             matmul( n , 0.5_wp*(fem(:,it,:,1)+fem(:,it,:,2)) )
         enddo
         do it=1,grid%d
           fne(1+it,:) = fne(1+it,:) + & 
                         matmul(n,0.5_wp*(ht(:,:,1)+ht(:,:,2)))
         enddo  
       endif

     endif viscous_flow_if
     if(with_stat) cycle internal_side_do

     ! include the quad. weights
     do iv=1,size(uuu,1) ! loop on the variables
       fne(iv,:) = wgsa*fne(iv,:)
     enddo

     !------------------------------------------------------------------

     !------------------------------------------------------------------
     !3) Compute the boundary integrals
     tnd = 0.0_wp ! side/element tendencies
     do ie=1,2 ! loop on the two elements of the side
       do l=1,base%ms ! loop on the quad. nodes
         do iv=1,size(uuu,1) ! loop on the variables
           tnd(iv,:,ie) = tnd(iv,:,ie) + fne(iv,l)*pb(:,l,ie)
         enddo
       enddo
     enddo
     !------------------------------------------------------------------

     !------------------------------------------------------------------
     !4) Add the new tendencies
     do ie=1,2
       tens(:,:,ie_s(ie)) = tens(:,:,ie_s(ie)) - sigma(ie)*tnd(:,:,ie)
     enddo
     !------------------------------------------------------------------

   enddo internal_side_do


   boundary_side_do: do is=grid%ni+1,grid%ns
     if(with_stat) then
       if(sc%s2ss(is)%ns.eq.0) cycle boundary_side_do
     endif

     !------------------------------------------------------------------
     !1) Preliminary computations

     ! element of the side
     ie_s(1) = grid%s(is)%ie(1)

     ! side local indexes on the element
     is_loc(1) = grid%s(is)%isl(1)

     ! base function evaluations on the side quad. nodes
       ! index of the element -> side permutation (perm. pi)
       i_perm = grid%e(ie_s(1))%pi(is_loc(1)) 
       pb(:,:,1) = & ! reorder on second index (side quad. node)
         base%pb(:, & ! first index unchanged
          base%stab(i_perm,:), & ! second index: reordered with pi
          is_loc(1)) ! third index: local side

     n = grid%e(ie_s(1))%n(:,is_loc(1))

     ! rescale the quadrature weights
     wgsa = (grid%s(is)%a/base%me%voldm1) * base%wgs

     !------------------------------------------------------------------

     !------------------------------------------------------------------
     !2) Compute the numerical fluxes (include quad. weights)

     !2.1) evaluate the solution in the quadrature nodes: the values
     !in the ghost element are defined according to the boundary
     !condition
     uuug(:,:,1) = matmul( uuu(:,:,ie_s(1)) , pb(:,:,1) )
     uuug(:,:,2) = uuu_bcs(:,:,is)
     call side_diags(rho, e, u, ekin, p_p, p, hh, uu, cc, aun, lambda, &
                     is, uuug, grid%d, rgcv, gamma, n, base%ms )

     !2.2) evaluate the normal numerical flux fne
     call side_numflux(fne,grid%d,base%ms,n,u,hh,uu,p_p,cc,lambda,uuug)
     if(with_stat) call update_side_section(sc,matmul(fne,wgsa),is,1)

     ! add the viscous terms
     b_viscous_flow_if: if(viscous_flow) then
       ! No need to call turbmod%flux as for the internal sides
       ! because everything has been computed already and stored in
       ! nflux_bcs.
       if(with_stat) then
         call update_side_section(sc, matmul(nflux_bcs(:,:,is),wgsa), &
                                  is,2)
       else
         fne(2:,:) = fne(2:,:) + nflux_bcs(:,:,is) ! all except density
       endif
     endif b_viscous_flow_if
     if(with_stat) cycle boundary_side_do

     ! include the quad. weights
     do iv=1,size(uuu,1) ! loop on the variables
       fne(iv,:) = wgsa*fne(iv,:)
     enddo

     !------------------------------------------------------------------

     !------------------------------------------------------------------
     !3) Compute the boundary integrals
     tnd(:,:,1) = 0.0_wp ! side/element tendencies
     do l=1,base%ms ! loop on the quad. nodes
       do iv=1,size(uuu,1) ! loop on the variables
         tnd(iv,:,1) = tnd(iv,:,1) + fne(iv,l)*pb(:,l,1)
       enddo
     enddo
     !------------------------------------------------------------------

     !------------------------------------------------------------------
     !4) Add the new tendencies
     tens(:,:,ie_s(1)) = tens(:,:,ie_s(1)) - sigma(1)*tnd(:,:,1)
     !------------------------------------------------------------------

   enddo boundary_side_do

 contains

  pure &
  subroutine side_diags(rho, e, u, ekin, p_p, p, hh, uu, cc, aun, &
    lambda, is, uuug, d, rgcv, gamma, n, ms)
   integer, intent(in) :: is, d, ms
   real(wp), intent(in) :: uuug(:,:,:), rgcv, gamma, n(:)
   real(wp), intent(out) :: rho(:,:), e(:,:), u(:,:,:), ekin(:,:), &
     p_p(:,:), p(:,:), hh(:,:), uu(:,:,:), cc(:,:,:), aun(:,:), lambda(:)

   integer :: ie, id, it, l

    do ie=1,2
      rho(:,ie) = uuug(1,:,ie) + atm_ref_s(:,is)%rho
      e(:,ie)   = uuug(2,:,ie) + atm_ref_s(:,is)%e
      u(:,:,ie) = uuug(2+1:2+d,:,ie)

      ! kinetic energy
      ekin(:,ie) = 0.5_wp * sum( u(:,:,ie)**2 , 1 ) / rho(:,ie)
      ! pressure perturbation
      p_p(:,ie)  = rgcv*( uuug(2,:,ie) &
        - ekin(:,ie) - atm_ref_s(:,is)%phi*uuug(1,:,ie) )
      ! total pressure
      p(:,ie)    = p_p(:,ie) + atm_ref_s(:,is)%p
      ! specific enthalpy
      hh(:,ie)   = (e(:,ie)+p(:,ie))/rho(:,ie)
      ! velocity
      do id=1,d
        uu(id,:,ie) = u(id,:,ie)/rho(:,ie)
      enddo
      ! tracers
      do it=1,ntrcs
        cc(it,:,ie) = uuug(2+grid%d+it,:,ie)/rho(:,ie)
      enddo

      ! Compute a + |u \cdot n|
      aun(:,ie) = sqrt(gamma*p(:,ie)/rho(:,ie)) &
                 + abs( matmul( n , uu(:,:,ie) ) )

    enddo

    do l=1,ms
      lambda(l) = max( aun(l,1) , aun(l,2) )
    enddo

  end subroutine side_diags

  !> Inviscid single valued numerical fluxes
  !!
  !! The numerical fluxes are computed with the Rusanov flux. Notice
  !! that the jump terms are proportional to \f$\lambda\f$, which is
  !! nonzero also when the velocity is zero, because of the positive
  !! sound speed. Thus, this contributes a stabilization terms also in
  !! absence of a mean flow. The net effect is a stabilized flux even
  !! if no \f$c_{11}\f$ term is explicitly introduced in the viscous
  !! numerical fluxes.
  pure &
  subroutine side_numflux(fne,d,ms,n,u,hh,uu,p_p,cc,lambda,uuug)
   integer, intent(in) :: d, ms
   real(wp), intent(in) :: n(:), u(:,:,:), hh(:,:), uu(:,:,:), p_p(:,:), &
     cc(:,:,:), lambda(:), uuug(:,:,:)
   real(wp), intent(out) :: fne(:,:)

   integer :: id, jd, it, iv
   real(wp) :: ff(d,ms,2)

    ! first the centered terms
    ff = u    ! mass
    fne(1,:) = matmul( n , 0.5_wp*(ff(:,:,1)+ff(:,:,2)) )
    do id=1,d ! energy
      ff(id,:,:) = hh*u(id,:,:)
    enddo
    fne(2,:) = matmul( n , 0.5_wp*(ff(:,:,1)+ff(:,:,2)) )
    do jd=1,grid%d ! j-th momentum
      do id=1,grid%d
        ff(id,:,:) = uu(jd,:,:)*u(id,:,:)
      enddo
      ff(jd,:,:) = ff(jd,:,:) + p_p
      fne(2+jd,:) = matmul(n,0.5_wp*(ff(:,:,1)+ff(:,:,2)))
    enddo
    do it=1,ntrcs ! tracers
      do id=1,grid%d
        ff(id,:,:) = cc(it,:,:)*u(id,:,:)
      enddo
      fne(2+grid%d+it,:) = matmul(n,0.5_wp*(ff(:,:,1)+ff(:,:,2)))
    enddo

    ! then the jump terms
    do iv=1,size(uuug,1) ! loop on the variables
      fne(iv,:) = fne(iv,:) + &
        0.5_wp*lambda*(sigma(1)*uuug(iv,:,1)+sigma(2)*uuug(iv,:,2))
    enddo

  end subroutine side_numflux

  !> Update the side statistics
  !!
  !! This function could be moved in mod_sgcomp_statistics and passed
  !! as an argument into this module. This would probably be the best
  !! solution when one has to deal with the many statistics required
  !! by the turbulent models. However, this is a first implementation.
  !!
  !! In each register, the statistics are arranged as: inviscid mean;
  !! inviscid fluctuations; viscous mean; viscous fluctuations.
  pure &
  subroutine update_side_section(sc,nflux,is,flux_type)
   real(wp), intent(in) :: nflux(:) !< integrated side normal flux
   integer, intent(in) :: is !< side index
   integer, intent(in) :: flux_type !< 1 for inviscid, 2 for viscous
   type(t_section_collection), intent(inout), target :: sc

   integer :: i, nflx, i1m, i2m, i1f, i2f
   real(wp) :: snflux(size(nflux))
   real(wp), pointer :: rnew(:), rold(:)

    if(flux_type.eq.1) then
      i1m = 1
    else
      i1m = 2*size(uuu,1)+1 ! taken from the containing subroutine
    endif
    nflx = size(nflux)
    i2m = i1m + nflx - 1
    i1f = i2m + 1
    i2f = i1f + nflx - 1
    do i=1,sc%s2ss(is)%ns
      snflux = sc%s2ss(is)%p(i)*nflux ! set the sign
      rnew => sc%s2ss(is)%s(i)%p%regs(i1m:i2m,1)
      rnew = rnew + snflux
      rold => sc%s2ss(is)%s(i)%p%regs(i1m:i2m,2)
      rnew => sc%s2ss(is)%s(i)%p%regs(i1f:i2f,1)
      rnew = rnew + (snflux-rold)**2
    enddo
  end subroutine update_side_section

 end subroutine tens_side_loop
 
!-----------------------------------------------------------------------

 pure &
 !$ subroutine omp_dummy_5(); end subroutine omp_dummy_5
 subroutine grad_side_loop(grad,turbmod,grid,base,uuu)
  class(c_turbmod), intent(in) :: turbmod
  type(t_grid),     intent(in) :: grid
  type(t_base),     intent(in) :: base
  real(wp),         intent(in) :: uuu(:,:,:)
  real(wp),         intent(inout) :: grad(:,:,:,:)
 
  integer :: is, ie_s(2), is_loc(2), ie, i_perm, iv, i
  real(wp) :: uue(size(grad,2),base%ms,2), uus(size(grad,2),base%ms), &
    pb(base%pk,base%ms), gmd(base%pk,base%ms,2),       &
    a_ns_db(grid%m), gq(base%pk)
 
   !$omp parallel do schedule(static) &
   !$omp   private( is , ie , ie_s , is_loc , i_perm , pb , uue , uus , &
   !$omp            gmd , a_ns_db , iv , gq , i ) &
   !$omp   shared( grid , base , uuu , atm_ref_s , gammad , grad ) &
   !$omp   default(none)
   internal_side_do: do is=1,grid%ni

     !------------------------------------------------------------------
     !1) Preliminary computations

     ! the two elements of the side
     ie_s = grid%s(is)%ie

     ! side local indexes on the two elements
     is_loc = grid%s(is)%isl

     do ie=1,2 ! loop on the two elements of the side

       ! index of the element -> side permutation (perm. pi)
       i_perm = grid%e(ie_s(ie))%pi(is_loc(ie))

       ! base function evaluations on the side quad. nodes
       pb(:,:) = & ! reorder on second index (side quad. node)
         base%pb(:, & ! first index unchanged
          base%stab(i_perm,:), & ! second index: reordered with pi
          is_loc(ie)) ! third index: local side

       ! evaluate the solution in the quadrature nodes
       call turbmod%compute_grad_diags( uue(:,:,ie),   &
         matmul(uuu(:,:,ie_s(ie)),pb), atm_ref_s(:,is) )

       ! side coefficients gamma
       gmd(:,:,ie) = gammad(:, & ! same permutation as for pb
          base%stab(i_perm,:), is_loc(ie))

     enddo
     ! LDG (or Bassi-Rebay) numerical flux
     uus = 0.5_wp*(uue(:,:,1)+uue(:,:,2))
     !------------------------------------------------------------------

     !------------------------------------------------------------------
     !2) Compute the numerical fluxes
     do ie=1,2
       a_ns_db = (grid%s(is)%a/grid%e(ie_s(ie))%det_b) &
                      * grid%e(ie_s(ie))%n(:,is_loc(ie))
       do iv=1,size(grad,2) ! variable loop
         gq = matmul( gmd(:,:,ie) , uus(iv,:) )
         do i=1,base%pk
           !$omp critical
           grad(:,iv,i,ie_s(ie)) = grad(:,iv,i,ie_s(ie)) + &
                                   a_ns_db * gq(i)
           !$omp end critical
         enddo
       enddo
     enddo
     !------------------------------------------------------------------

   enddo internal_side_do
   !$omp end parallel do

   !$omp parallel do schedule(static) &
   !$omp   private( is , ie_s , is_loc , i_perm , pb , uue , uus , &
   !$omp            gmd , a_ns_db , iv , gq , i ) &
   !$omp   shared( grid , base , uuu , atm_ref_s , gammad , &
   !$omp           grad , uuu_bcs ) &
   !$omp   default(none)
   boundary_side_do: do is=grid%ni+1,grid%ns

     !------------------------------------------------------------------
     !1) Preliminary computations

     ! the element of the side
     ie_s(1) = grid%s(is)%ie(1)

     ! side local index on the element
     is_loc(1) = grid%s(is)%isl(1)

       ! index of the element -> side permutation (perm. pi)
       i_perm = grid%e(ie_s(1))%pi(is_loc(1))

       ! base function evaluations on the side quad. nodes
       pb(:,:) = & ! reorder on second index (side quad. node)
         base%pb(:, & ! first index unchanged
          base%stab(i_perm,:), & ! second index: reordered with pi
          is_loc(1)) ! third index: local side

       ! evaluate the solution in the quadrature nodes
       call turbmod%compute_grad_diags( uue(:,:,1),   &
         matmul(uuu(:,:,ie_s(1)),pb), atm_ref_s(:,is) )

       ! side coefficients gamma
       gmd(:,:,1) = gammad(:, & ! same permutation as for pb
          base%stab(i_perm,:), is_loc(1))

       ! boundary datum
       call turbmod%compute_grad_diags( uue(:,:,2),   &
         uuu_bcs(:,:,is), atm_ref_s(:,is) )

     ! LDG (or Bassi-Rebay) numerical flux
     uus = 0.5_wp*(uue(:,:,1)+uue(:,:,2))
     !------------------------------------------------------------------

     !------------------------------------------------------------------
     !2) Compute the numerical fluxes
       a_ns_db = (grid%s(is)%a/grid%e(ie_s(1))%det_b) &
                      * grid%e(ie_s(1))%n(:,is_loc(1))
       do iv=1,size(grad,2) ! variable loop
         gq = matmul( gmd(:,:,1) , uus(iv,:) )
         do i=1,base%pk
           !$omp critical
           grad(:,iv,i,ie_s(1)) = grad(:,iv,i,ie_s(1)) + &
                                   a_ns_db * gq(i)
           !$omp end critical
         enddo
       enddo
     !------------------------------------------------------------------

   enddo boundary_side_do
   !$omp end parallel do

 end subroutine grad_side_loop
 
!-----------------------------------------------------------------------
 
 !> Prefetch the boundary data
 !!
 !! Fill the array \c uuu_bcs collecting the appropriate values either
 !! from the prescribed boundary conditions or, for domain
 !! decomposition sides, from the <tt>ddc\%nd\%fecbuf</tt> module
 !! buffers.
 !!
 !! \note This subroutines reads data from the module variable \c ddc,
 !! which thus <em>must be suitably set before calling</em>.
 !!
 !! \warning The lower bound of the input argument uuu_bcs is set to
 !! <tt>grid%ni+1</tt>, in analogy with the module variable with the
 !! same name.
 !<
 pure &
 !$ subroutine omp_dummy_6(); end subroutine omp_dummy_6
 subroutine update_bcs(uuu_bcs,grid,base,bcs,uuu,err)
  type(t_grid),      intent(in) :: grid
  type(t_base),      intent(in) :: base
  type(t_bcs),       intent(in) :: bcs
  real(wp),          intent(in) :: uuu(:,:,:)
  real(wp),          intent(out) :: uuu_bcs(:,:,grid%ni+1:)
  type(t_bcs_error), intent(out) :: err
 
  integer :: is, ie_s, is_loc, i_perm
  real(wp) :: u_r(2,base%ms), pb(base%pk,base%ms), &
    uuui(size(uuu,1),base%ms), n_bnd(grid%m,base%ms)
  character(len=*), parameter :: &
    this_sub_name = 'update_bcs'

   !$omp parallel do schedule(static) &
   !$omp   private( is , ie_s , is_loc , i_perm , pb , u_r , n_bnd , &
   !$omp            uuui ) &
   !$omp   shared( grid , base , bcs , uuu , atm_ref_s , uuu_bcs , &
   !$omp           ddc , err , coeff_dir , coeff_norm ) &
   !$omp   default(none)
   boundary_side_do: do is=grid%ni+1,grid%ns

     ! we need to reconstruct the internal value (see the details in
     ! tens_side_loop)
     ie_s   = grid%s(is)%ie(1)
     is_loc = grid%s(is)%isl(1)
     i_perm = grid%e(ie_s)%pi(is_loc) 
     pb = base%pb( : , base%stab(i_perm,:) , is_loc )
     uuui = matmul( uuu(:,:,ie_s) , pb )

     bcs_type: select case(bcs%b_s2bs(is)%p%bc)

      case(b_dir)
       u_r(1,:) = atm_ref_s(:,is)%rho
       u_r(2,:) = atm_ref_s(:,is)%e
       dirichlet_type: select case(bcs%b_s2bs(is)%p%btype)
        case(1)
         ! weakly imposed bcs for all variables
         uuu_bcs(:,:,is) = coeff_dir( affmap(grid%s(is),base%xigs) , &
                                u_r = u_r , breg = -grid%s(is)%ie(2) )
        case(2)
         ! This is a combination of strongly imposed no-flux bcs and
         ! weakly imposed Dirichlet bcs as in case(1).
         ! More in details:
         ! 1) the density is taken form the internal element (symmetry
         ! condition)
         ! 2) the velocity is obtained taking the reflected normal
         ! component of the internal velocity and the tangential
         ! component of the Dirichlet datum (see dirnofl_vect)
         ! 3) the energy is taken form the Dirichlet bcs
         ! 4) tracers are treated as density
         !
         ! These conditions result in zero mass and tracer fluxes,
         ! because the normal velocity in the numerical flux is zero
         ! and the jump is also zero, so there are no penalization
         ! contributions.
         !
         ! The optional argument u_i is passed to allow tuning the bcs
         ! on the present state, which is not completely prescribed by
         ! the boundary condition (in particular, rho, which is used
         ! to define the energy corresponding to a fixed temperature,
         ! is determined by the no-flux condition)
         uuu_bcs(:,:,is) = coeff_dir( affmap(grid%s(is),base%xigs) , &
                   u_r = u_r , u_i = uuui , breg = -grid%s(is)%ie(2) )
         uuu_bcs(1,:,is) = uuui(1,:) ! symmetric
         !uuu_bcs(2,:,is) = already set to coeff_dir
         ! combine uuui and coeff_dir
         uuu_bcs(2+1:2+grid%d,:,is) = dirnofl_vect(          &
           grid%e(ie_s)%n(:,is_loc) , uuui(2+1:2+grid%d,:) , &
           uuu_bcs(2+1:2+grid%d,:,is) )
         uuu_bcs(2+grid%d+1:,:,is) = uuui(2+grid%d+1:,:) ! symmetric
        case default
         !$omp critical
         err%lerr = .true.
         write(err%message,'(a,i10,a,i10,a)') &
      "Unknown boundary type ",bcs%b_s2bs(is)%p%btype," on side ",is,"."
         !$omp end critical
       end select dirichlet_type

      case(b_neu)
       uuu_bcs(1,:,is) = uuui(1,:) ! symmetric
       uuu_bcs(2,:,is) = uuui(2,:) ! symmetric
       uuu_bcs(3+grid%d:,:,is) = uuui(3+grid%d:,:) ! symmetric
       neumann_type: select case(bcs%b_s2bs(is)%p%btype)
        case(1) ! no-flux, the mesh normal is used
         uuu_bcs(2+1:2+grid%d,:,is) = reflect_vect(          &
             grid%e(ie_s)%n(:,is_loc) , uuui(2+1:2+grid%d,:) )
        case(2) ! no-flux, user defined normal (immersed boundary)
         n_bnd = coeff_norm( affmap(grid%s(is),base%xigs) , &
                             -grid%s(is)%ie(2) )
         ! simple consistency check on the side normal computation
         if(any(abs(n_bnd).gt.1.1_wp)) then
           !$omp critical
           err%lerr = .true.
           write(err%message,'(a,i10,a,i10,a)') &
             "Problems computing the boundary normal on side ",is, &
             ", region ",-grid%s(is)%ie(2),"."
           !$omp end critical
           uuu_bcs = 0.0_wp
         else
           uuu_bcs(2+1:2+grid%d,:,is) = reflect_vect( n_bnd , &
                                          uuui(2+1:2+grid%d,:) )
         endif
        case(3) ! free flux (zero normal derivative)
         uuu_bcs(2+1:2+grid%d,:,is) = uuui(2+1:2+grid%d,:)
        case default
         !$omp critical
         err%lerr = .true.
         write(err%message,'(a,i10,a,i10,a)') &
      "Unknown boundary type ",bcs%b_s2bs(is)%p%btype," on side ",is,"."
         !$omp end critical
       end select neumann_type

      case(b_ddc)
       is_loc = ddc%s2nd(1,is) ! used as temporary
       i_perm = ddc%s2nd(2,is) ! used as temporary
       uuu_bcs(:,:,is) = ddc%nd( is_loc )%recbuf(:,:, i_perm )

     end select bcs_type

   enddo boundary_side_do
   !$omp end parallel do
 
 end subroutine update_bcs
 
!-----------------------------------------------------------------------

 !> Prefetch the <em>gradient</em> boundary data
 !!
 !! Fill the array \c nflux_bcs collecting the appropriate values either
 !! from the prescribed boundary conditions or, for domain
 !! decomposition sides, from the <tt>ddc\%nd\%recbuf</tt> module
 !! buffers.
 !!
 !! \note This subroutines reads data from the module variable \c ddc,
 !! which thus <em>must be suitably set before calling</em>.
 !!
 !! \warning The lower bound of the input argument nflux_bcs is
 !! set to <tt>grid%ni+1</tt>, in analogy with the module variable
 !! with the same name.
 !!
 !! For type two Dirichlet boundary conditions, the following
 !! construction is used to determine the partially reflected momentum
 !! flux. Given the tensor momentum flux \f$F_{ij}\f$, where the
 !! \f$i\f$-th row is the momentum flux in the \f$i\f$-th direction,
 !! the flux of momentum in the normal direction is
 !! \f{displaymath}{
 !!  \underline{F}_n = \underline{n}^T F
 !! \f}
 !! while the flux of momentum in the tangential direction is
 !! \f{displaymath}{
 !!  \underline{F}_t = \underline{t}^T F
 !! \f}
 !! for any tangent vector \f$\underline{t}\f$. Here, given the
 !! internal value \f$F\f$, we define the boundary flux \f$F^b\f$ such
 !! that
 !! \f{displaymath}{
 !!  \underline{F}_n^b = -\underline{F}_n, \qquad
 !!  \underline{F}_t^b = \underline{F}_t,
 !! \f}
 !! corresponding to a Dirichlet condition in the tangential direction
 !! and a no-flux condition in the normal one. The resulting flux
 !! tensor is
 !! \f{displaymath}{
 !!  F^b = \left[ \mathcal{I} -
 !!   2\underline{n}\otimes\underline{n} \right] F
 !! \f}
 !! (see also an analogous relation for vectors in \c
 !! reflect_vect_1n), and the associated normal flux is
 !! \f{displaymath}{
 !!  F^b\underline{n} = \left[ \mathcal{I} -
 !!   2\underline{n}\otimes\underline{n} \right] (F\underline{n}).
 !! \f}
 pure &
 !$ subroutine omp_dummy_7(); end subroutine omp_dummy_7
 subroutine update_fluxbcs(nflux_bcs,grid,base,bcs,uuu , &
                           turbmod,progs,diags)
  type(t_grid), intent(in) :: grid
  type(t_base), intent(in) :: base
  type(t_bcs),  intent(in) :: bcs
  real(wp),     intent(in) :: uuu(:,:,:)
  class(c_turbmod),       intent(in) :: turbmod
  class(c_turbmod_progs), intent(in), allocatable :: progs
  class(c_turbmod_diags), intent(in) :: diags
  real(wp),     intent(out) :: nflux_bcs(:,:,grid%ni+1:)
 
  integer :: is, ie_s, is_loc, l, id, i
  real(wp) :: u_r(2,base%ms), fb(grid%d,dfu_size,base%ms)
  character(len=*), parameter :: &
    this_sub_name = 'update_fluxbcs'

   !$omp parallel do schedule(static) &
   !$omp   private( is , ie_s , is_loc , l, u_r, fb ) &
   !$omp   shared( grid , base , bcs , atm_ref_s , nflux_bcs , &
   !$omp           coeff_neu , uuu , grad , phc , ddc ) &
   !$omp   default(none)
   boundary_side_do: do is=grid%ni+1,grid%ns

     ie_s = grid%s(is)%ie(1)
     is_loc = grid%s(is)%isl(1)
     bcs_type: select case(bcs%b_s2bs(is)%p%bc)

      ! For Dirichlet bcs we read the numerical flux from the boundary
      ! element (see tens_side_loop for details)
      case(b_dir)
       call elemside_viscflux( fb , ie_s , is_loc ,                    &
                      uuu(:,:,ie_s) , grid , base , turbmod,progs,diags)
       dirichlet_type: select case(bcs%b_s2bs(is)%p%btype)
        case(1)
         do l=1,base%ms
           ! energy, momentum and tracers
           nflux_bcs(:,l,is)=matmul(grid%e(ie_s)%n(:,is_loc),fb(:,:,l))
         enddo
        case(2)
         ! The internal flux is copied in the boundary flux for energy
         ! and tangential momentum, while a no-flux condition (i.e.
         ! minus the internal flux) is enforced for the normal
         ! momentum (thus including the pressure term) and the tracers
         do l=1,base%ms
           ! energy: same as case(1)
           nflux_bcs(1,l,is) = & ! copared to case(1), use dot_product
                        dot_product(grid%e(ie_s)%n(:,is_loc),fb(:,1,l))
           ! momentum
           nflux_bcs(1+1:1+grid%d,l:l,is) = reflect_vect(         &
                                    grid%e(ie_s)%n(:,is_loc),     &
             reshape( matmul( grid%e(ie_s)%n(:,is_loc),           &
                      fb(:,1+1:1+grid%d,l) ) , (/ grid%m , 1 /) ) )
           ! tracers: change of sign compraed to case(1)
           nflux_bcs(1+grid%d+1:,l,is) = - matmul(                    &
                          grid%e(ie_s)%n(:,is_loc),fb(:,1+grid%d+1:,l))
         enddo
       end select dirichlet_type

      case(b_neu)
       u_r(1,:) = atm_ref_s(:,is)%rho
       u_r(2,:) = atm_ref_s(:,is)%e
       ! boundary fluxes
       fb = coeff_neu( affmap(grid%s(is),base%xigs) , &
                 u_r = u_r , breg = -grid%s(is)%ie(2) )
       do l=1,base%ms
         nflux_bcs(:,l,is) = matmul(grid%e(ie_s)%n(:,is_loc),fb(:,:,l))
       enddo

      case(b_ddc)
       ! we have to compute the complete numerical flux: Bassi and
       ! Rebay flux (this must be consistent with tens_side_loop)
       id = ddc%s2nd(1,is)
       i  = ddc%s2nd(2,is)
       ! energy, momentum and tracers
       nflux_bcs(:,:,is) = 0.5_wp * ( ddc%nd(id)%fecbuf(:,:,i) + &
                                      ddc%nd(id)%focbuf(:,:,i) )

     end select bcs_type

   enddo boundary_side_do
   !$omp end parallel do
 
 end subroutine update_fluxbcs
 
!-----------------------------------------------------------------------
 
 !> Viscous fluxes on the side of one element (at the quad nodes)
 !!
 !! \note These are the <em>internal</em> fluxes, prior to the
 !! computation of any numerical flux.
 pure &
 subroutine elemside_viscflux(vf,ie,isl,uue,grid,base, &
                           turbmod,progs,diags)
  integer,      intent(in) :: ie, isl
  real(wp),     intent(in) :: uue(:,:) ! local solution
  type(t_grid), intent(in) :: grid
  type(t_base), intent(in) :: base
  class(c_turbmod),       intent(in) :: turbmod
  class(c_turbmod_progs), intent(in), allocatable :: progs
  class(c_turbmod_diags), intent(in) :: diags
  real(wp),     intent(out) :: vf(:,:,:)
 
  integer :: is, i_perm, id, it
  real(wp) :: pb(base%pk,base%ms), uueg(size(uue,1),base%ms), &
    rho(base%ms), u(grid%d,base%ms), uu(grid%d,base%ms),      &
    ekin(base%ms), p_p(base%ms), p(base%ms), cc(ntrcs,base%ms)
   ! get the side index
   is = grid%e(ie)%is(isl)

   ! diagnose the local solution on the side quad. nodes
   i_perm = grid%e(ie)%pi(isl)
   pb = base%pb( : , base%stab(i_perm,:) , isl )
   uueg = matmul( uue , pb )
   rho = uueg(1,:) + atm_ref_s(:,is)%rho
   u   = uueg(2+1:2+grid%d,:)
   do id=1,grid%d
     uu(id,:) = u(id,:)/rho
   enddo
   ! kinetic energy
   ekin = 0.5_wp * sum( u**2 , 1 ) / rho
   ! pressure perturbation
   p_p  = (phc%rgas/phc%cv) &
          * ( uueg(2,:) - ekin - atm_ref_s(:,is)%phi*uueg(1,:) )
   ! total pressure
   p    = p_p + atm_ref_s(:,is)%p

   ! tracers
   do it=1,ntrcs
     cc(it,:) = uueg(2+grid%d+it,:)/rho
   enddo
   
   ! compute the nodal numerical fluxes
   call turbmod%flux(vf, ie,affmap(grid%s(is),base%xigs),pb,grid, &
                     uueg, atm_ref_s(:,is), rho,p,uu,cc , progs,  &
                     diags, uuu_mean=uuu_mean(:,:,ie),            &
                     tau_mean=tau_mean_side(:,:,:,is,1),          &
                     uu_square_mean=uu_square_mean_s(:,:,:,is,1))

 end subroutine elemside_viscflux
 
!-----------------------------------------------------------------------
 
 pure &
 subroutine side_diags_mean(rho, e, u, ekin, p_p, p, uu, cc, &
                            is, uuug, d, rgcv)
  integer, intent(in) :: is, d 
  real(wp), intent(in) :: uuug(:,:,:), rgcv
  real(wp), intent(out) :: rho(:,:), e(:,:), u(:,:,:), ekin(:,:), &
    p_p(:,:), p(:,:), uu(:,:,:), cc(:,:,:)

  integer :: ie, id, it, l

   do ie=1,2
     rho(:,ie) = uuug(1,:,ie) + atm_ref_s(:,is)%rho
     e(:,ie)   = uuug(2,:,ie) + atm_ref_s(:,is)%e
     u(:,:,ie) = uuug(2+1:2+d,:,ie)

     ! kinetic energy
     ekin(:,ie) = 0.5_wp * sum( u(:,:,ie)**2 , 1 ) / rho(:,ie)
     ! pressure perturbation
     p_p(:,ie)  = rgcv*( uuug(2,:,ie) &
       - ekin(:,ie) - atm_ref_s(:,is)%phi*uuug(1,:,ie) )
     ! total pressure
     p(:,ie)    = p_p(:,ie) + atm_ref_s(:,is)%p
     ! velocity
     do id=1,d
       uu(id,:,ie) = u(id,:,ie)/rho(:,ie)
     enddo
     ! tracers
     do it=1,ntrcs
       cc(it,:,ie) = uuug(2+d+it,:,ie)/rho(:,ie)
     enddo
   enddo

 end subroutine side_diags_mean

!-----------------------------------------------------------------------

 !> Compute the Courant number.
 !!
 !! This is a small utility which computes the maximum Courant number
 !! \f{displaymath}{
 !!  C = \frac{u\Delta t}{h}.
 !! \f}
 !! Two numbers are computed: in one case the velocity is the sum of
 !! the sound speed and the advective velocity \f$|\underline{u}|\f$,
 !! while in the second case only the advective velocity is
 !! considered; in both cases, the element shape is taken into account
 !! as documented in \c el_courant.
 !!
 !! The Courant number are computed for each element, and the
 !! advective one is computed using the both equivalent definitions
 !! given in \c el_courant.
 pure &
 subroutine compute_courant(c_tot,c_adv,grid,base,dt,uuu)
  type(t_grid), intent(in) :: grid
  type(t_base), intent(in) :: base
  real(wp), intent(in) :: dt, uuu(:,:,:)
  real(wp), intent(out) :: c_tot(:), c_adv(:,:)
 
  integer :: ie, id
  real(wp) :: uuug(2+grid%d,base%m), rho, u(grid%d), uu(grid%d), &
    ekin, p_p, p,a, rgcv, gamma, wg0(base%m)
  character(len=*), parameter :: &
    this_sub_name = 'compute_courant'

   rgcv = phc%rgas / phc%cv
   gamma = phc%gamma
   wg0 = base%wg/grid%me%vol ! scaled quad. weights
   elem_do: do ie=1,grid%ne

     !------------------------------------------------------------------
     !1) Solution in physical space (ignoring tracers)
     uuug = matmul( uuu(:2+grid%d,:,ie) , base%p )

     ! from now on, compute element mean values
     rho  = dot_product(uuug(1,:) + atm_ref_e(:,ie)%rho , wg0)
     u    =      matmul(uuug(3:,:)                      , wg0)

     uu   = u/rho ! velocity
     ekin = 0.5_wp * rho * sum( uu**2 ) ! kinetic energy
     ! pressure perturbation
     p_p  = rgcv*( dot_product(                                       &
              uuug(2,:) - atm_ref_e(:,ie)%phi*uuug(1,:) , wg0) - ekin )
     ! total pressure
     p    = p_p + dot_product(atm_ref_e(:,ie)%p         , wg0)
     ! sound speed
     a    = sqrt(gamma*p/rho)
     !------------------------------------------------------------------

     !------------------------------------------------------------------
     !2) Element Courant number
     call el_courant(c_adv(1,ie),c_adv(2,ie),c_tot(ie), grid%e(ie) , &
       (/(grid%e(ie)%s(id)%p%a,id=1,grid%d+1)/) , uu , a+sum(abs(uu)))
     !------------------------------------------------------------------

   enddo elem_do
   c_adv = dt*c_adv
   c_tot = dt*c_tot
 
 end subroutine compute_courant
 
!-----------------------------------------------------------------------

 !> Reflect a vector \f$\underline{v}\f$ with respect to a surface
 !! with normal \f$\underline{n}\f$.
 !!
 !! To construct the reflected vector, we first represent the given
 !! vector as
 !! \f{equation}{
 !! \underline{v} = \underline{n}\otimes\underline{n}\,\underline{v}
 !!   + \left[ \underline{v} -
 !!   \underline{n}\otimes\underline{n}\,\underline{v} \right]
 !! \f}
 !! and then change the sign of the first component (the normal
 !! component), yielding
 !! \f{equation}{
 !! \underline{v}^{\mathcal{R}} = \left[ \mathcal{I} -
 !! 2\underline{n}\otimes\underline{n} \right] \underline{v}.
 !! \f}
 pure &
 function reflect_vect_1n(n,v) result(vr)
  real(wp), intent(in) :: n(:), v(:,:)
  real(wp) :: vr(size(v,1),size(v,2))
 
  integer :: i, j
  real(wp) :: non(size(n),size(n))

   do j=1,size(n)
     do i=1,size(n)
       non(i,j) = n(i)*n(j)
     enddo
   enddo

   vr = v - 2.0_wp*matmul(non,v)

 end function reflect_vect_1n
 
!-----------------------------------------------------------------------

 !> As \c reflect_vect but with a space dependent normal.
 pure &
 function reflect_vect_var(n,v) result(vr)
  real(wp), intent(in) :: n(:,:), v(:,:)
  real(wp) :: vr(size(v,1),size(v,2))

  integer :: i, j, l
  real(wp) :: non(size(n,1),size(n,1))

   do l=1,size(n,2)
     do j=1,size(n,1)
       do i=1,size(n,1)
         non(i,j) = n(i,l)*n(j,l)
       enddo
     enddo
     vr(:,l) = v(:,l) - 2.0_wp*matmul(non,v(:,l))
   enddo

 end function reflect_vect_var
 
!-----------------------------------------------------------------------

 !> This function is similar to \c reflect_vect_1n, however it reflects
 !! only the normal component, while it uses a prescribed value for
 !! the tangential one.
 !!
 !! In practice, given \f$\underline{v}\f$ and the prescribed value
 !! \f$\underline{v}_b\f$, we set
 !! \f{displaymath}{
 !! \underline{v}^{\mathcal{R}} =  \underline{v}_b -
 !! \underline{n}\otimes\underline{n} \left[ \underline{v} +
 !! \underline{v}_b\right],
 !! \f}
 !! so that
 !! \f{displaymath}{
 !! \underline{v}^{\mathcal{R}}\cdot\underline{n} = -
 !! \underline{v}\cdot\underline{n}, \qquad
 !! \underline{v}^{\mathcal{R}}\cdot\underline{t} =
 !! \underline{v}_b\cdot\underline{t}.
 !! \f}
 pure &
 function dirnofl_vect(n,v,vb) result(vr)
  real(wp), intent(in) :: n(:), v(:,:), vb(:,:)
  real(wp) :: vr(size(v,1),size(v,2))
 
  integer :: i, j
  real(wp) :: non(size(n),size(n))

   do j=1,size(n)
     do i=1,size(n)
       non(i,j) = n(i)*n(j)
     enddo
   enddo

   vr = vb - matmul(non,v+vb)

 end function dirnofl_vect

!-----------------------------------------------------------------------

 subroutine update_time_step(n)
  integer, intent(in) :: n
   nstep = real(n,wp)  
 end subroutine update_time_step

!----------------------------------------------------------------------
! This subroutine compute the running time average for the solution, 
! with the data sent by dg_comp.f90, and for the subgrid stress tensor.
! This is used only by hybrid method.
! TODO create a separate module for update mean, in order to extract 
! this subroutine from  mod_dgcomp_rhs



 subroutine update_mean(uuu_n,grid,base,bcs, turbmod, &
                        progs, diags)
  real(wp),               intent(in) :: uuu_n(:,:,:) 
  type(t_grid),           intent(in) :: grid
  type(t_base),           intent(in) :: base
  type(t_bcs),            intent(in) :: bcs
  class(c_turbmod),       intent(in), allocatable :: turbmod  
  class(c_turbmod_progs), intent(in), allocatable :: progs  
  class(c_turbmod_diags), intent(inout), allocatable :: diags  
   
   real(wp) :: uu_n(grid%d,base%pk,grid%ne)
   real(wp) :: rho_g(base%m),wg0(base%m)
   real(wp) :: rgcv, rho_hf
   integer  :: ie,i_pk,i,j,is,it,id
   type(t_bcs_error) :: err
   !tau_mean_elem & uu_square_mean_e
   real(wp) :: uu_square_e(grid%d,grid%d,base%m) 
   real(wp) :: uuug_e(size(uuu_n,1),base%m), rho_e(base%m),          & 
               e_e(base%m), u_e(grid%d,base%m), ekin_e(base%m),      &
               p_p_e(base%m), p_e(base%m), uu_e(grid%d,base%m),      &
               cc_e(ntrcs,base%m), fem_les_e(grid%d,dfu_size,base%m)
   !tau_mean_side & uu_square_mean_s
   real(wp) :: uu_square_s(grid%d,grid%d,base%ms)
   integer  :: ie_s(2),is_loc(2),i_perm
   real(wp) :: pb(base%pk,base%ms,2),                               & 
               uuug_s(size(uuu_n,1),base%ms,2), rho_s(base%ms,2),   &
               e_s(base%ms,2), u_s(grid%d,base%ms,2),               &
               ekin_s(base%ms,2), p_p_s(base%ms,2), p_s(base%ms,2), &
               uu_s(grid%d,base%ms,2),             &
               cc_s(ntrcs,base%ms,2),                &
               fem_les_s(grid%d,dfu_size,base%ms,2)
   !tau_mean_side_boundary
   real(wp) :: uu_square_sb(grid%d,grid%d,base%ms)
   integer  :: i_perm_b, ie_sb, is_loc_b
   real(wp) :: uuug_sb(size(uuu_n,1),base%ms),pb_sb(base%pk,base%ms),&
               rho_sb(base%ms), u_sb(grid%d,base%ms), &  
               ekin_sb(base%ms), p_p_sb(base%ms), p_sb(base%ms), &  
               uu_sb(grid%d,base%ms), cc_sb(ntrcs,base%ms), &
               fb(grid%d,dfu_size,base%ms)



!debug
   real(wp):: uuu_mean_g(grid%d,base%m)
   real(wp):: vel(grid%d)
    !Compute uuu_mean
    uuu_mean = uuu_mean*(nstep-1)/nstep + uuu_n/nstep



    !-------------------------------------------------------------------- 
    ! Compute tau_mean_elem & tau_mean_side                    
                    
  !Gradients
   
  !0) Update flow state
  call update_flowstate(uuu_n,base,grid) 
  if(ddc%ddc) call bside_send(uuu_n) 
  

  !1) Initialization
  diags%grad = 0.0_wp
  !--------------------------------------------------------------------   
                                                                       
  !--------------------------------------------------------------------   
  !2) Gradient element loop                                           
  call grad_elem_loop(diags%grad,turbmod,grid,base,uuu_n)                    
  !--------------------------------------------------------------------   
  
  !--------------------------------------------------------------------
  if(ddc%ddc) call bside_recv()  
  call update_bcs(uuu_bcs,grid,base,bcs,uuu_n,err)
  
  call grad_side_loop(diags%grad,turbmod,grid,base,uuu_n)  

  call turbmod%compute_coeff_diags(diags, grid, base, uuu_n, &
                               progs, atm_ref_e)                   
   
  !--------------------------------------------------------------------   
  
  !1) Evaluate the solution at the quadrature nodes
   rgcv = phc%rgas / phc%cv
   do ie=1,grid%ne
 
     uuug_e = matmul( uuu_n(:,:,ie) , base%p )
     rho_e = uuug_e(1,:) + atm_ref_e(:,ie)%rho
     e_e   = uuug_e(2,:) + atm_ref_e(:,ie)%e
     u_e  = uuug_e(2+1:2+grid%d,:)

     ! kinetic energy
     ekin_e = 0.5_wp * sum( u_e**2 , 1 ) / rho_e
     ! pressure perturbation
     p_p_e  = rgcv*(uuug_e(2,:) - ekin_e - atm_ref_e(:,ie)%phi* &
              uuug_e(1,:))
     ! total pressure
     p_e    = p_p_e + atm_ref_e(:,ie)%p
     ! velocity
     do id=1,grid%d
       uu_e(id,:) = u_e(id,:)/rho_e
     enddo
     ! tracers
     do it=1,ntrcs
       cc_e(it,:) = uuug_e(2+grid%d+it,:)/rho_e
     enddo
  
     do i=1,grid%d
       do j=1,grid%d 
         uu_square_e(i,j,:) = uu_e(i,:)*uu_e(j,:)
       enddo
     enddo
     uu_square_mean_e(:,:,:,ie) = uu_square_mean_e(:,:,:,ie)*(nstep-1)/&
                                  nstep + uu_square_e/nstep
   !2) Compute flux
     call turbmod%flux(fem_les_e, ie,affmap(grid%e(ie),base%xig), &
                        base%p,grid,uuug_e,atm_ref_e(:,ie),rho_e,p_e, &
                        uu_e, cc_e, progs,diags, & 
                        uuu_mean=uuu_mean(:,:,ie))
   !3) Update running time average                       
   
     tau_mean_elem(:,:,:,ie) = tau_mean_elem(:,:,:,ie)*(nstep &
                             -1.0_wp) /nstep + fem_les_e(:,2:,:) &
                              /nstep
   enddo
  !--------------------------------------------------------------------  
   
   
  ! Internal sides
  do is=1,grid%ni
    
     !1) Preliminary computations

     ! the two elements of the side
     ie_s = grid%s(is)%ie

     ! side local indexes on the two elements
     is_loc = grid%s(is)%isl

     ! base function evaluations on the side quad. nodes
     do ie=1,2 ! loop on the two elements of the side
       ! index of the element -> side permutation (perm. pi)
       i_perm = grid%e(ie_s(ie))%pi(is_loc(ie))
       pb(:,:,ie) = & ! reorder on second index (side quad. node)
         base%pb(:, & ! first index unchanged
         base%stab(i_perm,:), & ! second index: reordered with pi
         is_loc(ie)) ! third index: local side
     enddo
     
     !2) Evaluate the solution in the quadrature nodes
     do ie=1,2
       uuug_s(:,:,ie) = matmul( uuu_n(:,:,ie_s(ie)) , pb(:,:,ie) )
     enddo
     call side_diags_mean(rho_s, e_s, u_s, ekin_s, p_p_s, p_s, uu_s, &
                          cc_s, is, uuug_s, grid%d, rgcv)
                    
     !3) Compute flux and update running time average
     do ie=1,2 
       
       do i=1,grid%d
         do j=1,grid%d 
           uu_square_s(i,j,:) = uu_s(i,:,ie)*uu_s(j,:,ie)
         enddo
       enddo

       uu_square_mean_s(:,:,:,is,ie) = uu_square_mean_s(:,:,:,is,ie)* &
                                       (nstep-1)/ nstep + uu_square_s &
                                       /nstep
 
       call turbmod%flux( fem_les_s(:,:,:,ie),                    &
                ie_s(ie),affmap(grid%s(is),base%xigs),pb(:,:,ie), &
                grid, uuug_s(:,:,ie),atm_ref_s(:,is),             &
                rho_s(:,ie),p_s(:,ie),uu_s(:,:,ie),cc_s(:,:,ie),  &
                progs, diags, uuu_mean=uuu_mean(:,:,ie_s(ie)) )
     

        tau_mean_side(:,:,:,is,ie) = tau_mean_side(:,:,:,is,ie)* &
                                     (nstep -1.0_wp)/nstep +     &
                                     fem_les_s(:,2:,:,ie)/nstep 
  
     enddo   
  enddo
  
  ! Boundary sides
  
   do is=grid%ni+1,grid%ns
    
     ie_sb = grid%s(is)%ie(1)
     is_loc_b = grid%s(is)%isl(1)
     i_perm_b = grid%e(ie)%pi(is_loc_b)
     pb_sb = base%pb(:, base%stab(i_perm_b,:), is_loc_b)
     uuug_sb = matmul(uuu_n(:,:,ie_sb),pb_sb)
     rho_sb = uuug_sb(1,:) + atm_ref_s(:,is)%rho
     u_sb  = uuug_sb(2+1:2+grid%d,:)

     do id=1,grid%d
       uu_sb(id,:) = u_sb(id,:)/rho_sb
     enddo

     ! kinetic energy
     ekin_sb = 0.5_wp * sum( u_sb**2 , 1 ) / rho_sb

     ! pressure perturbation
     p_p_sb  = (phc%rgas/phc%cv) &
              * ( uuug_sb(2,:) - ekin_sb - atm_ref_s(:,is)%phi* &
              uuug_sb(1,:) )

     ! total pressure 
     p_sb    = p_p_sb + atm_ref_s(:,is)%p

     ! tracers
     do it=1,ntrcs
       cc_sb(it,:) = uuug_sb(2+grid%d+it,:)/rho_sb
     enddo
       
     do i=1,grid%d
       do j=1,grid%d 
         uu_square_sb(i,j,:) = uu_sb(i,:)*uu_sb(j,:)
       enddo
     enddo

     uu_square_mean_s(:,:,:,is,1) = uu_square_mean_s(:,:,:,is,1)*  &
                                   (nstep-1)/ nstep + uu_square_sb &
                                   /nstep

     call turbmod%flux(fb,ie_sb,affmap(grid%s(is),base%xigs),pb_sb,&
                       grid,uuug_sb,atm_ref_s(:,is),rho_sb,p_sb,   &
                       uu_sb,cc_sb, progs, diags,                  &
                       uuu_mean=uuu_mean(:,:,ie_sb))
                        

     tau_mean_side(:,:,:,is,1) = tau_mean_side(:,:,:,is,1)* &
                                (nstep -1.0_wp)/nstep  + &
                                fb(:,2:,:)/nstep 
   enddo
   
 end subroutine update_mean

end module mod_dgcomp_rhs

