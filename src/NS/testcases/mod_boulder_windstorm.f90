!>\brief
!!
!! Nonlinear, breaking mountain wave.
!!
!! \n
!!
!! This is the reference test case described in <a
!! href="http://journals.ametsoc.org/doi/pdf/10.1175/1520-0493%282000%29128%3C0901%3AAIOMPW%3E2.0.CO%3B2">[Doyle
!! et al., Monthly Weather Review, 2000]</a>.
!!
!! For the initial and boundary conditions, the experimental profiles
!! are used for \f$T\f$ and \f$u\f$, while the pressure profile is
!! computed by the hydrostatic balance:
!! \f{displaymath}{
!!  p(z) = p_s e^{\displaystyle -\frac{g}{R}\int_0^z
!!  \frac{d\zeta}{T(\zeta)}}.
!! \f}
!! Integrating the piecewise linear temperature profile we obtain:
!! \f{displaymath}{
!!  p(z) = p_k e^{\displaystyle -\frac{g}{R}
!!  \int_{z_k}^{z}
!!    \left[ T_k \frac{z_{k+1}-\zeta}{z_{k+1}-z_k}
!!         + T_{k+1} \frac{\zeta-z_k}{z_{k+1}-z_k} \right]^{-1}d\zeta
!!         } , \qquad z\in[z_k\,,\,z_{k+1}].
!! \f}
!! From this we have
!! \f{displaymath}{
!!  p(z) =
!!  \left\{\begin{array}{ll}
!!  \displaystyle
!!   p_k \left[
!!   \frac{(T_{k+1}-T_k)z+T_kz_{k+1}-T_{k+1}z_k}{T_k(z_{k+1}-z_k)}
!!   \right]^{\displaystyle 
!!     -\frac{g}{R}\frac{z_{k+1}-z_k}{T_{k+1}-T_k}}, &
!!   T_{k+1}\neq T_k \\[4mm]
!!   p_k e^{\displaystyle -\frac{g}{RT_k}(z-z_k)} \left[
!!   \displaystyle 1 +
!!   \frac{g(z-z_k)^2}{2RT_k^2(z_{k+1}-z_k)}(T_{k+1}-T_k) \right] , &
!!   T_{k+1}\to T_k.
!!  \end{array}\right.
!! \f}
!<----------------------------------------------------------------------
module mod_boulder_windstorm_test

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_fu_manager, only: &
   mod_fu_manager_initialized, &
   new_file_unit

 use mod_physical_constants, only: &
   mod_physical_constants_initialized, &
   t_phc

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_boulder_windstorm_test_constructor, &
   mod_boulder_windstorm_test_destructor,  &
   mod_boulder_windstorm_test_initialized, &
   test_name,        &
   test_description, &
   phc,              &
   ntrcs,            &
   ref_prof, t_ref,  &
   coeff_dir,        &
   coeff_neu,        &
   coeff_norm,       &
   coeff_sponge,     &
   coeff_visc,       &
   coeff_init,       &
   coeff_outref

 private

!-----------------------------------------------------------------------

 ! public interface
 character(len=*), parameter   :: test_name = "boulder-windstorm"
 character(len=100), protected :: test_description(2)
 type(t_phc), save, protected  :: phc
 integer, parameter            :: ntrcs = 1
 character(len=*), parameter   :: ref_prof = "isothermal"
 real(wp), target              :: t_ref

 logical, protected :: mod_boulder_windstorm_test_initialized = .false.
 ! private members
 real(wp), parameter :: &
   zu0(43,2) = reshape( (/ &
    ! velocity data
    0.00_wp ,  8.920860_wp , &
 1057.69_wp ,  8.920860_wp , &
 1778.85_wp , 11.223000_wp , &
 2211.54_wp , 13.669100_wp , &
 2403.85_wp , 15.827300_wp , &
 2884.62_wp , 17.697800_wp , &
 3028.85_wp , 21.151100_wp , &
 3413.46_wp , 24.748200_wp , &
 3557.69_wp , 27.194200_wp , &
 3750.00_wp , 29.064700_wp , &
 4134.62_wp , 30.935300_wp , &
 4567.31_wp , 32.805800_wp , &
 4807.69_wp , 33.812900_wp , &
 5288.46_wp , 35.107900_wp , &
 5721.15_wp , 36.546800_wp , &
 6586.54_wp , 42.446000_wp , &
 7740.38_wp , 36.690600_wp , &
 8846.15_wp , 31.798600_wp , &
 9855.77_wp , 31.654700_wp , &
10048.10_wp , 33.812900_wp , &
10240.40_wp , 35.683500_wp , &
10769.20_wp , 38.417300_wp , &
10961.50_wp , 35.251800_wp , &
11153.80_wp , 31.654700_wp , &
11346.20_wp , 29.496400_wp , &
12067.30_wp , 33.381300_wp , &
12644.20_wp , 18.129500_wp , &
13221.20_wp , 18.705000_wp , &
14519.20_wp , 30.071900_wp , &
15336.50_wp , 26.906500_wp , &
15528.80_wp , 25.179900_wp , &
15817.30_wp , 22.302200_wp , &
16201.90_wp , 20.863300_wp , &
16442.30_wp , 20.863300_wp , &
17596.20_wp , 14.388500_wp , &
18894.20_wp , 12.661900_wp , &
19951.90_wp ,  6.474820_wp , &
20336.50_wp ,  2.589930_wp , &
20721.20_wp ,  0.431655_wp , &
22115.40_wp ,  2.733810_wp , &
23750.00_wp ,  1.726620_wp , &
24759.60_wp ,  7.050360_wp , &
25000.00_wp ,  7.194240_wp   &
    ! end velocity data
       /) , shape(zu0) , order=(/2,1/) )
 real(wp), parameter :: &
   zt0(40,2) = reshape( (/ &
    ! temperature data
    0.00_wp , 272.275_wp , &
  433.53_wp , 273.227_wp , &
  770.71_wp , 269.913_wp , &
 1252.41_wp , 266.126_wp , &
 1589.60_wp , 264.233_wp , &
 2023.12_wp , 260.209_wp , &
 2408.48_wp , 257.132_wp , &
 3420.04_wp , 251.928_wp , &
 3660.89_wp , 252.404_wp , &
 4046.24_wp , 249.801_wp , &
 4335.26_wp , 249.330_wp , &
 5154.14_wp , 244.599_wp , &
 5780.35_wp , 239.154_wp , &
 6743.74_wp , 232.054_wp , &
 8477.84_wp , 216.905_wp , &
 9200.39_wp , 211.225_wp , &
 9730.25_wp , 207.438_wp , &
10067.40_wp , 206.257_wp , &
10452.80_wp , 206.023_wp , &
10838.20_wp , 206.975_wp , &
10982.70_wp , 213.137_wp , &
11319.80_wp , 213.614_wp , &
11801.50_wp , 212.434_wp , &
12042.40_wp , 212.673_wp , &
12668.60_wp , 216.471_wp , &
13246.60_wp , 212.448_wp , &
14547.20_wp , 206.536_wp , &
16233.10_wp , 213.424_wp , &
16281.30_wp , 219.823_wp , &
16714.80_wp , 218.879_wp , &
17581.90_wp , 218.650_wp , &
18352.60_wp , 215.340_wp , &
18834.30_wp , 217.951_wp , &
19123.30_wp , 217.243_wp , &
19845.90_wp , 222.226_wp , &
20664.70_wp , 218.916_wp , &
21580.00_wp , 224.612_wp , &
22206.20_wp , 223.433_wp , &
23699.40_wp , 223.447_wp , &
25000.00_wp , 220.142_wp   &
    ! end temperature data
       /) , shape(zt0) , order=(/2,1/) )
 real(wp) :: p0(size(zt0,1))

 real(wp) :: &
   nu,     & ! kinematic viscosity
   ! mountain geometry
   hm, a, x0, &
   ! sponge layers
   zd,    & ! begin of the upper damping layer
   zt,    & ! domain top
   alpha, & ! gravity wave damping
   dv,    & ! vertical thickness acoustic damping
   dh,    & ! horizontal thickness acoustic damping
   x1(2), x2(2) ! lateral domain boundaries
 character(len=*), parameter :: &
   test_input_file_name = 'boulder_windstorm.in'
 character(len=*), parameter :: &
   this_mod_name = 'mod_boulder_windstorm_test'

 ! Input namelist
 namelist /input/ &
   nu, hm, a, x0, zd, zt, alpha, dv, dh, x1, x2

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_boulder_windstorm_test_constructor(d)
  integer, intent(in) :: d

  integer :: fu, i, ierr
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.)   .or. &
       (mod_kinds_initialized.eqv..false.)      .or. &
       (mod_fu_manager_initialized.eqv..false.) .or. &
       (mod_physical_constants_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_boulder_windstorm_test_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   ! Read input file
   call new_file_unit(fu,ierr)
   open(fu,file=trim(test_input_file_name), &
      status='old',action='read',form='formatted',iostat=ierr)
    if(ierr.ne.0) call error(this_sub_name,this_mod_name, &
      'Problems opening the input file')
    read(fu,input)
   close(fu,iostat=ierr)

   write(test_description(1),'(a,i1)') &
     'Windstorm test, dimension d = ',d
   write(test_description(2),'(a,e9.3,a)') &
     '  viscosity nu = ',nu,'.'

   ! All the physical constants use the default values: phc unchanged

   t_ref = 300.0_wp

   ! Initialize the module array
   p0(1) = phc%p_s
   do i=2,size(p0)
     p0(i:i) = pint(zt0(i:i,1),zt0(:,1),p0,zt0(:,2))
   enddo

   mod_boulder_windstorm_test_initialized = .true.
 end subroutine mod_boulder_windstorm_test_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_boulder_windstorm_test_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_boulder_windstorm_test_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_boulder_windstorm_test_initialized = .false.
 end subroutine mod_boulder_windstorm_test_destructor

!-----------------------------------------------------------------------

  !> Viscosity
  pure function coeff_visc(x,rho,p,t) result(mu)
   real(wp), intent(in) :: x(:,:), rho(:), p(:), t(:)
   real(wp) :: mu(2,size(x,2))

    mu = nu
  end function coeff_visc

!-----------------------------------------------------------------------

 !> Hydrostatic atmosphere
 !!
 pure function coeff_init(x,u_r) result(u0)
  real(wp), intent(in) :: x(:,:), u_r(:,:)
  real(wp) :: u0(2+size(x,1)+ntrcs,size(x,2))

  integer :: l
  real(wp) :: r2
  real(wp), dimension(size(x,2)) :: z, p, t, uuh

   z = x(size(x,1),:) ! vertical coordinate
   ! primitive variables
   t   = linint(z,zt0(:,1),zt0(:,2))
   p   = pint(z,zt0(:,1),p0,zt0(:,2))
   uuh = linint(z,zu0(:,1),zu0(:,2)) ! horizontal velocity

   ! translate the result in conservative variables
   u0(1,:) = p/(phc%rgas*t)                                     ! rho
   u0(2,:) = u0(1,:)*(phc%cv*t + 0.5_wp*uuh**2 + phc%gravity*z) ! e
   u0(3,:) = u0(1,:)*uuh                                        ! Ux
   u0(4:2+size(x,1),:) = 0.0_wp                                 ! Uyz
   ! subtract the reference state
   u0(1,:) = u0(1,:) - u_r(1,:) ! rho
   u0(2,:) = u0(2,:) - u_r(2,:) ! e

   ! tracers
   if(ntrcs.gt.0) then
     do l=1,size(x,2)
       r2 = (x(1,l)-x0)**2 + 0.25_wp*(z(l)-zd/2.0_wp)**2
       u0(2+size(x,1)+1,l) = 1.0_wp*exp( -r2/(zd/3.0_wp)**2  )
     enddo
   endif

 end function coeff_init
 
!-----------------------------------------------------------------------

 !> Use the initial condition.
 pure function coeff_outref(x,u_r) result(u0)
  real(wp), intent(in) :: x(:,:), u_r(:,:)
  real(wp) :: u0(2+size(x,1)+ntrcs,size(x,2))

   u0 = coeff_init(x,u_r)
   u0(2+size(x,1)+1,:) = 0.0_wp

 end function coeff_outref

!-----------------------------------------------------------------------

 !> Use the initial condition.
 pure function coeff_dir(x,u_r,u_i,breg) result(ub)
  real(wp), intent(in) :: x(:,:)
  real(wp), intent(in), optional :: u_r(:,:), u_i(:,:)
  integer, intent(in), optional :: breg
  real(wp) :: ub(2+size(x,1)+ntrcs,size(x,2))

   ub = coeff_init(x,u_r)

 end function coeff_dir

!-----------------------------------------------------------------------

 pure function coeff_neu(x,u_r,breg) result(h)
  real(wp), intent(in) :: x(:,:)
  real(wp), intent(in), optional :: u_r(:,:)
  integer, intent(in), optional :: breg
  real(wp) :: h(size(x,1),1+size(x,1)+ntrcs,size(x,2))

   h = 0.0_wp

 end function coeff_neu

!-----------------------------------------------------------------------

 !> Normal to the boundary
 !!
 !! \warning This function should be only called for the bottom
 !! boundary.
 !!
 !! In 2D, we have
 !! \f{equation}{
 !!  y(x) = \frac{h_m}{1+\left(\frac{x-x_0}{a}\right)^2}
 !! \f}
 !! so that
 !! \f{equation}{
 !!  \tilde{\underline{n}}(x) = 
 !!   \left[\begin{array}{c} \frac{dy}{dx}(x) \\\ -1 \end{array} \right]
 !!   = \left[\begin{array}{c} 
 !!    -2\frac{h_m}{a}\frac{\xi}{\left(1+\xi^2\right)^2} \\\ -1
 !!   \end{array} \right]
 !! \f}
 !! where the \f$\tilde{}\f$ indicates that the resulting vector must be
 !! normalized and
 !! \f{equation}{
 !!  \xi = \frac{x-x_0}{a}.
 !! \f}
 !<
 pure function coeff_norm(x,breg) result(nb)
  real(wp), intent(in) :: x(:,:)
  integer, intent(in) :: breg
  real(wp) :: nb(size(x,1),size(x,2))

  integer :: l
  real(wp) :: xi(size(x,2))

   breg_case: select case(breg)
    case(3) ! bottom boundry, 2D
     xi = (x(1,:) - x0)/a
     nb(1,:) = -2.0_wp*hm/a * xi/(1.0_wp+xi**2)**2
     nb(2,:) = -1.0_wp
     do l=1,size(nb,2)
       nb(:,l) = nb(:,l)/sqrt(nb(1,l)**2+nb(2,l)**2)
     enddo
    case default
     nb = huge(1.0_wp) ! this is an error
   end select breg_case

 end function coeff_norm

!-----------------------------------------------------------------------

 pure subroutine coeff_sponge(tmask,tau,taup,x)
  real(wp), intent(in) :: x(:,:)
  logical, intent(out) :: tmask
  real(wp), intent(out) :: tau(:), taup(:)

  real(wp), parameter :: pi_trig = 3.141592653589793_wp, &
    big = huge(1.0_wp)
  integer :: l, i
  real(wp) :: z, zn, d_bnd, coeff

   ! initialization
   tau = 0.0_wp
   taup = 0.0_wp
   tmask = .false.

   do l=1,size(x,2)

     ! upper layer
     z = x(size(x,1),l)
     if(z.gt.zd) then ! upper layer nonzero

       tmask = .true.

       ! gravity wave damping
       zn = (z-zd)/(zt-zd) ! normalized vertical coordinate
       if(zn.le.0.5_wp) then
         tau(l) = tau(l) + alpha/2.0_wp*(1.0_wp-cos(zn*pi_trig))
       else
         tau(l) = tau(l) + alpha/2.0_wp*(1.0_wp+(zn-0.5_wp)*pi_trig)
       endif

       ! acoustic wave damping
       d_bnd = (zt-z)/dv ! normalization
       if(d_bnd.le.3.0_wp) then
         ! tmask already set to .true.
         if(d_bnd.lt.1000.0_wp/big) then
           coeff = big/1000.0_wp
         else
           coeff = (1.0_wp-tanh(d_bnd))/tanh(d_bnd)
         endif
         tau(l)  = tau(l)  + coeff
         taup(l) = taup(l) + coeff
       endif

     endif

     ! lateral sponges (only acoustic wave damping)
     d_bnd = big
     do i=1,size(x,1)-1 ! first d-1 coordinates
       d_bnd = min( d_bnd , min( abs(x(i,l)-x1(i)) , abs(x(i,l)-x2(i)) ) )
     enddo
     d_bnd = d_bnd/dh ! normalization
     if(d_bnd.le.3.0_wp) then
       tmask = .true.
       if(d_bnd.lt.1000.0_wp/big) then
         coeff = big/1000.0_wp
       else
         coeff = (1.0_wp-tanh(d_bnd))/tanh(d_bnd)
       endif
       tau(l)  = tau(l)  + coeff
       taup(l) = taup(l) + coeff
     endif

   enddo
   
 end subroutine coeff_sponge

!-----------------------------------------------------------------------
 
 !> 1D linear interpolation
 !!
 !! It is assumed that \c xn is sorted in increasing order. The first
 !! and last values are extrapolated as constants.
 !<
 pure function linint(x,xn,yn) result(y)
  real(wp), intent(in) :: x(:), xn(:), yn(:)
  real(wp) :: y(size(x))

  integer :: nn, i, k
  real(wp) :: fdx(size(xn)-1), q
 
   nn = size(xn)
   fdx = 1.0_wp / ( xn(2:nn) - xn(1:nn-1) )

   do i=1,size(x)
     if(x(i).le.xn(1)) then
       y(i) = yn(1)
     elseif(x(i).ge.xn(nn)) then
       y(i) = yn(nn)
     else
       do k=2,size(xn)
         if( x(i).le.xn(k) ) then
           q = fdx(k-1)*(x(i)-xn(k-1))
           y(i) = yn(k-1)*(1.0_wp-q) + yn(k)*q
           exit
         endif
       enddo
     endif
   enddo
 
 end function linint
 
!-----------------------------------------------------------------------

 !> 1D pressure interpolation
 !!
 !! This function is similar to \c linint, but uses the nonlinear
 !! expression of the pressure.
 !!
 !! \note This function can be used to initialize the array of the
 !! nodal pressure, provided that the first element is set (surface
 !! pressure) and that it is called for increasing nodal vertical
 !! coordinate.
 !<
 pure function pint(z,zn,pn,tn) result(p)
  real(wp), intent(in) :: z(:), zn(:), pn(:), tn(:)
  real(wp) :: p(size(z))

  real(wp), parameter :: dt_toll = 1.0e4_wp*epsilon(1.0_wp)
  integer :: nn, i, k
  real(wp) :: gr, dtn, dzn, dz, coeff1, coeff2
 
   nn = size(zn)
   gr = phc%gravity/phc%rgas

   do i=1,size(z)
     if(z(i).le.zn(1)) then
       p(i) = pn(1)
     elseif(z(i).gt.zn(nn)) then ! isothermal profile
       p(i) = pn(nn) * exp( -gr/tn(nn)*(z(i)-zn(nn)) )
     else
       do k=2,size(zn)
         if( z(i).le.zn(k) ) then
           dz  =  z(i)-zn(k-1)
           dzn = zn(k)-zn(k-1)
           dtn = tn(k)-tn(k-1)
           if(abs(dtn).gt.dt_toll) then
             coeff1 = tn(k)/tn(k-1)
             coeff2 = zn(k)-coeff1*zn(k-1)
             p(i) = pn(k-1) *                                     &
               ( ((coeff1-1.0_wp)*z(i)+coeff2)/dzn )**(-gr*dzn/dtn)
           else
             coeff1 = gr/tn(k-1)
             coeff2 = 0.5_wp*coeff1/tn(k-1)
             p(i) = pn(k-1) * exp( -coeff1*dz )              &
               * ( 1.0_wp + coeff2*dz**2/(zn(k)-zn(k-1))*dtn )
           endif
           exit
         endif
       enddo
     endif
   enddo
 
 end function pint

!-----------------------------------------------------------------------

end module mod_boulder_windstorm_test

