!! Copyright (C) 2015 Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!>\brief
!! Base module for symbolic functions
!!
!! \n
!!
!! This module provides two types and some related operations:
!! <ul>
!!  <li> \c c_symfun is the base type for all the symbolic objects; it
!!  is an abstract type providing the general interface
!!  <li> \c t_funcont is a container type for \c c_symfun objects;
!!  this type is useful in various situations, such as
!!  <ul>
!!   <li> making an array of \c c_symfun objects with different
!!   dynamic types
!!   <li> writing elemental functions where the result is not allowed
!!   to be polymorphic by the standard, still one wants to work with
!!   arbitrary \c c_symfun objects.
!!  </ul>
!! </ul>
!! Since a function container \f$\left[\cdot\right]\f$ naturally
!! defines the function
!! \f{displaymath}{
!!  \left[ f\right](x) = f(x),
!! \f}
!! the type \c t_funcont is itself a \c c_symfun object.
!!
!! \note Before doing anything with a \c c_symfun object, it is
!! advisable to unpack it. In fact, since \c t_funcont objects can in
!! turn contain \c t_funcont entities, care should be paid to avoid an
!! unnecessary deep nesting.
!!
!! \section symfun_interface Interface design
!!
!! The interface defined by this module must conform to the standard,
!! which poses various limitations to the combination of polymorphism
!! and the \c PURE and \c ELEMENTAL attributes. The general motivation
!! for the chosen design in the following.
!! <ol>
!!  <li> All the operations which are part of the interface must be
!!  implemented as type bound operators and procedures, because this
!!  ensure the correct dynamic dispatching. Using generic interfaces to
!!  overload the operations would result in dispatching depending on
!!  the declared type, rather than the dynamic type, which is not what
!!  is needed in a polymorphic implementation.
!!  <li> The passed argument, which controls the dynamic dispatching,
!!  must be polymorphic: in order to have \c PURE and \c ELEMENTAL
!!  methods, the passed argument must have <tt>intent(in)</tt>.
!!  <li> Given that we desire \c PURE and \c ELEMENTAL methods,
!!  function results and <tt>intent(out)</tt> arguments can not
!!  polymorphic; hence, they must be of type \c t_funcont. This has
!!  the nice effect of enabling \c ELEMENTAL methods which operate on
!!  inhomogeneous arrays.
!!  <li> The assignment operator can not be redefined. To see this,
!!  observe that a suitable assignment operator should be at the same
!!  time:
!!  <ul>
!!   <li> \c ELEMENTAL and \c PURE
!!   <li> have the result as passed argument, which thus must be
!!   polymorphic and have <tt>intent(out)</tt>.
!!  </ul>
!! </ol>
!!
!! Given these guidelines, the interface is done as follows.
!! <ol>
!!  <li> All the methods are \c PURE and whenever possible \c
!!  ELEMENTAL.
!!  <li> All the methods return \c t_funcont results.
!!  <li> Conversion of an arbitrary \c c_symfun object into a \c
!!  t_funcont one can be done with the function <tt>\<s|f\>npack</tt>.
!!  \note The subroutine \c snpack does not need to be polymorphic,
!!  and for convenience it is also made available overloading the
!!  assignment operator.
!!  <li> Conversion of a \c t_funcont object into a specific type \c
!!  t_spectype, for some \c t_spectype extending the abstract class,
!!  can not be specified here, and must be provided by the
!!  implementation of \c t_spectype. Typically, this will provide a
!!  function \c to_spectype.
!!  \note As for \c snpack, there is no need for \c to_spectype to be
!!  polymorphic, and it could be overloaded to the assignment operator
!!  with an abstract interface.
!!  <li> Conversion of an arbitrary \c c_symfun object into a specific
!!  type \c t_spectype should be done in two steps:
!!  \code{.f90}
!!   class(c_symfun) :: x
!!   type(t_spectype) :: y
!!
!!   ! Two options:
!!   ! 1) using default assignment for t_spectype
!!   y = to_spectype( fnpack( x ) )
!!   ! 2) provided that to_spectype has been included in assignment(=)
!!   y = fnpack( x )
!!  \endcode
!!  \note The fact that the input argument of \c to_spectype is of
!!  type \c t_funcont and not generically \c c_symfun implies that
!!  this function does not overload the default assignment for \c
!!  t_spectype.
!!  \warning When overloading the assignment operator with an \c
!!  ELEMENTAL subroutine the reallocation-on-assignment is lost,
!!  unless one provides also the operators for the allocatable result
!!  cases.
!!  <li> Error conditions should be set with the
!!  \field_fref{mod_symfun,c_symfun,set_err} method, which can be
!!  configured to produce a runtime error using \c interrupt_on_error.
!!  <li> \anchor setting_c_symfun_flags Whenever setting a field such
!!  as \field_fref{mod_symfun,c_symfun,try_permutation} or
!!  \field_fref{mod_symfun,c_symfun,ierr} of an object contained in \c
!!  t_funcont container, always set the field of the contained object,
!!  and never of the container. Fields set for the container would be
!!  lost when manipulating the container itself in this module.
!!  <li> Since the methods included in the interface return \c
!!  t_funcont object, it might be convenient, in the implementation of
!!  the specific types, to define functions which perform the same
!!  operations on non polymorphic arguments, which would be simpler
!!  and faster. However, these functions can not be included in the
!!  operator interfaces, because they would clash with the type bound
!!  operators.
!! </ol>
!<----------------------------------------------------------------------
module mod_symfun

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   pure_abort, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_symfun_constructor, &
   mod_symfun_destructor,  &
   mod_symfun_initialized, &
   c_symfun, t_funcont, &
   assignment(=),       &
   fnpack, snpack

 private

!-----------------------------------------------------------------------

! Module types and parameters

 logical, parameter :: interrupt_on_error = .false.

 !> Base type for symbolic functions
 type, abstract :: c_symfun
  !> Sometimes an operation is defined only for a given order of the
  !! arguments; in this case, it makes sense permuting the two terms.
  !! However, sometimes an operation is not defined at all, in which
  !! case we don't want to iterate indefinitely back and forth.
  !!
  !! So, before trying swapping the terms of a binary operator, one
  !! should check that this field is <tt>.true.</tt> for its first
  !! argument and set it to <tt>.false.</tt> for its second argument,
  !! so that at most one attempt is made at permuting the terms.
  !!
  !! \note As mentioned \ref setting_c_symfun_flags "here", always set
  !! this field for a contained object, and never for a container.
  logical :: try_permutation = .true.
  !> error indicator
  !!
  !! This should not be set directly, but using the
  !! \field_fref{mod_symfun,c_symfun,set_err} method. This allows
  !! switching easily from setting the error flag and interrupting the
  !! computation.
  !!
  !! \note As mentioned \ref setting_c_symfun_flags "here", always set
  !! this field for a contained object, and never for a container.
  integer :: ierr = 0
 contains
  private
  ! public interface
  generic, public :: operator(+) => add
  generic, public :: operator(-) => sub, minus
  generic, public :: operator(*) => mult, mult_s
  generic, public :: ev    => ev_scal, ev_1d
  generic, public :: pderj => pderj_1, pderj_k
  procedure,            public, pass(f), non_overridable :: set_err
  procedure(i_show),    public, pass(f), deferred        :: show
  ! specific implementations and interfaces
  ! ifort bug: add should be private, see DPD200374263
  ! https://software.intel.com/en-us/forums/topic/563741
  procedure(i_binop),   public, pass(x), deferred :: add
  procedure,            public, pass(x)           :: sub
  procedure,            public, pass(x)           :: minus
  procedure(i_binop),   public, pass(x), deferred :: mult
  procedure(i_mult_s),  public, pass(x), deferred :: mult_s
  procedure(i_ev_scal), pass(f), deferred :: ev_scal
  procedure(i_ev_1d),   pass(f), deferred :: ev_1d
  procedure(i_pderj_1), pass(f), deferred :: pderj_1
  procedure,            pass(f)           :: pderj_k
 end type c_symfun

 !> Function container
 type, extends(c_symfun) :: t_funcont
  class(c_symfun), allocatable :: f
 contains
  private
  ! from c_symfun:
  !   generic, public :: operator(+) => add
  !   generic, public :: operator(-) => sub, minus
  !   generic, public :: operator(*) => mult, mult_s
  !   generic, public :: ev    => ev_scal, ev_1d
  !   generic, public :: pderj => pderj_1, pderj_k
  procedure, public, pass(f) :: show => funcont_show
  ! ifort bug: add should be private, see DPD200374263
  ! https://software.intel.com/en-us/forums/topic/563741
  procedure, public, pass(x) :: add    => funcont_add
  procedure, public, pass(x) :: mult   => funcont_mult
  procedure, public, pass(x) :: mult_s => funcont_mult_s
  procedure, pass(f) :: ev_scal => funcont_ev_scal
  procedure, pass(f) :: ev_1d   => funcont_ev_1d
  procedure, pass(f) :: pderj_1 => funcont_pderj_1
 end type t_funcont
 
 abstract interface
  subroutine i_show(f)
   import :: c_symfun
   implicit none
   class(c_symfun), intent(in) :: f
  end subroutine i_show
 end interface

 abstract interface
  elemental function i_binop(x,y) result(z)
   import :: c_symfun, t_funcont
   implicit none
   class(c_symfun), intent(in) :: x, y
   type(t_funcont) :: z
  end function i_binop
 end interface

 abstract interface
  elemental function i_mult_s(r,x) result(y)
   import :: wp, c_symfun, t_funcont
   implicit none
   real(wp),        intent(in) :: r
   class(c_symfun), intent(in) :: x
   type(t_funcont) :: y
  end function i_mult_s
 end interface

 abstract interface
  pure recursive function i_ev_scal(f,x) result(y)
   import :: wp, c_symfun
   implicit none
   class(c_symfun), intent(in) :: f
   real(wp),        intent(in) :: x(:)
   real(wp) :: y
  end function i_ev_scal

  pure recursive function i_ev_1d(f,x) result(y)
   import :: wp, c_symfun
   implicit none
   class(c_symfun), intent(in) :: f
   real(wp),        intent(in) :: x(:,:)
   real(wp) :: y(size(x,2))
  end function i_ev_1d
 end interface

 abstract interface
  elemental function i_pderj_1(f,j) result(djf)
   import :: c_symfun, t_funcont
   implicit none
   class(c_symfun), intent(in) :: f
   integer,         intent(in) :: j
   type(t_funcont) :: djf
  end function i_pderj_1
 end interface
 
 interface assignment(=)
   module procedure snpack
 end interface

! Module variables
 logical, protected ::               &
   mod_symfun_initialized = .false.
 character(len=*), parameter :: &
   this_mod_name = 'mod_symfun'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_symfun_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
       (mod_kinds_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_symfun_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_symfun_initialized = .true.
 end subroutine mod_symfun_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_symfun_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_symfun_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_symfun_initialized = .false.
 end subroutine mod_symfun_destructor

!-----------------------------------------------------------------------

 elemental function sub(x,y) result(z)
  class(c_symfun), intent(in) :: x, y
  type(t_funcont) :: z

   !z = x + (-1.0_wp)*y
   z = x%add( y%mult_s(-1.0_wp) ) ! ifort compiler bug

 end function sub

!-----------------------------------------------------------------------

 elemental function minus(x) result(y)
  class(c_symfun), intent(in) :: x
  type(t_funcont) :: y

   !y = (-1.0_wp) * x
   y = x%mult_s(-1.0_wp) ! ifort compiler bug

 end function minus
 
!-----------------------------------------------------------------------

 elemental function pderj_k(f,j,k) result(djf)
  class(c_symfun), intent(in) :: f
  integer,         intent(in) :: j, k
  type(t_funcont) :: djf
 
  type(t_funcont) :: df

   if(k.eq.0) then
     djf = f
   elseif(k.gt.0) then
     df  = f%pderj(j)
     djf = df%pderj(j,k-1)
   else ! error: k<0
     djf = f
     call djf%f%set_err( 1 )
   endif

 end function pderj_k
 
!-----------------------------------------------------------------------

 pure subroutine set_err(f,ierr)
  class(c_symfun), intent(inout) :: f
  integer, intent(in) :: ierr

   ! Sometimes it could be simpler to call this subroutine without
   ! knowing whether there is an error or not.
   if(ierr.ne.0) then
     if(interrupt_on_error) then
       call pure_abort()
     else
       f%ierr = ierr
     endif
   endif

 end subroutine set_err

!-----------------------------------------------------------------------

 !> Normalize packaging
 !!
 !! This function returns a \c t_funcont object \c y such that
 !! <tt>y\%f</tt> is not of class \c t_funcont.
 !!
 !! This is useful for two reasons:
 !! <ul>
 !!  <li> package a generic \c c_symfun object into a \c t_funcont one
 !!  <li> remove nested packaging levels from a \c t_funcont object.
 !! </ul>
 !!
 !! \note Two implementations are provided: as subroutine and as
 !! function.
 elemental function fnpack(x) result(y)
  class(c_symfun), intent(in)  :: x
  type(t_funcont) :: y

   y = x ! calling snpack

 end function fnpack

 elemental subroutine snpack(y,x)
  class(c_symfun), intent(in)  :: x
  type(t_funcont), intent(out) :: y

   select type(x)
    type is(t_funcont)
     y = x%f ! equivalent to "call snpack( y , x%f )"
    class default
     allocate( y%f , source=x )
   end select

 end subroutine snpack

!-----------------------------------------------------------------------

 elemental function funcont_add(x,y) result(z)
  class(t_funcont), intent(in) :: x
  class(c_symfun),  intent(in) :: y
  type(t_funcont) :: z

  type(t_funcont) :: xnp, ynp

   ! The key point here is that fnpack guarantees that the contained
   ! object is not of type t_funcont, so here we know that we will
   ! call some other operator(+)
   xnp = fnpack(x)
   ynp = fnpack(y)
   z = xnp%f + ynp%f

 end function funcont_add
 
!-----------------------------------------------------------------------

 elemental function funcont_mult(x,y) result(z)
  class(t_funcont), intent(in) :: x
  class(c_symfun),  intent(in) :: y
  type(t_funcont) :: z

  type(t_funcont) :: xnp, ynp

   xnp = fnpack(x)
   ynp = fnpack(y)
   z = xnp%f * ynp%f

 end function funcont_mult

!-----------------------------------------------------------------------

 elemental function funcont_mult_s(r,x) result(y)
  real(wp),         intent(in) :: r
  class(t_funcont), intent(in) :: x
  type(t_funcont) :: y

  type(t_funcont) :: xnp

   xnp = fnpack(x)
   !y = r * xnp%f
   y = xnp%f%mult_s( r ) ! ifort compiler bug

 end function funcont_mult_s

!-----------------------------------------------------------------------

 pure recursive function funcont_ev_scal(f,x) result(z)
  class(t_funcont), intent(in) :: f
  real(wp),         intent(in) :: x(:)
  real(wp) :: z

   z = f%f%ev(x)
 
 end function funcont_ev_scal
 
!-----------------------------------------------------------------------

 pure recursive function funcont_ev_1d(f,x) result(z)
  class(t_funcont), intent(in) :: f
  real(wp),         intent(in) :: x(:,:)
  real(wp) :: z(size(x,2))

   z = f%f%ev(x)
   
 end function funcont_ev_1d

!-----------------------------------------------------------------------

 elemental function funcont_pderj_1(f,j) result(djf)
  class(t_funcont), intent(in) :: f
  integer,         intent(in) :: j
  type(t_funcont) :: djf

   djf = f%f%pderj(j)

 end function funcont_pderj_1
 
!-----------------------------------------------------------------------

 subroutine funcont_show(f)
  class(t_funcont), intent(in) :: f

   write(*,*) "*** Function container t_funcont containing ***"
   call f%f%show()
   write(*,*) "******** end of the contained function ********"

 end subroutine funcont_show

!-----------------------------------------------------------------------

end module mod_symfun

