!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!> \file
!! Simple test for the octave IO module
program ode_test

 use mod_messages, only: &
   mod_messages_constructor, &
   mod_messages_destructor,  &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_constructor, &
   mod_kinds_destructor,  &
   wp

 use mod_octave_io, only: &
   mod_octave_io_constructor, &
   mod_octave_io_destructor,  &
   write_octave,   &
   read_octave,    &
   read_octave_al

 use mod_fu_manager, only: &
   mod_fu_manager_constructor, &
   mod_fu_manager_destructor,  &
   new_file_unit

 implicit none
 
 character(len=*), parameter :: &
  output_file = "octave_io_test_output_file.octxt", &
  input_file  = "octave_io_test_input_file.octxt",  &
  echo_file   = "octave_io_test_echo_file.octxt"

 integer :: i, fu, fue, ierr

 logical, parameter :: &
   lg(2,1,3,2) = reshape( &
    (/ .true. , .false. , .true. , .false. , .true. , .false. ,   &
       .true. , .false. , .true. , .false. , .true. , .false. /), &
    (/2,1,3,2/) )
 logical, allocatable :: lv(:), lm(:,:), l4(:,:,:,:)

 integer, parameter :: &
   in(2,1,3,1,2) = reshape( (/( i, i=1,2*1*3*1*2 )/) , (/2,1,3,1,2/) )
 integer, allocatable :: ivr(:), ivc(:), im(:,:), i5(:,:,:,:,:)

 real(wp), parameter :: &
   re(2,1,3,1,2) = sqrt(real(in,wp))
 real(wp), allocatable :: rvr(:), rvc(:), rm(:,:), r5(:,:,:,:,:)

 complex(wp), parameter :: &
   cm(2,1,3,1,2) = sqrt(-cmplx(in,in**2,wp))
 complex(wp), allocatable :: cvr(:), cvc(:)

 character(len=4), parameter :: &
   st(2,1,3,1,2) = reshape( &
    (/ "Aa  " , "Ää"   , "Àà"   , "Áá"   , "Ââ"   , "Ȧȧ"   ,   &
       "Bb  " , "Cc  " , "Dd  " , "Ee  " , "Ff  " , "Gg  " /), &
    (/2,1,3,1,2/) )
 character(len=25), allocatable :: sa(:), sa2(:,:)
   

  call mod_messages_constructor()
  call mod_kinds_constructor()
  call mod_octave_io_constructor()
  call mod_fu_manager_constructor()

  !---------------------------------------------------------------------
  ! 1) Let us write some data
  call new_file_unit(fu,ierr)
  open(fu,file=output_file, &
       status='replace',action='write',form='formatted')

  ! 1.1) logical
  call write_octave(lg(1,1,1,1)    ,'lg_s' ,fu)
  call write_octave(lg(:,1,1,1),'c','lg_cv',fu)
  call write_octave(lg(:,1,1,1),'r','lg_rv',fu)
  call write_octave(lg(:,1,:,1)    ,'lg_m' ,fu)
  call write_octave(lg(:,:,:,:)    ,'lg_4' ,fu)

  ! 1.2) integer
  call write_octave(in(1,1,1,1,1)    ,'in_s' ,fu)
  call write_octave(in(:,1,1,1,1),'c','in_cv',fu)
  call write_octave(in(:,1,1,1,1),'r','in_rv',fu)
  call write_octave(in(:,1,:,:,1)    ,'in_3' ,fu)
  call write_octave(in(:,:,:,:,:)    ,'in_5' ,fu)

  ! 1.3) real
  call write_octave(re(1,1,1,1,1)    ,'re_s' ,fu)
  call write_octave(re(:,1,1,1,1),'c','re_cv',fu)
  call write_octave(re(:,1,1,1,1),'r','re_rv',fu)
  call write_octave(re(:,1,:,:,1)    ,'re_3' ,fu)
  call write_octave(re(:,:,:,:,:)    ,'re_5' ,fu)

  ! 1.4) complex
  call write_octave(cm(1,1,1,1,1)    ,'cm_s' ,fu)
  call write_octave(cm(:,1,1,1,1),'c','cm_cv',fu)
  call write_octave(cm(:,1,1,1,1),'r','cm_rv',fu)
  call write_octave(cm(:,1,:,:,1)    ,'cm_3' ,fu)
  call write_octave(cm(:,:,:,:,:)    ,'cm_5' ,fu)

  ! 1.5) character
  call write_octave(st(1,1,1,1,1)    ,'st_s' ,fu)
  call write_octave(st(:,1,1,1,1)    ,'st_v' ,fu) ! no distinction c/r
  call write_octave(st(:,1,:,:,1)    ,'st_3' ,fu)
  call write_octave(st(:,:,:,:,:)    ,'st_5' ,fu)

  ! 1.6) function handles
  call write_octave("function_handle","f1","exp",fu)
  call write_octave("function_handle","f2","<anonymous>",fu, &
                    "(x,y)(exp(x*y))")

  close(fu)
  !---------------------------------------------------------------------

  !---------------------------------------------------------------------
  ! 2) Now read/allocate
  call new_file_unit(fu,ierr)
  open(fu,file=input_file, &
       status='old',action='read',form='formatted')
  call new_file_unit(fue,ierr)
  open(fue,file=echo_file, &
       status='replace',action='write',form='formatted')

  ! 2.1) logical
  call read_octave_al(lv    ,'lv',fu)
  call write_octave(  lv,'r','lv',fue)
  call read_octave_al(lm    ,'lm',fu)
  call write_octave(  lm    ,'lm',fue)
  call read_octave_al(l4    ,'l4',fu)
  call write_octave(  l4    ,'l4',fue)

  ! 2.2) integer
  call read_octave_al(ivr    ,'iv_r',fu)
  call write_octave(  ivr,'r','iv_r',fue)
  call read_octave_al(ivc    ,'iv_c',fu)
  call write_octave(  ivc,'c','iv_c',fue)
  call read_octave_al(im     ,'im'  ,fu)
  call write_octave(  im     ,'im'  ,fue)
  call read_octave_al(i5     ,'im'  ,fu) ! same matrix, as 5D
  call write_octave(  i5     ,'i5'  ,fue)

  ! 2.3) real: use the same data as for integer
  call read_octave_al(rvr    ,'iv_r',fu)
  call write_octave(  rvr,'r','rv_r',fue)
  call read_octave_al(rvc    ,'iv_c',fu)
  call write_octave(  rvc,'c','rv_c',fue)
  call read_octave_al(rm     ,'im'  ,fu)
  call write_octave(  rm     ,'rm'  ,fue)
  call read_octave_al(r5     ,'im'  ,fu)
  call write_octave(  r5     ,'r5'  ,fue)

  ! 2.4) complex
  ! Warning: there could be some issues loading these variables; you
  ! should substitute  Infinity -> Inf
  call read_octave_al(cvr    ,'cv_r',fu)
  call write_octave(  cvr,'r','cv_r',fue)
  call read_octave_al(cvc    ,'cv_c',fu)
  call write_octave(  cvc,'c','cv_c',fue)

  ! 2.5) characters
  call read_octave_al(sa,'s1',fu)
  call write_octave(  sa,'s1',fue)
  deallocate(sa)
  call read_octave_al(sa,'s2',fu)
  call write_octave(  sa,'s2',fue)
  deallocate(sa)
  call read_octave_al(sa,'s3',fu)
  call write_octave(  sa,'s3',fue)
  deallocate(sa)
  call read_octave_al(sa,'s4',fu)
  call write_octave(  sa,'s4',fue)
  deallocate(sa)
  call read_octave_al(sa,'s5',fu)
  call write_octave(  sa,'s5',fue)
  deallocate(sa)
  call read_octave_al(sa,'s6',fu)
  call write_octave(  sa,'s6',fue)
  deallocate(sa)
  call read_octave_al(sa,'s7',fu)
  call write_octave(  sa,'s7',fue)
  deallocate(sa)
  call read_octave_al(sa,'s8',fu)
  call write_octave(  sa,'s8',fue)
  deallocate(sa)
  call read_octave_al(sa,'s9',fu)
  call write_octave(  sa,'s9',fue)
  deallocate(sa)
  call read_octave_al(sa,'s10',fu)
  call write_octave(  sa,'s10',fue)
  deallocate(sa)
  call read_octave_al(sa,'s11',fu)
  call write_octave(  sa,'s11',fue)
  deallocate(sa)
  call read_octave_al(sa2,'s12',fu)
  call write_octave(  sa2,'s12',fue)
  deallocate(sa2)
  !---------------------------------------------------------------------

  !---------------------------------------------------------------------
  ! 3) Read the scalars
  call read_octave( lv(1) ,'ls',fu)
  call write_octave(lv(1) ,'ls',fue)
  call read_octave( ivr(1),'is',fu)
  call write_octave(ivr(1),'is',fue)
  call read_octave( rvr(1),'rs',fu)
  call write_octave(rvr(1),'rs',fue)
  call read_octave( cvr(1),'cs',fu)
  call write_octave(cvr(1),'cs',fue)
  allocate(sa(1))
  call read_octave( sa(1) ,'s13',fu)
  call write_octave(sa(1) ,'s13',fue)
  
  close(fue)
  close(fu)
  !---------------------------------------------------------------------

  call mod_fu_manager_destructor()
  call mod_octave_io_destructor()
  call mod_kinds_destructor()
  call mod_messages_destructor()

end program ode_test
