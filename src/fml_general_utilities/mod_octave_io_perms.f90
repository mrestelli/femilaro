!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!>\brief
!! Octave io for permutation and data permutation objects.
!<----------------------------------------------------------------------
module mod_octave_io_perms

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_octave_io, only: &
   mod_octave_io_initialized, &
   write_octave

 use mod_perms, only: &
   mod_perms_initialized, &
   t_perm, t_dperm

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_octave_io_perms_constructor, &
   mod_octave_io_perms_destructor,  &
   mod_octave_io_perms_initialized, &
   write_octave

 private

!-----------------------------------------------------------------------

! Module types and parameters

! Module variables

 ! public members
 logical, protected ::               &
   mod_octave_io_perms_initialized = .false.
 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_octave_io_perms'

 !> \bug Output of t_dperm objcts not yet implemented
 interface write_octave
   module procedure write_perm, write_perm_1 !, write_dperm
 end interface

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_octave_io_perms_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
       (mod_kinds_initialized.eqv..false.)    .or. &
       (mod_octave_io_initialized.eqv..false.).or. &
       (mod_perms_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_octave_io_perms_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_octave_io_perms_initialized = .true.
 end subroutine mod_octave_io_perms_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_octave_io_perms_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_octave_io_perms_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_octave_io_perms_initialized = .false.
 end subroutine mod_octave_io_perms_destructor

!-----------------------------------------------------------------------

 subroutine write_perm(pi,var_name,fu)
  integer, intent(in) :: fu
  type(t_perm), intent(in) :: pi
  character(len=*), intent(in) :: var_name
 
  character(len=*), parameter :: &
    this_sub_name = 'write_perm'

   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   write(fu,'(a)')      '# length: 3' ! number of fields

   ! field 01 : d
   write(fu,'(a)')      '# name: d'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(pi%d,'<cell-element>',fu)

   ! field 02 : p
   write(fu,'(a)')      '# name: p'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(pi%p,'<cell-element>',fu)

   ! field 03 : i
   write(fu,'(a)')      '# name: i'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(pi%i,'r','<cell-element>',fu)

 end subroutine write_perm

!-----------------------------------------------------------------------

 subroutine write_perm_1(pi,var_name,fu)
  integer, intent(in) :: fu
  type(t_perm), intent(in) :: pi(:)
  character(len=*), intent(in) :: var_name
 
  integer :: i
  character(len=*), parameter :: &
    this_sub_name = 'write_perm'

   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   call write_dims()
   write(fu,'(a)')      '# length: 3' ! number of fields

   ! field 01 : d
   write(fu,'(a)')      '# name: d'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do i=1,size(pi)
     call write_octave(pi(i)%d,'<cell-element>',fu)
   enddo

   ! field 02 : p
   write(fu,'(a)')      '# name: p'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do i=1,size(pi)
     call write_octave(pi(i)%p,'<cell-element>',fu)
   enddo

   ! field 03 : i
   write(fu,'(a)')      '# name: i'
   write(fu,'(a)')      '# type: cell'
   call write_dims()
   do i=1,size(pi)
     call write_octave(pi(i)%i,'r','<cell-element>',fu)
   enddo

 contains

  ! A small subroutine to write the array dimensions for each field.
  ! Octave seems to require at least two dimensions.
  subroutine write_dims()
   write(fu,'(a)')      '# ndims: 2'
   write(fu,'(i0,a,i0)') 1," ",size(pi)
  end subroutine write_dims

 end subroutine write_perm_1

!-----------------------------------------------------------------------

end module mod_octave_io_perms

