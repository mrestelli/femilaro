!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!> Write/read data in the Octave text data format, as used by \c load
!! and \c save in Octave
!!
!! \warning According to the Octave documentation, one should not
!! access data files directly but instead use the Octave library,
!! which has a stable and documented API. Nevertheless, this would
!! require compiling Octave and linking its library, which is
!! sometimes not so obvious. Hence, since the data format is very
!! clean and easy to handle, here we write it ourselves. It could be
!! possible that changes to this module are required when a new Octave
!! release appears, anyway so far this has proven to work very well.
!!
!! For all these subroutines, it is assumed that the file is already
!! open and connected to unit \c fu.
!!
!! In general, this module supports operations for scalars and arrays
!! of the following fortran types: <tt>logical</tt>, <tt>integer</tt>,
!! <tt>real(wp)</tt>, <tt>complex(wp)</tt> and
!! <tt>character(len=*)</tt>.
!!
!! Three operations are provided:
!! <ul>
!!  <li> write a fortran object
!!  <li> read an octave object in an already allocated fortran one
!!  <li> read an octave object in a fortran one which is allocated
!!  with the corresponding dimension.
!! </ul>
!!
!! \section octave_io_ocformats Octave data formats
!!
!! An overview of the octave type system can be found <a
!! href="https://www.gnu.org/software/octave/doc/interpreter/Data-Types.html">here</a>;
!! notice that one can see a list of the currently installed data
!! types typing <tt>typeinfo()</tt>.
!!
!! Here we isolate two aspects of the octave types, which help us
!! finding the proper match for a given frotran type: the
!! corresponding fortran type and the "structure" of the octave
!! object. Here are some example:
!! | octave type  | fortran type  | structure                |
!! | :----------: | :------------ | :----------------------- |
!! | <tt>bool matrix</tt>    | <tt>logical</tt>     | array  |
!! | <tt>complex matrix</tt> | <tt>complex(wp)</tt> | array  |
!! | <tt>int32 scalar</tt>   | <tt>integer</tt>     | scalar |
!! | <tt>scalar</tt>         | <tt>real(wp)</tt>    | scalar |
!! | <tt>matrix</tt>         | <tt>real(wp)</tt>    | array  |
!! where "array" means a fortran array of arbitrary rank. Notice that
!! the octave default type is real. These types are parsed by \c
!! parse_octave_type and mapped to the module parameters
!! <tt>ft_*</tt>; more details are provided in the following \ref
!! octave_io_input "section".
!!
!! Character variables in octave require some care, since one has to
!! deal with:
!! <ul>
!!  <li> single and double quoted strings
!!  <li> strings vs. character arrays
!!  <li> escaping and line termination (see also <a
!!  href="https://www.gnu.org/software/octave/doc/interpreter/Escape-Sequences-in-String-Constants.html">this
!!  page</a>).
!! </ul>
!! Some documentation can be found <a
!! href="https://www.gnu.org/software/octave/doc/interpreter/Strings.html">here</a>.
!!
!! \section octave_io_output Octave output
!!
!! The output uses only a subset of all the possible octave types, due
!! both to a certain redundancy in the octave representation and in
!! the limited number of fortran types considered here. Concerning the
!! fortran type we have:
!! <ul>
!!  <li> <tt>logical</tt>: depending on the value of \c
!!  represent_logicals_as_bool they can be represented as \c bool
!!  octave objects or converted into fortran integers and then saved
!!  <li> <tt>integer</tt>: depending on the value of \c
!!  represent_integers_as_int they can be represented as \c
!!  oct_int_type octave objects or converted into fortran reals and
!!  then saved (see also <a
!!  href="https://www.gnu.org/software/octave/doc/interpreter/Integer-Data-Types.html#Integer-Data-Types">this
!!  page</a> for the possible values of \c oct_int_type)
!!  <li> <tt>real(wp)</tt>: are written without specifying any type,
!!  i.e. either as \c scalar or as \c matrix
!!  <li> <tt>complex(wp)</tt>: are written as \c complex octave
!!  objects.
!!  <li> <tt>character(len=*)</tt>: are written as double quoted
!!  strings; more details are given \ref octave_io_outstring "here".
!! </ul>
!! Concerning the structure of the objects, there is no ambiguity for
!! scalars, while the fortran/octave correspondence is not so nice for
!! arrays. In fact, in octave:
!! <ul>
!!  <li> 1D arrays do not exist: they are represented either as row
!!  matrices or as column matrices, in any case as 2D arrays
!!  <li> 2D arrays are \c matrix objects which have two possible
!!  representations:
!!  <ul>
!!   <li> specifying the numbers of rows and columns (default
!!   behaviour in octave)
!!   <li> specifying the number of dimensions (two) and these two
!!   dimensions (a special case of the \f$n\f$D array case)
!!  </ul>
!!  here we always use the second option
!!  <li> \f$n\f$D arrays with \f$n\geq3\f$ are \c matrix objects
!!  defined indicating the number of dimensions and the values of
!!  these dimensions.
!! </ul>
!! Hence, this module includes three "back-end" functions for the
!! octave output:
!! <ul>
!!  <li> one to write a scalar
!!  <li> one to write an \f$n\f$D array, with \f$n\geq2\f$ 
!!  <li> one to convert 1D arrays into row/column 2D arrays.
!! </ul>
!!
!! \subsection octave_io_outstring Strings
!!
!! String types do not include any "structure", since they are
!! implicitly treated as arrays of characters.  There are two ways in
!! octave to represent a string (regardless of whether is has single
!! of double quotes):
!! <ul>
!!  <li> as a collection of \c elements, each of which has a \c
!!  length: this is suitable for character arrays with (up to) two
!!  dimensions, where the length of the string itself is the first
!!  dimension
!!  <li> specifying \c ndims, the extent of these dimensions and the
!!  characters of the array.
!! </ul>
!! The first method is the default in octave, however in this module
!! we prefer the second one, which is more general, except for scalar
!! fortran string which are easy to recast in the \c element, \c
!! length format. This is very similar to the treatment chosen for
!! numeric arrays.
!!
!! \subsection octave_io_function_handle Function handles
!!
!! It is also possible writing octave function handles; these are
!! treated as special cases compared with the other types, since there
!! is no direct fortran correspondence. More details in \c
!! write_function_handle.
!!
!! \subsection octave_io_outdetail Implementation details
!!
!! In this module, different overloaded subroutines are used for each
!! intrinsic type, such as the <tt>read_scal_*</tt> functions. In
!! principle, one could substitute all these subroutines with a single
!! one, using a <tt>class(*)</tt> argument, as in \c write_scal_impl.
!! The problem with this solution is that this would not allow
!! overloading other subroutines for derived types defined elsewhere
!! in the code, such as, for instance, in \c mod_octave_io_sparse. In
!! fact, the subroutine provided here with the unlimited polymorphic
!! argument would also match any derived data type. For this reason,
!! the implementation is split here in a "front-end" and a "back-end",
!! where the "front-end" is made of overloaded subroutines and does
!! not use polymorphic arguments, while the "back-end" is private and
!! does uses <tt>class(*)</tt> arguments (see \c write_scal_impl, \c
!! write_vect_impl and so on).
!!
!! \section octave_io_input Octave input
!!
!! A fortran object is first classified according to its <tt>\#type:
!! XXX</tt> specification, which is parsed by \c parse_octave_type.
!! This subroutine determines two characteristics of the octave
!! object: the corresponding fortran type, and the object "structure",
!! as anticipated \ref octave_io_ocformats "above".
!!
!! The octave type is detrmined by searching for a specific substring,
!! for instance integer type are defined as containing the substring
!! \c int, which includes \c int8, \c int16, \c uint8, \c uint16 and
!! so on; all these octave types correspond to the single fortran type
!! \c integer. Analogously, the octave structure is identified looking
!! for a substring, for instance \c matrix, which includes \c matrix
!! and \c null_matrix.
!!
!! Octave objects are often "compressed", for instance
!! <ul>
!!  <li> arrays with only one element are stored as \c scalar
!!  <li> the most external dimensions are dropped when they are equal
!!  to 1, for instance a \f$2\times3\times1\f$ array can be stored as
!!  a 2D array, which in turns allows for the two formats discussed in
!!  the previous \ref octave_io_output "section".
!! </ul>
!! To take all of this into account, the reading process is split into
!! two parts:
!! <ul>
!!  <li> a "back-end" subroutine reads an arbitrary octave object and
!!  returns it as a one-dimensional fortran array, passing also the
!!  shape of the octave object
!!  <li> various "front-end" subroutines adjust the information
!!  returned by the "back-end" according to the shape of the fortran
!!  object.
!! </ul>
!! This back/front end implementation simplifies also the type
!! conversions. With this regard, notice that
!! <ul>
!!  <li> numeric types can be converted (this is required because the
!!  octave default behaviour is saving most of the types as real)
!!  <li> a fortran \c logical variable can be used to read both \c
!!  bool and numeric octave types
!!  <li> an octave \c bool variable can be read only in a fortran \c
!!  logical one
!!  <li> the only conversions for character variables are between
!!  octave \c string and fortran \c character.
!! </ul>
!<----------------------------------------------------------------------
module mod_octave_io

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_octave_io_constructor, &
   mod_octave_io_destructor,  &
   mod_octave_io_initialized, &
   write_octave,   &
   read_octave,    &
   read_octave_al, &
   real_format,    &
   locate_var

 private

!-----------------------------------------------------------------------

! Module types and parameters

 ! private members
 character(len=*), parameter :: &
   real_format = 'e24.15e3' ! format: _+0.***************E+***
!   real_format = 'e42.33e3' ! format: _+0.** 33 **E+*** : real(16)

 character(len=*), parameter :: &
   complex_format = '("(",'//real_format//',",",'//real_format//',")")'
 
 logical, parameter :: represent_logicals_as_bool = .true.
 logical, parameter :: represent_integers_as_int  = .true.

 character(len=*), parameter :: oct_int_type = 'int32'

 !> This format writes an arbitrary number of integers on the same
 !! line, using the minimum amount of digits for each of them and
 !! separating them with a single space. The explanation is:
 !! | descriptor   |                                            |
 !! | :----------: | :----------------------------------------- |
 !! | <tt>*</tt>   | unlimited format item (f2008)              |
 !! | <tt>i0</tt>  | as many digits as required, without spaces |
 !! | <tt>:</tt>   | break here when there are no more data     |
 !! | <tt>" "</tt> | insert a white space                       |
 character(len=*), parameter :: ia_fmt = '(*(i0,:," "))'

 ! Some shorthands for the fortran types treated here
 !
 ! Defining these parameters is useful because fortran types can not
 ! be "passed around" easily.
 integer, parameter :: &
   ft_x = -1, & ! error
   ft_l =  1, & ! logical
   ft_i =  2, & ! integer
   ft_r =  3, & ! real(wp)
   ft_z =  4, & ! complex(wp)
   ft_c =  5    ! character with arbitrary length
 
 ! Octave structures
 integer, parameter :: &
   os_x = -1, & ! error
   os_u =  0, & ! undefined
   os_s =  1, & ! scalar
   os_m =  2, & ! matrix
   os_t =  3, & ! struct
   os_c =  4    ! cell

! Module variables

 ! public members
 logical, protected ::               &
   mod_octave_io_initialized = .false.

 interface write_octave
   module procedure write_scal_r, write_vect_r, write_mat_r,  &
                    write_3dim_r, write_4dim_r, write_5dim_r, &
                    write_scal_i, write_vect_i, write_mat_i,  &
                    write_3dim_i, write_4dim_i, write_5dim_i, &
                    write_scal_l, write_vect_l, write_mat_l,  &
                    write_3dim_l, write_4dim_l, write_5dim_l, &
                    write_scal_z, write_vect_z, write_mat_z,  &
                    write_3dim_z, write_4dim_z, write_5dim_z, &
                    write_scal_c, write_vect_c, write_mat_c,  &
                    write_3dim_c, write_4dim_c, write_5dim_c, &
                    write_function_handle
 end interface

 interface read_octave
   module procedure read_scal_r, read_vect_r, read_mat_r,  &
                    read_3dim_r, read_4dim_r, read_5dim_r, &
                    read_scal_i, read_vect_i, read_mat_i,  &
                    read_3dim_i, read_4dim_i, read_5dim_i, &
                    read_scal_l, read_vect_l, read_mat_l,  &
                    read_3dim_l, read_4dim_l, read_5dim_l, &
                    read_scal_z, read_vect_z, &
                    read_scal_c, read_vect_c, read_mat_c
 end interface

 interface read_octave_al
   module procedure read_vect_al_r, read_mat_al_r,                  &
                    read_3dim_al_r, read_4dim_al_r, read_5dim_al_r, &
                    read_vect_al_i, read_mat_al_i,                  &
                    read_3dim_al_i, read_4dim_al_i, read_5dim_al_i, &
                    read_vect_al_l, read_mat_al_l,                  &
                    read_3dim_al_l, read_4dim_al_l, read_5dim_al_l, &
                    read_vect_al_z,              &
                    read_vect_al_c, read_mat_al_c
 end interface

 character(len=*), parameter :: &
   this_mod_name = 'mod_octave_io'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_octave_io_constructor()

  character(len=100) message
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
       (mod_kinds_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_octave_io_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   write(message,'(A,A)') 'Octave output format: ', real_format
   call info(this_sub_name,this_mod_name,message)

   mod_octave_io_initialized = .true.
 end subroutine mod_octave_io_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_octave_io_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_octave_io_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_octave_io_initialized = .false.
 end subroutine mod_octave_io_destructor

!-----------------------------------------------------------------------
 
 subroutine write_scal_r(x,var_name,fu)
  integer, intent(in) :: fu
  real(wp), intent(in) :: x
  character(len=*), intent(in) :: var_name
 
   call write_scal_impl(x,var_name,fu)
  
 end subroutine write_scal_r

!-----------------------------------------------------------------------

 subroutine read_scal_r(x,var_name,fu,norewind)
  integer, intent(in) :: fu
  character(len=*), intent(in) :: var_name
  real(wp), intent(out) :: x
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_scal'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(size(Tx).ne.1) & ! this is not a scalar...
     call error(this_sub_name,this_mod_name,                   &
       'This variable is not a scalar: "'//trim(var_name)//'".')

   ! Tx has the type defined in the octave file; must be copied in x
   select type(Tx)
    type is(complex(wp))
     x = real( Tx(1) , wp )
    type is(real(wp))
     x =       Tx(1)
    type is(integer)
     x = real( Tx(1) , wp )
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)

 end subroutine read_scal_r
 
!-----------------------------------------------------------------------
 
 subroutine write_vect_r(V,rc,var_name,fu)
  integer, intent(in) :: fu
  real(wp), intent(in) :: V(:)
  character(len=*), intent(in) :: rc, var_name

   call write_vect_impl(V,rc,var_name,fu)

 end subroutine write_vect_r
 
!-----------------------------------------------------------------------
 
 subroutine read_vect_al_r(V,var_name,fu,norewind)
  integer, intent(in) :: fu
  real(wp), allocatable, intent(out) :: V(:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   ! Vectors are always treated as matrices with two indexes, so the
   ! simplest solution is reading a matrix and then changing the
   ! number of dimensions. This handles automatically all the required
   ! conversions.

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   ! octave uses many representations for arrays with either one entry
   ! or no entris; a meaningful test is that only one dimension should
   ! be larger than 1
   if(count(dims.gt.1).gt.1) & ! this is not a vector...
     call error(this_sub_name,this_mod_name,                   &
       'This variable is not a vector: "'//trim(var_name)//'".')

   ! Tx has the type defined in the octave file; must be copied in V
   allocate( V(size(Tx)) )
   select type(Tx)
    type is(complex(wp))
     V = real( Tx , wp )
    type is(real(wp))
     V =       Tx
    type is(integer)
     V = real( Tx , wp )
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)

 end subroutine read_vect_al_r
 
!-----------------------------------------------------------------------

 subroutine read_vect_r(V,var_name,fu,norewind)
  integer, intent(in) :: fu
  real(wp), intent(out) :: V(:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  real(wp), allocatable :: Va(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'

   ! Not very efficient, but simpler...
   call read_vect_al_r(Va,var_name,fu,norewind)

   if(size(V).ne.size(Va)) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   V = Va

   deallocate(Va)

 end subroutine read_vect_r
 
!-----------------------------------------------------------------------
 
 subroutine write_mat_r(M,var_name,fu)
  integer, intent(in) :: fu
  real(wp), intent(in) :: M(:,:)
  character(len=*), intent(in) :: var_name

   call write_ndim_impl(reshape(M,(/size(M)/)),shape(M),var_name,fu)
  
 end subroutine write_mat_r

 !! An alternative implementation, assuming (..) is allowed
 !subroutine write_r(t,var_name,fu)
 ! use iso_c_binding, only: c_f_pointer, c_loc
 ! integer, intent(in) :: fu
 ! real(wp), intent(in), target :: t(..)
 ! character(len=*), intent(in) :: var_name

 ! real(wp), pointer :: pt(:)
 !
 !  ! view the array as a 1d object
 !  call c_f_pointer(c_loc(t),pt,(/size(t)/))

 !  ! call the appropriate back-end function
 !  rank_case: select case(rank(t))
 !   case(0) ! scalar
 !    call write_scal_impl(pt(1),var_name,fu)
 !   case(1) ! vector: must specify either column or row
 !    call write_vect_r(pt,                                     &
 !      "*** For vectors, use the 'rc' argument: "              &
 !      //"call write_vect_r(V,rc,var_name,fu) ***", var_name,fu)
 !   case default ! matrix or nd array
 !    call write_ndim_impl(pt,shape(t),var_name,fu)
 !  end select rank_case
 ! 
 !end subroutine write_r
 
!-----------------------------------------------------------------------
 
 subroutine read_mat_al_r(M,var_name,fu,norewind)
  integer, intent(in) :: fu
  real(wp), allocatable, intent(out) :: M(:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer :: ord(2)
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(size(dims).gt.2) &
     call error(this_sub_name,this_mod_name,                   &
       'This variable is not a matrix: "'//trim(var_name)//'".')

   ! An octave object is either a scalar or it has at least two
   ! indexes
   if(size(dims).eq.0) then
     allocate( M(1,1) )
   else
     allocate( M(dims(1),dims(2)) )
   endif

   if(is_transp) then
     ord = (/ 2 , 1 /)
   else
     ord = (/ 1 , 2 /)
   endif

   select type(Tx)
    type is(complex(wp))
     M = real( reshape( Tx , shape(M), order=ord ) , wp )
    type is(real(wp))
     M =       reshape( Tx , shape(M), order=ord )
    type is(integer)
     M = real( reshape( Tx , shape(M), order=ord ) , wp )
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)

 end subroutine read_mat_al_r

!-----------------------------------------------------------------------
 
 subroutine read_mat_r(M,var_name,fu,norewind)
  integer, intent(in) :: fu
  real(wp), intent(out) :: M(:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  real(wp), allocatable :: Ma(:,:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'
 
   call read_mat_al_r(Ma,var_name,fu,norewind)

   if(any(shape(M).ne.shape(Ma))) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   M = Ma

   deallocate(Ma)

 end subroutine read_mat_r
 
!-----------------------------------------------------------------------
 
 ! Unfortunately, an assumed size array T(*), which would cover all
 ! the 3D, 4D, xD cases, would not match the generic interface. Thus,
 ! it would be necessary to call directly the write_multidim
 ! subroutine. The following is nicer, provided we don't want really
 ! large rank arrays.
 subroutine write_3dim_r(T,var_name,fu)
  integer, intent(in) :: fu
  real(wp), intent(in) :: T(:,:,:)
  character(len=*), intent(in) :: var_name

   call write_ndim_impl(reshape(T,(/size(T)/)),shape(T),var_name,fu)
 
 end subroutine write_3dim_r
 
!-----------------------------------------------------------------------
 
 subroutine read_3dim_al_r(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  real(wp), allocatable, intent(out) :: T(:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer :: ord(3)
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(size(dims).gt.3) &
     call error(this_sub_name,this_mod_name,                     &
       'This variable is not a 3D array: "'//trim(var_name)//'".')

   ! An octave object is either a scalar or it has at least two
   ! indexes
   if(size(dims).eq.0) then ! scalar
     allocate( T(1,1,1) )
   elseif(size(dims).eq.2) then ! matrix, implies dims(3)=1
     allocate( T(dims(1),dims(2),1) )
   else
     allocate( T(dims(1),dims(2),dims(3)) )
   endif

   if(is_transp) then ! only if the octave object is a matrix
     ord = (/ 2 , 1 , 3 /)
   else
     ord = (/ 1 , 2 , 3 /)
   endif

   select type(Tx)
    type is(complex(wp))
     T = real( reshape( Tx , shape(T), order=ord ) , wp )
    type is(real(wp))
     T =       reshape( Tx , shape(T), order=ord )
    type is(integer)
     T = real( reshape( Tx , shape(T), order=ord ) , wp )
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)
  
 end subroutine read_3dim_al_r
 
!-----------------------------------------------------------------------

 subroutine read_3dim_r(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  real(wp), intent(out) :: T(:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  real(wp), allocatable :: Ta(:,:,:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'
 
   call read_3dim_al_r(Ta,var_name,fu,norewind)

   if(any(shape(T).ne.shape(Ta))) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   T = Ta

   deallocate(Ta)

 end subroutine read_3dim_r
 
!-----------------------------------------------------------------------

 subroutine write_4dim_r(T,var_name,fu)
  integer, intent(in) :: fu
  real(wp), intent(in) :: T(:,:,:,:)
  character(len=*), intent(in) :: var_name

   call write_ndim_impl(reshape(T,(/size(T)/)),shape(T),var_name,fu)
 
 end subroutine write_4dim_r
 
!-----------------------------------------------------------------------

 subroutine read_4dim_al_r(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  real(wp), allocatable, intent(out) :: T(:,:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer :: ord(4)
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(size(dims).gt.4) &
     call error(this_sub_name,this_mod_name,                     &
       'This variable is not a 4D array: "'//trim(var_name)//'".')

   if(size(dims).eq.0) then ! scalar
     allocate( T(1,1,1,1) )
   elseif(size(dims).eq.2) then ! matrix, implies dims(3:)=1
     allocate( T(dims(1),dims(2),1,1) )
   elseif(size(dims).eq.3) then 
     allocate( T(dims(1),dims(2),dims(3),1) )
   else
     allocate( T(dims(1),dims(2),dims(3),dims(4)) )
   endif

   if(is_transp) then ! only if the octave object is a matrix
     ord = (/ 2 , 1 , 3 , 4 /)
   else
     ord = (/ 1 , 2 , 3 , 4 /)
   endif

   select type(Tx)
    type is(complex(wp))
     T = real( reshape( Tx , shape(T), order=ord ) , wp )
    type is(real(wp))
     T =       reshape( Tx , shape(T), order=ord )
    type is(integer)
     T = real( reshape( Tx , shape(T), order=ord ) , wp )
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)
  
 end subroutine read_4dim_al_r
 
!-----------------------------------------------------------------------

 subroutine read_4dim_r(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  real(wp), intent(out) :: T(:,:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  real(wp), allocatable :: Ta(:,:,:,:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'
 
   call read_4dim_al_r(Ta,var_name,fu,norewind)

   if(any(shape(T).ne.shape(Ta))) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   T = Ta

   deallocate(Ta)

 end subroutine read_4dim_r
 
!-----------------------------------------------------------------------

 subroutine write_5dim_r(T,var_name,fu)
  integer, intent(in) :: fu
  real(wp), intent(in) :: T(:,:,:,:,:)
  character(len=*), intent(in) :: var_name

   call write_ndim_impl(reshape(T,(/size(T)/)),shape(T),var_name,fu)
 
 end subroutine write_5dim_r
 
!-----------------------------------------------------------------------

 subroutine read_5dim_al_r(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  real(wp), allocatable, intent(out) :: T(:,:,:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer :: ord(5)
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(size(dims).gt.5) &
     call error(this_sub_name,this_mod_name,                     &
       'This variable is not a 5D array: "'//trim(var_name)//'".')

   if(size(dims).eq.0) then ! scalar
     allocate( T(1,1,1,1,1) )
   elseif(size(dims).eq.2) then ! matrix, implies dims(3:)=1
     allocate( T(dims(1),dims(2),1,1,1) )
   elseif(size(dims).eq.3) then 
     allocate( T(dims(1),dims(2),dims(3),1,1) )
   elseif(size(dims).eq.4) then 
     allocate( T(dims(1),dims(2),dims(3),dims(4),1) )
   else
     allocate( T(dims(1),dims(2),dims(3),dims(4),dims(5)) )
   endif

   if(is_transp) then
     ord = (/ 2 , 1 , 3 , 4 , 5 /)
   else
     ord = (/ 1 , 2 , 3 , 4 , 5 /)
   endif

   select type(Tx)
    type is(complex(wp))
     T = real( reshape( Tx , shape(T), order=ord ) , wp )
    type is(real(wp))
     T =       reshape( Tx , shape(T), order=ord )
    type is(integer)
     T = real( reshape( Tx , shape(T), order=ord ) , wp )
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)
  
 end subroutine read_5dim_al_r
 
!-----------------------------------------------------------------------

 subroutine read_5dim_r(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  real(wp), intent(out) :: T(:,:,:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  real(wp), allocatable :: Ta(:,:,:,:,:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'
 
   call read_5dim_al_r(Ta,var_name,fu,norewind)

   if(any(shape(T).ne.shape(Ta))) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   T = Ta

   deallocate(Ta)

 end subroutine read_5dim_r

!-----------------------------------------------------------------------

 subroutine write_scal_i(x,var_name,fu)
  integer, intent(in) :: fu
  integer, intent(in) :: x
  character(len=*), intent(in) :: var_name
 
   if(represent_integers_as_int) then
     call write_scal_impl(  x    ,var_name,fu)
   else
     call write_scal_r(real(x,wp),var_name,fu)
   endif

 end subroutine write_scal_i

!-----------------------------------------------------------------------

 subroutine read_scal_i(x,var_name,fu,norewind)
  integer, intent(in) :: fu
  integer, intent(out) :: x
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind
 
  logical :: is_transp
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_scal'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(size(Tx).ne.1) & ! this is not a scalar...
     call error(this_sub_name,this_mod_name,                   &
       'This variable is not a scalar: "'//trim(var_name)//'".')

   ! Tx has the type defined in the octave file; must be copied in x
   select type(Tx)
    type is(complex(wp))
     x = int( Tx(1) )
    type is(real(wp))
     x = int( Tx(1) )
    type is(integer)
     x =      Tx(1)
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)

 end subroutine read_scal_i

!-----------------------------------------------------------------------
 
 subroutine write_vect_i(V,rc,var_name,fu)
  integer, intent(in) :: fu
  integer, intent(in) :: V(:)
  character(len=*), intent(in) :: rc, var_name

   if(represent_integers_as_int) then
     call write_vect_impl(  V    ,rc,var_name,fu)
   else
     call write_vect_r(real(V,wp),rc,var_name,fu)
   endif
  
 end subroutine write_vect_i
 
!-----------------------------------------------------------------------
 
 subroutine read_vect_al_i(V,var_name,fu,norewind)
  integer, intent(in) :: fu
  integer, allocatable, intent(out) :: V(:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(count(dims.gt.1).gt.1) & ! this is not a vector...
     call error(this_sub_name,this_mod_name,                   &
       'This variable is not a vector: "'//trim(var_name)//'".')

   allocate( V(size(Tx)) )
   select type(Tx)
    type is(complex(wp))
     V = int( Tx )
    type is(real(wp))
     V = int( Tx )
    type is(integer)
     V =      Tx
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)

 end subroutine read_vect_al_i
 
!-----------------------------------------------------------------------
 
 subroutine read_vect_i(V,var_name,fu,norewind)
  integer, intent(in) :: fu
  integer, intent(out) :: V(:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  integer, allocatable :: Va(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'

   ! Not very efficient, but simpler...
   call read_vect_al_i(Va,var_name,fu,norewind)

   if(size(V).ne.size(Va)) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   V = Va

   deallocate(Va)

 end subroutine read_vect_i
 
!-----------------------------------------------------------------------
 
 subroutine write_mat_i(M,var_name,fu)
  integer, intent(in) :: fu
  integer, intent(in) :: M(:,:)
  character(len=*), intent(in) :: var_name

   if(represent_integers_as_int) then
     call write_ndim_impl(reshape(M,(/size(M)/)),shape(M),var_name,fu)
   else
     call write_mat_r(       real(M,wp)                  ,var_name,fu)
   endif

 end subroutine write_mat_i
 
!-----------------------------------------------------------------------
 
 subroutine read_mat_al_i(M,var_name,fu,norewind)
  integer, intent(in) :: fu
  integer, allocatable, intent(out) :: M(:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer :: ord(2)
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(size(dims).gt.2) &
     call error(this_sub_name,this_mod_name,                   &
       'This variable is not a matrix: "'//trim(var_name)//'".')

   ! An octave object is either a scalar or it has at least two
   ! indexes
   if(size(dims).eq.0) then
     allocate( M(1,1) )
   else
     allocate( M(dims(1),dims(2)) )
   endif

   if(is_transp) then
     ord = (/ 2 , 1 /)
   else
     ord = (/ 1 , 2 /)
   endif

   select type(Tx)
    type is(complex(wp))
     M = int( reshape( Tx , shape(M), order=ord ) )
    type is(real(wp))
     M = int( reshape( Tx , shape(M), order=ord ) )
    type is(integer)
     M =      reshape( Tx , shape(M), order=ord )
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)

 end subroutine read_mat_al_i
 
!-----------------------------------------------------------------------

 subroutine read_mat_i(M,var_name,fu,norewind)
  integer, intent(in) :: fu
  integer, intent(out) :: M(:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  integer, allocatable :: Ma(:,:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'
 
   call read_mat_al_i(Ma,var_name,fu,norewind)

   if(any(shape(M).ne.shape(Ma))) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   M = Ma

   deallocate(Ma)

 end subroutine read_mat_i
 
!-----------------------------------------------------------------------

 subroutine write_3dim_i(T,var_name,fu)
  integer, intent(in) :: fu
  integer, intent(in) :: T(:,:,:)
  character(len=*), intent(in) :: var_name

   if(represent_integers_as_int) then
     call write_ndim_impl(reshape(T,(/size(T)/)),shape(T),var_name,fu)
   else
     call write_3dim_r(      real(T,wp)                  ,var_name,fu)
   endif

 end subroutine write_3dim_i
 
!-----------------------------------------------------------------------

 subroutine read_3dim_al_i(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  integer, allocatable, intent(out) :: T(:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer :: ord(3)
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(size(dims).gt.3) &
     call error(this_sub_name,this_mod_name,                     &
       'This variable is not a 3D array: "'//trim(var_name)//'".')

   if(size(dims).eq.0) then ! scalar
     allocate( T(1,1,1) )
   elseif(size(dims).eq.2) then ! matrix, implies dims(3)=1
     allocate( T(dims(1),dims(2),1) )
   else
     allocate( T(dims(1),dims(2),dims(3)) )
   endif

   if(is_transp) then ! only if the octave object is a matrix
     ord = (/ 2 , 1 , 3 /)
   else
     ord = (/ 1 , 2 , 3 /)
   endif

   select type(Tx)
    type is(complex(wp))
     T = int( reshape( Tx , shape(T), order=ord ) )
    type is(real(wp))
     T = int( reshape( Tx , shape(T), order=ord ) )
    type is(integer)
     T =      reshape( Tx , shape(T), order=ord )
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)
  
 end subroutine read_3dim_al_i
 
!-----------------------------------------------------------------------

 subroutine read_3dim_i(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  integer, intent(out) :: T(:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  integer, allocatable :: Ta(:,:,:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'
 
   call read_3dim_al_i(Ta,var_name,fu,norewind)

   if(any(shape(T).ne.shape(Ta))) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   T = Ta

   deallocate(Ta)

 end subroutine read_3dim_i
 
!-----------------------------------------------------------------------

 subroutine write_4dim_i(T,var_name,fu)
  integer, intent(in) :: fu
  integer, intent(in) :: T(:,:,:,:)
  character(len=*), intent(in) :: var_name

   if(represent_integers_as_int) then
     call write_ndim_impl(reshape(T,(/size(T)/)),shape(T),var_name,fu)
   else
     call write_4dim_r(      real(T,wp)                  ,var_name,fu)
   endif

 end subroutine write_4dim_i
 
!-----------------------------------------------------------------------

 subroutine read_4dim_al_i(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  integer, allocatable, intent(out) :: T(:,:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer :: ord(4)
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(size(dims).gt.4) &
     call error(this_sub_name,this_mod_name,                     &
       'This variable is not a 4D array: "'//trim(var_name)//'".')

   if(size(dims).eq.0) then ! scalar
     allocate( T(1,1,1,1) )
   elseif(size(dims).eq.2) then ! matrix, implies dims(3:)=1
     allocate( T(dims(1),dims(2),1,1) )
   elseif(size(dims).eq.3) then 
     allocate( T(dims(1),dims(2),dims(3),1) )
   else
     allocate( T(dims(1),dims(2),dims(3),dims(4)) )
   endif

   if(is_transp) then ! only if the octave object is a matrix
     ord = (/ 2 , 1 , 3 , 4 /)
   else
     ord = (/ 1 , 2 , 3 , 4 /)
   endif

   select type(Tx)
    type is(complex(wp))
     T = int( reshape( Tx , shape(T), order=ord ) )
    type is(real(wp))
     T = int( reshape( Tx , shape(T), order=ord ) )
    type is(integer)
     T =      reshape( Tx , shape(T), order=ord )
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)
  
 end subroutine read_4dim_al_i
 
!-----------------------------------------------------------------------

 subroutine read_4dim_i(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  integer, intent(out) :: T(:,:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  integer, allocatable :: Ta(:,:,:,:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'
 
   call read_4dim_al_i(Ta,var_name,fu,norewind)

   if(any(shape(T).ne.shape(Ta))) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   T = Ta

   deallocate(Ta)

 end subroutine read_4dim_i
 
!-----------------------------------------------------------------------

 subroutine write_5dim_i(T,var_name,fu)
  integer, intent(in) :: fu
  integer, intent(in) :: T(:,:,:,:,:)
  character(len=*), intent(in) :: var_name

   if(represent_integers_as_int) then
     call write_ndim_impl(reshape(T,(/size(T)/)),shape(T),var_name,fu)
   else
     call write_5dim_r(      real(T,wp)                  ,var_name,fu)
   endif

 end subroutine write_5dim_i
 
!-----------------------------------------------------------------------

 subroutine read_5dim_al_i(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  integer, allocatable, intent(out) :: T(:,:,:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer :: ord(5)
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(size(dims).gt.5) &
     call error(this_sub_name,this_mod_name,                     &
       'This variable is not a 5D array: "'//trim(var_name)//'".')

   if(size(dims).eq.0) then ! scalar
     allocate( T(1,1,1,1,1) )
   elseif(size(dims).eq.2) then ! matrix, implies dims(3:)=1
     allocate( T(dims(1),dims(2),1,1,1) )
   elseif(size(dims).eq.3) then 
     allocate( T(dims(1),dims(2),dims(3),1,1) )
   elseif(size(dims).eq.4) then 
     allocate( T(dims(1),dims(2),dims(3),dims(4),1) )
   else
     allocate( T(dims(1),dims(2),dims(3),dims(4),dims(5)) )
   endif

   if(is_transp) then
     ord = (/ 2 , 1 , 3 , 4 , 5 /)
   else
     ord = (/ 1 , 2 , 3 , 4 , 5 /)
   endif

   select type(Tx)
    type is(complex(wp))
     T = int( reshape( Tx , shape(T), order=ord ) )
    type is(real(wp))
     T = int( reshape( Tx , shape(T), order=ord ) )
    type is(integer)
     T =      reshape( Tx , shape(T), order=ord )
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)
  
 end subroutine read_5dim_al_i
 
!-----------------------------------------------------------------------

 subroutine read_5dim_i(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  integer, intent(out) :: T(:,:,:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  integer, allocatable :: Ta(:,:,:,:,:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'
 
   call read_5dim_al_i(Ta,var_name,fu,norewind)

   if(any(shape(T).ne.shape(Ta))) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   T = Ta

   deallocate(Ta)

 end subroutine read_5dim_i

!-----------------------------------------------------------------------

 subroutine write_scal_l(x,var_name,fu)
  integer, intent(in) :: fu
  logical, intent(in) :: x
  character(len=*), intent(in) :: var_name
 
   if(represent_logicals_as_bool) then
     call write_scal_impl(             x ,var_name,fu)
   else
     call write_scal_i(logical2integer(x),var_name,fu)
   endif

 end subroutine write_scal_l

!-----------------------------------------------------------------------

 subroutine read_scal_l(x,var_name,fu,norewind)
  integer, intent(in) :: fu
  logical, intent(out) :: x
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind
 
  logical :: is_transp
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_scal'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(size(Tx).ne.1) & ! this is not a scalar...
     call error(this_sub_name,this_mod_name,                   &
       'This variable is not a scalar: "'//trim(var_name)//'".')

   ! Tx has the type defined in the octave file; must be copied in x
   select type(Tx)
    type is(complex(wp))
     x = integer2logical( int( Tx(1) ) )
    type is(real(wp))
     x = integer2logical( int( Tx(1) ) )
    type is(integer)
     x = integer2logical(      Tx(1)   )
    type is(logical)
     x =                       Tx(1)
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)

 end subroutine read_scal_l

!-----------------------------------------------------------------------
 
 subroutine write_vect_l(V,rc,var_name,fu)
  integer, intent(in) :: fu
  logical, intent(in) :: V(:)
  character(len=*), intent(in) :: rc,var_name

   if(represent_logicals_as_bool) then
     call write_vect_impl(             V ,rc,var_name,fu)
   else
     call write_vect_i(logical2integer(V),rc,var_name,fu)
   endif
  
 end subroutine write_vect_l
 
!-----------------------------------------------------------------------
 
 subroutine read_vect_al_l(V,var_name,fu,norewind)
  integer, intent(in) :: fu
  logical, allocatable, intent(out) :: V(:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(count(dims.gt.1).gt.1) & ! this is not a vector...
     call error(this_sub_name,this_mod_name,                   &
       'This variable is not a vector: "'//trim(var_name)//'".')

   allocate( V(size(Tx)) )
   select type(Tx)
    type is(complex(wp))
     V = integer2logical( int( Tx ) )
    type is(real(wp))
     V = integer2logical( int( Tx ) )
    type is(integer)
     V = integer2logical(      Tx   )
    type is(logical)
     V =                       Tx
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)

 end subroutine read_vect_al_l
 
!-----------------------------------------------------------------------
 
 subroutine read_vect_l(V,var_name,fu,norewind)
  integer, intent(in) :: fu
  logical, intent(out) :: V(:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical, allocatable :: Va(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'

   ! Not very efficient, but simpler...
   call read_vect_al_l(Va,var_name,fu,norewind)

   if(size(V).ne.size(Va)) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   V = Va

   deallocate(Va)
  
 end subroutine read_vect_l
 
!-----------------------------------------------------------------------
 
 subroutine write_mat_l(M,var_name,fu)
  integer, intent(in) :: fu
  logical, intent(in) :: M(:,:)
  character(len=*), intent(in) :: var_name

   if(represent_logicals_as_bool) then
     call write_ndim_impl(reshape(M,(/size(M)/)),shape(M),var_name,fu)
   else
     call write_mat_i(logical2integer(M)                 ,var_name,fu)
   endif
  
 end subroutine write_mat_l
 
!-----------------------------------------------------------------------
 
 subroutine read_mat_al_l(M,var_name,fu,norewind)
  integer, intent(in) :: fu
  logical, allocatable, intent(out) :: M(:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer :: ord(2)
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(size(dims).gt.2) &
     call error(this_sub_name,this_mod_name,                   &
       'This variable is not a matrix: "'//trim(var_name)//'".')

   ! An octave object is either a scalar or it has at least two
   ! indexes
   if(size(dims).eq.0) then
     allocate( M(1,1) )
   else
     allocate( M(dims(1),dims(2)) )
   endif

   if(is_transp) then
     ord = (/ 2 , 1 /)
   else
     ord = (/ 1 , 2 /)
   endif

   select type(Tx)
    type is(complex(wp))
     M = integer2logical( int( reshape( Tx , shape(M), order=ord ) ) )
    type is(real(wp))
     M = integer2logical( int( reshape( Tx , shape(M), order=ord ) ) )
    type is(integer)
     M = integer2logical(      reshape( Tx , shape(M), order=ord )   )
    type is(logical)
     M =                       reshape( Tx , shape(M), order=ord )
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)

 end subroutine read_mat_al_l
 
!-----------------------------------------------------------------------
 
 subroutine read_mat_l(M,var_name,fu,norewind)
  integer, intent(in) :: fu
  logical, intent(out) :: M(:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical, allocatable :: Ma(:,:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'
 
   call read_mat_al_l(Ma,var_name,fu,norewind)

   if(any(shape(M).ne.shape(Ma))) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   M = Ma

   deallocate(Ma)

 end subroutine read_mat_l
 
!-----------------------------------------------------------------------
 
 subroutine write_3dim_l(T,var_name,fu)
  integer, intent(in) :: fu
  logical, intent(in) :: T(:,:,:)
  character(len=*), intent(in) :: var_name

   if(represent_logicals_as_bool) then
     call write_ndim_impl(reshape(T,(/size(T)/)),shape(T),var_name,fu)
   else
     call write_3dim_i(logical2integer(T)                ,var_name,fu)
   endif
  
 end subroutine write_3dim_l
 
!-----------------------------------------------------------------------
 
 subroutine read_3dim_al_l(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  logical, allocatable, intent(out) :: T(:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer :: ord(3)
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(size(dims).gt.3) &
     call error(this_sub_name,this_mod_name,                     &
       'This variable is not a 3D array: "'//trim(var_name)//'".')

   if(size(dims).eq.0) then ! scalar
     allocate( T(1,1,1) )
   elseif(size(dims).eq.2) then ! matrix, implies dims(3)=1
     allocate( T(dims(1),dims(2),1) )
   else
     allocate( T(dims(1),dims(2),dims(3)) )
   endif

   if(is_transp) then ! only if the octave object is a matrix
     ord = (/ 2 , 1 , 3 /)
   else
     ord = (/ 1 , 2 , 3 /)
   endif

   select type(Tx)
    type is(complex(wp))
     T = integer2logical( int( reshape( Tx , shape(T), order=ord ) ) )
    type is(real(wp))
     T = integer2logical( int( reshape( Tx , shape(T), order=ord ) ) )
    type is(integer)
     T = integer2logical(      reshape( Tx , shape(T), order=ord )   )
    type is(logical)
     T =                       reshape( Tx , shape(T), order=ord )
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)
  
 end subroutine read_3dim_al_l
 
!-----------------------------------------------------------------------

 subroutine read_3dim_l(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  logical, intent(out) :: T(:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical, allocatable :: Ta(:,:,:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'
 
   call read_3dim_al_l(Ta,var_name,fu,norewind)

   if(any(shape(T).ne.shape(Ta))) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   T = Ta

   deallocate(Ta)

 end subroutine read_3dim_l
 
!-----------------------------------------------------------------------
 
 subroutine write_4dim_l(T,var_name,fu)
  integer, intent(in) :: fu
  logical, intent(in) :: T(:,:,:,:)
  character(len=*), intent(in) :: var_name

   if(represent_logicals_as_bool) then
     call write_ndim_impl(reshape(T,(/size(T)/)),shape(T),var_name,fu)
   else
     call write_4dim_i(logical2integer(T)                ,var_name,fu)
   endif
  
 end subroutine write_4dim_l
 
!-----------------------------------------------------------------------

 subroutine read_4dim_al_l(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  logical, allocatable, intent(out) :: T(:,:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer :: ord(4)
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(size(dims).gt.4) &
     call error(this_sub_name,this_mod_name,                     &
       'This variable is not a 4D array: "'//trim(var_name)//'".')

   if(size(dims).eq.0) then ! scalar
     allocate( T(1,1,1,1) )
   elseif(size(dims).eq.2) then ! matrix, implies dims(3:)=1
     allocate( T(dims(1),dims(2),1,1) )
   elseif(size(dims).eq.3) then 
     allocate( T(dims(1),dims(2),dims(3),1) )
   else
     allocate( T(dims(1),dims(2),dims(3),dims(4)) )
   endif

   if(is_transp) then ! only if the octave object is a matrix
     ord = (/ 2 , 1 , 3 , 4 /)
   else
     ord = (/ 1 , 2 , 3 , 4 /)
   endif

   select type(Tx)
    type is(complex(wp))
     T = integer2logical( int( reshape( Tx , shape(T), order=ord ) ) )
    type is(real(wp))
     T = integer2logical( int( reshape( Tx , shape(T), order=ord ) ) )
    type is(integer)
     T = integer2logical(      reshape( Tx , shape(T), order=ord )   )
    type is(logical)
     T =                       reshape( Tx , shape(T), order=ord )
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)
  
 end subroutine read_4dim_al_l
 
!-----------------------------------------------------------------------
 
 subroutine read_4dim_l(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  logical, intent(out) :: T(:,:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical, allocatable :: Ta(:,:,:,:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'
 
   call read_4dim_al_l(Ta,var_name,fu,norewind)

   if(any(shape(T).ne.shape(Ta))) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   T = Ta

   deallocate(Ta)

 end subroutine read_4dim_l
 
!-----------------------------------------------------------------------
 
 subroutine write_5dim_l(T,var_name,fu)
  integer, intent(in) :: fu
  logical, intent(in) :: T(:,:,:,:,:)
  character(len=*), intent(in) :: var_name

   if(represent_logicals_as_bool) then
     call write_ndim_impl(reshape(T,(/size(T)/)),shape(T),var_name,fu)
   else
     call write_5dim_i(logical2integer(T)                ,var_name,fu)
   endif
  
 end subroutine write_5dim_l
 
!-----------------------------------------------------------------------
 
 subroutine read_5dim_al_l(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  logical, allocatable, intent(out) :: T(:,:,:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer :: ord(5)
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(size(dims).gt.5) &
     call error(this_sub_name,this_mod_name,                     &
       'This variable is not a 5D array: "'//trim(var_name)//'".')

   if(size(dims).eq.0) then ! scalar
     allocate( T(1,1,1,1,1) )
   elseif(size(dims).eq.2) then ! matrix, implies dims(3:)=1
     allocate( T(dims(1),dims(2),1,1,1) )
   elseif(size(dims).eq.3) then 
     allocate( T(dims(1),dims(2),dims(3),1,1) )
   elseif(size(dims).eq.4) then 
     allocate( T(dims(1),dims(2),dims(3),dims(4),1) )
   else
     allocate( T(dims(1),dims(2),dims(3),dims(4),dims(5)) )
   endif

   if(is_transp) then
     ord = (/ 2 , 1 , 3 , 4 , 5 /)
   else
     ord = (/ 1 , 2 , 3 , 4 , 5 /)
   endif

   select type(Tx)
    type is(complex(wp))
     T = integer2logical( int( reshape( Tx , shape(T), order=ord ) ) )
    type is(real(wp))
     T = integer2logical( int( reshape( Tx , shape(T), order=ord ) ) )
    type is(integer)
     T = integer2logical(      reshape( Tx , shape(T), order=ord )   )
    type is(logical)
     T =                       reshape( Tx , shape(T), order=ord )
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)
  
 end subroutine read_5dim_al_l
 
!-----------------------------------------------------------------------

 subroutine read_5dim_l(T,var_name,fu,norewind)
  integer, intent(in) :: fu
  logical, intent(out) :: T(:,:,:,:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical, allocatable :: Ta(:,:,:,:,:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'
 
   call read_5dim_al_l(Ta,var_name,fu,norewind)

   if(any(shape(T).ne.shape(Ta))) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   T = Ta

   deallocate(Ta)
  
 end subroutine read_5dim_l
 
!-----------------------------------------------------------------------

 subroutine write_scal_z(x,var_name,fu)
  integer, intent(in) :: fu
  complex(wp), intent(in) :: x
  character(len=*), intent(in) :: var_name
 
   call write_scal_impl(x,var_name,fu)
  
 end subroutine write_scal_z

!-----------------------------------------------------------------------

 subroutine read_scal_z(x,var_name,fu,norewind)
  integer, intent(in) :: fu
  character(len=*), intent(in) :: var_name
  complex(wp), intent(out) :: x
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_scal'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(size(Tx).ne.1) & ! this is not a scalar...
     call error(this_sub_name,this_mod_name,                   &
       'This variable is not a scalar: "'//trim(var_name)//'".')

   ! Tx has the type defined in the octave file; must be copied in x
   select type(Tx)
    type is(complex(wp))
     x =        Tx(1)
    type is(real(wp))
     x = cmplx( Tx(1) , kind=wp )
    type is(integer)
     x = cmplx( Tx(1) , kind=wp )
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)

 end subroutine read_scal_z
 
!-----------------------------------------------------------------------
 
 subroutine write_vect_z(V,rc,var_name,fu)
  integer, intent(in) :: fu
  complex(wp), intent(in) :: V(:)
  character(len=*), intent(in) :: rc, var_name

   call write_vect_impl(V,rc,var_name,fu)

 end subroutine write_vect_z
 
!-----------------------------------------------------------------------
 
 subroutine read_vect_al_z(V,var_name,fu,norewind)
  integer, intent(in) :: fu
  complex(wp), allocatable, intent(out) :: V(:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind)

   if(count(dims.gt.1).gt.1) & ! this is not a vector...
     call error(this_sub_name,this_mod_name,                   &
       'This variable is not a vector: "'//trim(var_name)//'".')

   allocate( V(size(Tx)) )
   select type(Tx)
    type is(complex(wp))
     V =        Tx
    type is(real(wp))
     V = cmplx( Tx , kind=wp )
    type is(integer)
     V = cmplx( Tx , kind=wp )
    class default
     call error(this_sub_name,this_mod_name,                 &
       'Unknown type conversion for: "'//trim(var_name)//'".')
   end select
   deallocate(Tx,dims)

 end subroutine read_vect_al_z
 
!-----------------------------------------------------------------------

 subroutine read_vect_z(V,var_name,fu,norewind)
  integer, intent(in) :: fu
  complex(wp), intent(out) :: V(:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  complex(wp), allocatable :: Va(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'

   ! Not very efficient, but simpler...
   call read_vect_al_z(Va,var_name,fu,norewind)

   if(size(V).ne.size(Va)) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   V = Va

   deallocate(Va)

 end subroutine read_vect_z
 
!-----------------------------------------------------------------------

 subroutine write_mat_z(M,var_name,fu)
  integer, intent(in) :: fu
  complex(wp), intent(in) :: M(:,:)
  character(len=*), intent(in) :: var_name

   call write_ndim_impl(reshape(M,(/size(M)/)),shape(M),var_name,fu)

 end subroutine write_mat_z
 
!-----------------------------------------------------------------------

 subroutine write_3dim_z(T,var_name,fu)
  integer, intent(in) :: fu
  complex(wp), intent(in) :: T(:,:,:)
  character(len=*), intent(in) :: var_name

   call write_ndim_impl(reshape(T,(/size(T)/)),shape(T),var_name,fu)

 end subroutine write_3dim_z
 
!-----------------------------------------------------------------------

 subroutine write_4dim_z(T,var_name,fu)
  integer, intent(in) :: fu
  complex(wp), intent(in) :: T(:,:,:,:)
  character(len=*), intent(in) :: var_name

   call write_ndim_impl(reshape(T,(/size(T)/)),shape(T),var_name,fu)
 
 end subroutine write_4dim_z
 
!-----------------------------------------------------------------------

 subroutine write_5dim_z(T,var_name,fu)
  integer, intent(in) :: fu
  complex(wp), intent(in) :: T(:,:,:,:,:)
  character(len=*), intent(in) :: var_name

   call write_ndim_impl(reshape(T,(/size(T)/)),shape(T),var_name,fu)
 
 end subroutine write_5dim_z
 
!-----------------------------------------------------------------------

 subroutine write_scal_c(s,var_name,fu)
  integer, intent(in) :: fu
  character(len=*), intent(in) :: s
  character(len=*), intent(in) :: var_name
 
   call write_scal_impl(s,var_name,fu)

 end subroutine write_scal_c

!-----------------------------------------------------------------------

 subroutine read_scal_c(s,var_name,fu,norewind)
  integer, intent(in) :: fu
  character(len=*), intent(out) :: s
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:) ! should be used instead of Tc
  character(len=len(s)), allocatable :: Tc(:) ! should be eliminated
  character(len=*), parameter :: &
    this_sub_name = 'read_scal'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind,Tc)

   if(size(Tc).gt.1) & ! this is not a scalar...
     call error(this_sub_name,this_mod_name,                   &
       'This variable is not a scalar: "'//trim(var_name)//'".')

   !select type(Tx)
   ! type is(character(len=*))
     if(size(Tc).eq.0) then ! this is an empty string
       s = ''
     else
       s = Tc(1)
     endif
   ! class default
   !  call error(this_sub_name,this_mod_name,                 &
   !    'Unknown type conversion for: "'//trim(var_name)//'".')
   !end select
   deallocate(Tc,dims)

 end subroutine read_scal_c
 
!-----------------------------------------------------------------------

 subroutine write_vect_c(s,var_name,fu)
  integer, intent(in) :: fu
  character(len=*), intent(in) :: s(:)
  character(len=*), intent(in) :: var_name
 
   call write_ndim_impl(s,shape(s),var_name,fu)

 end subroutine write_vect_c

!-----------------------------------------------------------------------

 subroutine read_vect_al_c(s,var_name,fu,norewind)
  integer, intent(in) :: fu
  character(len=*), allocatable, intent(out) :: s(:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=len(s)), allocatable :: Tc(:) ! should be eliminated
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind,Tc)

   if(count( (/ dims(1) , dims(3:) /).gt.1).gt.1) &
     call error(this_sub_name,this_mod_name,                   &
       'This variable is not a vector: "'//trim(var_name)//'".')
    
   ! workaround for this gfortran bug (dims is not used anyway)
   ! https://gcc.gnu.org/bugzilla/show_bug.cgi?id=60560
   !allocate( s(size(Tc)) )
   dims(1) = size(Tc); allocate( s(dims(1)) )
   !select type(Tx)
   ! type is(character(len=*))
     s = Tc
   ! class default
   !  call error(this_sub_name,this_mod_name,                 &
   !    'Unknown type conversion for: "'//trim(var_name)//'".')
   !end select
   deallocate(Tc,dims)

 end subroutine read_vect_al_c
 
!-----------------------------------------------------------------------

 subroutine read_vect_c(s,var_name,fu,norewind)
  integer, intent(in) :: fu
  character(len=*), intent(out) :: s(:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  character(len=len(s)), allocatable :: sa(:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'

   call read_vect_al_c(sa,var_name,fu,norewind)

   if(size(s).ne.size(sa)) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   s = sa

   deallocate(sa)

 end subroutine read_vect_c
 
!-----------------------------------------------------------------------

 subroutine write_mat_c(s,var_name,fu)
  integer, intent(in) :: fu
  character(len=*), intent(in) :: s(:,:)
  character(len=*), intent(in) :: var_name
 
   call write_ndim_impl(reshape(s,(/size(s)/)),shape(s),var_name,fu)

 end subroutine write_mat_c

!-----------------------------------------------------------------------

 subroutine read_mat_al_c(s,var_name,fu,norewind)
  integer, intent(in) :: fu
  character(len=*), allocatable, intent(out) :: s(:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  logical :: is_transp
  integer, allocatable :: dims(:)
  class(*), allocatable :: Tx(:)
  character(len=len(s)), allocatable :: Tc(:) ! should be eliminated
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   call read_al_impl(Tx,dims,is_transp,var_name,fu,norewind,Tc)

   if(size(dims).gt.3) &
     call error(this_sub_name,this_mod_name,                   &
       'This variable is not a matrix: "'//trim(var_name)//'".')
    
   ! For strings, dims has always at least two elements
   if(size(dims).eq.2) then
     allocate( s(dims(1),1) )
   else
     allocate( s(dims(1),dims(3)) )
   endif

   !select type(Tx)
   ! type is(character(len=*))
     s = reshape( Tc , shape(s) )
   ! class default
   !  call error(this_sub_name,this_mod_name,                 &
   !    'Unknown type conversion for: "'//trim(var_name)//'".')
   !end select
   deallocate(Tc,dims)

 end subroutine read_mat_al_c
 
!-----------------------------------------------------------------------

 subroutine read_mat_c(s,var_name,fu,norewind)
  integer, intent(in) :: fu
  character(len=*), intent(out) :: s(:,:)
  character(len=*), intent(in) :: var_name
  logical, intent(in), optional :: norewind

  character(len=len(s)), allocatable :: sa(:,:)
  character(len=*), parameter :: &
    this_sub_name = 'read_octave'

   call read_mat_al_c(sa,var_name,fu,norewind)

   if(any(shape(s).ne.shape(sa))) &
     call error(this_sub_name,this_mod_name,            &
       'Shape mismatch reading "'//trim(var_name)//'".' )

   s = sa

   deallocate(sa)

 end subroutine read_mat_c
 
!-----------------------------------------------------------------------

 subroutine write_3dim_c(s,var_name,fu)
  integer, intent(in) :: fu
  character(len=*), intent(in) :: s(:,:,:)
  character(len=*), intent(in) :: var_name
 
   call write_ndim_impl(reshape(s,(/size(s)/)),shape(s),var_name,fu)

 end subroutine write_3dim_c

!-----------------------------------------------------------------------

 subroutine write_4dim_c(s,var_name,fu)
  integer, intent(in) :: fu
  character(len=*), intent(in) :: s(:,:,:,:)
  character(len=*), intent(in) :: var_name
 
   call write_ndim_impl(reshape(s,(/size(s)/)),shape(s),var_name,fu)

 end subroutine write_4dim_c

!-----------------------------------------------------------------------

 subroutine write_5dim_c(s,var_name,fu)
  integer, intent(in) :: fu
  character(len=*), intent(in) :: s(:,:,:,:,:)
  character(len=*), intent(in) :: var_name
 
   call write_ndim_impl(reshape(s,(/size(s)/)),shape(s),var_name,fu)

 end subroutine write_5dim_c

!-----------------------------------------------------------------------

 !> Write a function handle
 !!
 !! A function handle does not have a well-defined fortran
 !! counterpart; in fact we assume here that it is specified as a
 !! string. However, to clarify that we are writing a function handle,
 !! we introduce a dummy argument \c fh which <em>must</em> be set to
 !! <tt>function_handle</tt>.
 !!
 !! <ul>
 !!  <li> To write a function handle, simply use \c function to pass
 !!  the name of the function; \c function_expr shall not be present.
 !!  <li> To write an anonymous function, set \c function to
 !!  <tt>\<anonymous\></tt> and use \c function_expr to pass the
 !!  definition of the anonymous function, omitting the leading
 !!  <tt>\@</tt>.
 !! </ul>
 !!
 !! \note No path information is included for a function handle: this
 !! means that the function must be visible from the octave path when
 !! the data file is loaded.
 subroutine write_function_handle(fh,fun_name,function,fu,function_expr)
  integer, intent(in) :: fu
  character(len=*), intent(in) :: fh
  character(len=*), intent(in) :: fun_name
  character(len=*), intent(in) :: function
  character(len=*), intent(in), optional :: function_expr

  character(len=*), parameter :: &
    this_sub_name = 'write_function_handle'

   if(trim(fh).ne."function_handle") &
    call error(this_sub_name,this_mod_name,                            &
                 'Please, set the dummy argument "fh=function_handle".')
  
   ! There are two cases: function handles and anonymous functions

   write(fu,'(a,a)') '# name: ',fun_name
   write(fu,'(a)')   '# type: function handle'

   if(trim(function) .eq. "<anonymous>") then
     if(.not.present(function_expr)) &
      call error(this_sub_name,this_mod_name,                          &
      'To write an anonymous function the expression must be provided.')

     write(fu,'(a)') '@<anonymous>'
     write(fu,'(a,a)') '@',trim(adjustl(function_expr))
   else
     if(present(function_expr)) &
      call error(this_sub_name,this_mod_name,                          &
    'When writing a function handle no expression should be specified.')

     write(fu,'(a)') function
   endif

 end subroutine write_function_handle

!-----------------------------------------------------------------------
 
 subroutine locate_var(fu,var_name,ierr,norewind)
 ! Position the file at the first occurrence of string
 ! Use  norewind.eqv..true.  to avoid rewind
  integer, intent(in) :: fu
  character(len=*), intent(in) :: var_name
  integer, intent(out) :: ierr
  logical, intent(in), optional :: norewind

  logical :: found, rew
  character(len=1000) :: record
  character(len=len(var_name)+8) :: record_var_name
  character(len=*), parameter :: &
    this_sub_name = 'locate_var'

   found = .false.
   record_var_name = '# name: ' // var_name! // '\n'

   ! Check whether rewind is required
   rew = .true. ! default is to rewind the file
   if(present(norewind)) rew = .not.norewind
   if(rew) rewind(fu)

   do
    if(found) exit
     read(fu,'(a)',iostat=ierr) record
     if(ierr.ne.0) then ! either error or end of file
       found = .true.
     elseif(record.eq.record_var_name) then
       found = .true.
       ierr = 0
     endif
   enddo

 end subroutine locate_var

!-----------------------------------------------------------------------

 elemental function logical2integer(l) result(i)
  logical, intent(in) :: l
  integer :: i

   if(l.eqv..true.) then
     i = 1
   else
     i = 0
   endif
 end function logical2integer

 elemental function integer2logical(i) result(l)
  integer, intent(in) :: i
  logical :: l

   l = i.eq.1
 end function integer2logical

!-----------------------------------------------------------------------

 subroutine write_scal_impl(x,var_name,fu)
  integer, intent(in) :: fu
  class(*), intent(in) :: x
  character(len=*), intent(in) :: var_name

  character(len=*), parameter :: &
    this_sub_name = 'write_octave'

   if(var_name.ne.'') then
     write(fu,'(a,a)') '# name: ',var_name
   endif

   select type(x)

    type is(logical)
     write(fu,'(a)')   '# type: bool' ! no "scalar" for the bool type
     write(fu,'(i0)') logical2integer(x)
     
    type is(integer)
     write(fu,'(a)')   '# type: '//oct_int_type//' scalar'
     write(fu,'(i0)') x
     
    type is(real(wp))
     write(fu,'(a)')   '# type: scalar'
     write(fu,'('//real_format//')') x
    
    type is(complex(wp))
     write(fu,'(a)')   '# type: complex scalar'
     write(fu,'('//complex_format//')') x

    type is(character(len=*))
     write(fu,'(a)')   '# type: string'
     write(fu,'(a)')   '# elements: 1'
     write(fu,'(a,i0)')'# length: ',len_trim(x)
     write(fu,'(a)')   trim(x)

    class default
     call error(this_sub_name,this_mod_name,            &
       'Unknown type for variable "'//trim(var_name)//'".')

   end select

 end subroutine write_scal_impl

!-----------------------------------------------------------------------
 
 !> Reshape the vector as a 2D array and call \c write_ndim_impl
 subroutine write_vect_impl(V,rc,var_name,fu)
  integer, intent(in) :: fu
  class(*), intent(in) :: V(:)
  character(len=*), intent(in) :: rc, var_name

  integer :: nrow, ncol
  character(len=100) :: message(2)
  character(len=*), parameter :: &
    this_sub_name = 'write_octave'

   if(rc.eq.'c') then
     nrow = product(shape(V))
     ncol = 1
   elseif(rc.eq.'r') then
     nrow = 1
     ncol = product(shape(V))
   else
     write(message(1),'(A)') &
       'Variable rc must be either "r" or "c".'
     write(message(2),'(A,A,A)') &
       'Present value rc="',rc,'".'
     call warning(this_sub_name,this_mod_name,message)
     return
   endif
     
   call write_ndim_impl(V,(/nrow,ncol/),var_name,fu)

 end subroutine write_vect_impl

!-----------------------------------------------------------------------

 !> Write a multidimensional array
 !!
 !! The array is reshaped as a 1D object, since the octave format is
 !! the always the same. The shape of the array then must be passed
 !! separately.
 subroutine write_ndim_impl(T,dims,var_name,fu)
  integer, intent(in) :: fu
  class(*), intent(in) :: T(:)
  integer, intent(in) :: dims(:)
  character(len=*), intent(in) :: var_name

  integer :: i, j, k
  character(len=*), parameter :: &
    this_sub_name = 'write_octave'

   if(var_name.ne.'') then
     write(fu,'(a,a)') '# name: ',var_name
   endif

   select type(T)

    type is(logical)
     write(fu,'(a)')   '# type: bool matrix'
     write(fu,'(a,i0)')'# ndims: ',size(dims)
     write(fu,ia_fmt)  dims
     write(fu,'(i0)') logical2integer(T)
     
    type is(integer)
     write(fu,'(a)')   '# type: '//oct_int_type//' matrix'
     write(fu,'(a,i0)')'# ndims: ',size(dims)
     write(fu,ia_fmt)  dims
     write(fu,'(i0)') T
     
    type is(real(wp))
     write(fu,'(a)')   '# type: matrix'
     write(fu,'(a,i0)')'# ndims: ',size(dims)
     write(fu,ia_fmt)  dims
     write(fu,'('//real_format//')') T
    
    type is(complex(wp))
     write(fu,'(a)')   '# type: complex matrix'
     write(fu,'(a,i0)')'# ndims: ',size(dims)
     write(fu,ia_fmt)  dims
     write(fu,'('//complex_format//')') T

    type is(character(len=*))
     ! The string length becomes in octave the second dimension
     write(fu,'(a)')   '# type: string'
     write(fu,'(a,i0)')'# ndims: ',1+size(dims)
     write(fu,ia_fmt)  (/ dims(1) , len(T) , dims(2:) /)
     ! we need to transpose the first array dimension and the length
     ! Compiler bug: using associate is easier, but ifort has a
     ! problem: see https://software.intel.com/en-us/forums/topic/535268
     !associate( Tr => reshape( T , (/ dims(1) , product(dims(2:)) /) ) )
     !write(fu,'(*(a))') &
     !  (((Tr(i,k)(j:j) , i=1,size(Tr,1)), j=1,len(Tr)), k=1,size(Tr,2))
     !end associate
     call ifort_bug_workaround(T)
      
    class default
     call error(this_sub_name,this_mod_name,            &
       'Unknown type for variable "'//trim(var_name)//'".')

   end select

 contains

  subroutine ifort_bug_workaround(T)
   character(len=*), intent(in) :: T(:)
   character(len=len(T)), allocatable :: Tr(:,:)

     allocate( Tr(dims(1) , product(dims(2:))) )
     Tr = reshape( T , shape(Tr) )
     write(fu,'(*(a))') &
       (((Tr(i,k)(j:j) , i=1,size(Tr,1)), j=1,len(Tr)), k=1,size(Tr,2))
     deallocate( Tr )
  end subroutine ifort_bug_workaround

 end subroutine write_ndim_impl

!-----------------------------------------------------------------------

 !> Read an octave object and return it as a 1D array
 !!
 !! This subroutine reads one of the following octave objects:
 !! <ul>
 !!  <li> scalar
 !!  <li> matrix; ndims
 !!  <li> matrix; rows,column
 !!  <li> string; ndims
 !!  <li> string; elements,length
 !! </ul>
 !! Notice that when using the "rows,column" format the matrix is
 !! transposed while reading it; it is the caller's responsibility to
 !! handle this checking the \c is_transp argument.
 !!
 !! \note For strings, \c is_transp is always <tt>.false.</tt>, since
 !! the two string formats are dealt with completely inside this
 !! subroutine.
 subroutine read_al_impl(T,dims,is_transp,var_name,fu,norewind,Tc)
  integer,               intent(in) :: fu
  character(len=*),      intent(in) :: var_name
  logical,               intent(in), optional :: norewind
  class(*), allocatable, intent(out) :: T(:)
  integer,  allocatable, intent(out) :: dims(:)
  logical,               intent(out) :: is_transp
  !> Read an octave string
  !!
  !! In principle, there is no need for this additional argument,
  !! since \c T could also return a string. However, very few
  !! compilers support allocating a <tt>class(*)</tt> object to a \c
  !! character, because thins implies dynamic length. Hence, the
  !! character case is handled separately.
  character(len=*), allocatable, intent(out), optional :: Tc(:)

  logical :: elem_format
  integer :: ft, os, nel, ierr
  integer, allocatable :: Tbool(:)
  character(len=1000) :: oc_type
  character(len=*), parameter :: &
    this_sub_name = 'read_octave_al'

   ! The following subroutine positions the file at the '# name' line
   call locate_var(fu,var_name,ierr,norewind)
   if(ierr.ne.0) &
     call error(this_sub_name,this_mod_name,              &
       'Problem reading variable "'//trim(var_name)//'".' )

   ! Check the data type
   read(fu,'(a)') oc_type !  # type: XXX
   call parse_octave_type(oc_type,ft,os)

   if( (ft.eq.ft_x).or.(os.eq.os_x) ) &
     call error(this_sub_name,this_mod_name,           &
       'Problem parsing the type of "'//trim(var_name) &
       //'", type is "'//trim(oc_type)//'".' )

   ! This subroutine requires an octave matrix or a scalar
   os_case: select case(os)

    case(os_s)
     allocate( dims(0) )
     nel = 1
     is_transp = .false.

    case(os_m)
     call read_oc_dims(dims,is_transp)
     nel = product(dims)

    case(os_u) ! is this a string?
     if(ft.eq.ft_c) then
       call read_oc_string_dims(dims,elem_format)
       nel = dims(1)*product(dims(3:)) ! dims(2) is the length
     else
       call error(this_sub_name,this_mod_name,             &
         'Unable to process the type of "'//trim(var_name) &
         //'", type is "'//trim(oc_type)//'".' )
     endif
     
    case default
     call error(this_sub_name,this_mod_name,             &
       'Unable to process the type of "'//trim(var_name) &
       //'", type is "'//trim(oc_type)//'".' )

   end select os_case

   ! Now we can read the array
   ft_case: select case(ft)

    case(ft_l)
     allocate(logical::T(nel))
     select type(T); type is(logical)
     ! bool are represented as 0/1
     allocate(Tbool(nel))
     read(fu,*) Tbool
     T = integer2logical(Tbool)
     deallocate(Tbool)
     end select

    case(ft_i)
     allocate(integer::T(nel))
     select type(T); type is(integer)
     read(fu,*) T
     end select

    case(ft_r)
     allocate(real(wp)::T(nel))
     select type(T); type is(real(wp))
     read(fu,*) T
     end select

    case(ft_z)
     allocate(complex(wp)::T(nel))
     select type(T); type is(complex(wp))
     read(fu,*) T
     end select

    case(ft_c)
     ! Notice: one should be able to use T instead of Tc and avoid the
     ! additional argument Tc at all. See also
     ! https://software.intel.com/en-us/comment/1804813
     !allocate(character(len=dims(2))::T(nel))
     !select type(T); type is(character(len=*))
     allocate(Tc(nel))
     call read_oc_string_data(Tc,dims,elem_format)
     !end select

    case default
     ! this should never happen...
     call error(this_sub_name,this_mod_name,                     &
       'Error in "os_s_ft_case" reading "'//trim(var_name)//'".' )

   end select ft_case

 contains
  
  subroutine read_oc_dims(dims,is_transp)
   integer, allocatable, intent(out) :: dims(:)
   logical,              intent(out) :: is_transp

   integer :: i, j, ndims
   character(len=1000) :: oc_ndims

    ! get the dimensions
    read(fu,'(a)') oc_ndims

    ! make sure that the array uses the right format
    i = index(oc_ndims,"ndims")
    j = index(oc_ndims,"rows")
    if( (i.eq.0).and.(j.eq.0) ) &
      call error(this_sub_name,this_mod_name,                &
        'Problem determining the shape of "'//trim(var_name) &
        //'", ndims is "'//trim(oc_ndims)//'".' )

    if(i.gt.0) then ! using the "ndims" format

      i = index(oc_ndims,":")
      if(i.eq.0) &
        call error(this_sub_name,this_mod_name,                &
          'Problem determining the shape of "'//trim(var_name) &
          //'", ndims is "'//trim(oc_ndims)//'".' )

      read(oc_ndims(i+1:),*) ndims
      allocate( dims(ndims) )
      read(fu,*) dims
      is_transp = .false.

    else ! using the "rows" format

      i = index(oc_ndims,":")
      if(i.eq.0) &
        call error(this_sub_name,this_mod_name,                &
          'Problem determining the shape of "'//trim(var_name) &
          //'", ndims is "'//trim(oc_ndims)//'".' )

      allocate( dims(2) ) ! these are always 2D matrices
      read(oc_ndims(i+1:),*) dims(1)

      read(fu,'(a)') oc_ndims
      i = index(oc_ndims,"columns")
      if(i.eq.0) &
        call error(this_sub_name,this_mod_name,                &
          'Problem determining the shape of "'//trim(var_name) &
          //'", ndims is "'//trim(oc_ndims)//'".' )

      i = index(oc_ndims,":")
      if(i.eq.0) &
        call error(this_sub_name,this_mod_name,                &
          'Problem determining the shape of "'//trim(var_name) &
          //'", ndims is "'//trim(oc_ndims)//'".' )

      read(oc_ndims(i+1:),*) dims(2)
      is_transp = .true.

    endif

  end subroutine read_oc_dims

  subroutine read_oc_string_dims(dims,elem_format)
   integer, allocatable, intent(out) :: dims(:)
   logical,              intent(out) :: elem_format
    
   integer :: i, j, ndims
   character(len=1000) :: oc_ndims

    ! get the dimensions
    read(fu,'(a)') oc_ndims

    ! make sure that the string uses the right format
    i = index(oc_ndims,"ndims")
    j = index(oc_ndims,"elements")
    if( (i.eq.0).and.(j.eq.0) ) &
      call error(this_sub_name,this_mod_name,                &
        'Problem determining the shape of "'//trim(var_name) &
        //'", ndims is "'//trim(oc_ndims)//'".' )

    if(i.gt.0) then ! using the "ndims" format (same as for matrices)

      i = index(oc_ndims,":")
      if(i.eq.0) &
        call error(this_sub_name,this_mod_name,                &
          'Problem determining the shape of "'//trim(var_name) &
          //'", ndims is "'//trim(oc_ndims)//'".' )

      read(oc_ndims(i+1:),*) ndims
      allocate( dims(ndims) )
      ! notce: the second octave dimension is the fortran length.
      read(fu,*) dims
      elem_format = .false.

    else ! using the "elements" format

      i = index(oc_ndims,":")
      if(i.eq.0) &
        call error(this_sub_name,this_mod_name,                &
          'Problem determining the shape of "'//trim(var_name) &
          //'", elements is "'//trim(oc_ndims)//'".' )

      allocate( dims(2) ) ! these are always 2D string matrices
      read(oc_ndims(i+1:),*) dims(1)

      if(dims(1).gt.0) then
        read(fu,'(a)') oc_ndims ! length taken from the first element
        i = index(oc_ndims,"length")
        if(i.eq.0) &
          call error(this_sub_name,this_mod_name,                 &
            'Problem determining the length of "'//trim(var_name) &
            //'", length is "'//trim(oc_ndims)//'".' )

        i = index(oc_ndims,":")
        if(i.eq.0) &
          call error(this_sub_name,this_mod_name,                 &
            'Problem determining the length of "'//trim(var_name) &
            //'", length is "'//trim(oc_ndims)//'".' )

        read(oc_ndims(i+1:),*) dims(2)
      else
        dims(2) = 0
      endif

      elem_format = .true.

    endif

  end subroutine read_oc_string_dims

  subroutine read_oc_string_data(s,dims,elem_format)
   integer, intent(in) :: dims(:)
   logical, intent(in) :: elem_format
   character(len=*), intent(out) :: s(:)

   integer :: i, j, k, ii
   ! A string used to discard some lines, must be long enough that we
   ! can test whether we are dropping the right lines...
   character(len=100) :: discard

   ! Using the nonadvancing IO (see later) it is important to read the
   ! correct number of characters, even if they have to be truncated
   ! during the assignment to the output variable, otherwise the
   ! following reads are not reading the right records. This is
   ! especially difficult here because the octave strings can include
   ! newlines, which cause an end of record in fortran.
   !
   ! That said, one allocating the dummy argument to the desired
   ! length is supported, there is no need for an additional variable,
   ! but since for the time being we are forced to use the length of
   ! the string passed by the user, and this length can be smaller
   ! than the one of the octave string, we need here a temporary
   ! variable which is guaranteed to contain the octave string.
   character(len=dims(2)) :: s_tmp(size(s))

    if(elem_format) then

      do i=1,size(s)
        if(i.ne.1) then ! discard  # length: X
          read(fu,'(a)') discard
          ! there could be a leftover from the previous record...
          if(index(discard,"length").eq.0) read(fu,'(a)') discard
        endif
        ! We have to read a string, which in principle is easy. The
        ! problem is that an octave string can include newlines, so
        ! we have to use advance='no' and read the characters one by
        ! one. Notice that we know that we have to read dims(2)
        ! characters.
        do j=1,dims(2)
          read(fu,'(a)',advance='no',eor=10) s_tmp(i)(j:j) ! one char
          cycle
          ! if we reach an end of record it means that the octave
          ! string includes a newline
10        s_tmp(i)(j:j) = new_line(s_tmp)
        enddo
      enddo

    else

      ! This is simpler: we read all the characters one by one; see
      ! also write_ndim_impl to see how the order of the characters
      ! must be followed
      do k=1,product(dims(3:))
        do j=1,dims(2)
          do i=1,dims(1)
            ii = dims(1)*(k-1)+i
            read(fu,'(a)',advance='no',eor=20) s_tmp(ii)(j:j)
            cycle
20          s_tmp(ii)(j:j) = new_line(s_tmp)
          enddo
        enddo
      enddo

    endif

    ! Here the truncation/padding of s takes place
    s = s_tmp

  end subroutine read_oc_string_data

 end subroutine read_al_impl

!-----------------------------------------------------------------------

 !> Parse a <tt>\# type: XXX</tt> octave type declaration
 !!
 !! One should always parse octave types with this function, because
 !! this is not always an obvious operations (consider the cases \c
 !! matrix vs. \c null_matrix or \c string vs. \c sq_string)
 pure subroutine parse_octave_type(oc_type,ft,os)
  character(len=*), intent(in) :: oc_type
  integer, intent(out) :: ft, os

  integer :: i
  character(len=len(oc_type)) :: ot

   ! Preliminary check
   i = index(oc_type,"type")
   if(i.eq.0) then
     ft = ft_x
     return
   endif

   ! Locate ":"
   i = index(oc_type,":")
   if(i.eq.0) then
     ft = ft_x
     return
   endif

   ot = adjustl( oc_type(i+1:) ) ! skip "# type:"

   ! check the type
   if(      index(ot,"bool"   ).gt.0 ) then
     ft = ft_l
   elseif(  index(ot,"int"    ).gt.0 ) then
     ft = ft_i
   elseif(  index(ot,"complex").gt.0 ) then
     ft = ft_z
   elseif(  index(ot,"string" ).gt.0 ) then
     ft = ft_c
     ! strings have no additional specification
     os = os_u
     return
   else ! if nothing of the above is specified, real is understood
     ft = ft_r
   endif

   ! check the structure
   if(      index(ot,"scalar").gt.0 ) then
     os = os_s
   elseif(  index(ot,"matrix").gt.0 ) then
     os = os_m
   elseif(  index(ot,"struct").gt.0 ) then
     os = os_t
   elseif(  index(ot,"cell"  ).gt.0 ) then
     os = os_c
   else
     ! bool objects do not include "scalar" when they are scalar...
     if(ft.eq.ft_l) then
       os = os_s
     else
       os = os_x
     endif
   endif
     
 end subroutine parse_octave_type

!-----------------------------------------------------------------------

end module mod_octave_io

