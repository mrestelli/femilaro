!! Copyright (C) 2012, 2013  Carlo de Falco
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Carlo de Falco

!> To compile use:
!!
!!    mkoctfile -c mod_utils.f90 mod_messages.f90 mod_kinds.f90 \
!!                 mod_octave_io_binary.f90 test_octave_io_binary.f90 \
!!                 octave_file_io.cc -I.
!!
!! then link with:
!!
!!     mpif90 octave_file_io.o test_octave_io_binary.o mod_messages.o \
!!            mod_utils.o mod_kinds.o mod_octave_io_binary.o \
!!            $(mkoctfile -p LFLAGS) $(mkoctfile -p OCTAVE_LIBS)
!!
!! create a binary (gzip compressed or not) file named "pippo.octbin"
!! containing a NDArray<double> variable named "pippo" to test.
!! 
!! Additionally, notice that compiler options can be passed to
!! mkoctfile with
!!
!! export FFLAGS="-g -O0 -pedantic -Wall -Wconversion \
!!     -Wconversion-extra -fcheck=all -finit-real=snan  \
!!     -ffpe-trap=invalid -finit-integer=-9999 -Wintrinsics-std"
!<

program test_octave_io_binary

  use iso_c_binding
  use mod_octave_io_binary
  use mod_kinds

  implicit none

  real(wp), allocatable :: vec(:), mat(:,:), &
       vecout(:), matout(:, :)
  real(wp) :: pluto = 3.14
  integer(c_int) :: mode_in, mode_out, i, j


  allocate (vecout(7))
  do i = 1, 7
     vecout(i) = real (i, wp)
  end do

  allocate (matout(4,7))
  do i = 1, 4
     do j = 1, 7
        matout(i, j) = real (i+j/2, wp)
     end do
  end do

  !!  write(*,*) vecout

  mode_in = 3
  call octave_binary_open ("minnie.octbin", mode_in, mode_out)
  call write_octave_binary ("clarabella", vecout)
  call write_octave_binary ("orazio", matout)
  call write_octave_binary ("pluto", pluto)
  call octave_binary_close ()

  mode_in = 1
  call octave_binary_open ("pippo.octbin", mode_in, mode_out)
  call read_octave_binary_al ("topolino", mat)
  call read_octave_binary_al ("pippo", vec)
  call octave_binary_close ()

  write(*,*) 'vec'
  write(*,*) vec

  write(*,*) 'mat'
  write(*,*) mat
 
end program test_octave_io_binary
