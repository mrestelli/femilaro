!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

module mod_octave_io_sparse
!General comments: octave io for sparse matrices. Overloads the
!mod_octave_io subroutines for the types of mod_sparse.
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_octave_io, only: &
   mod_octave_io_initialized, &
   write_octave, &
   read_octave,  &
   real_format,  &
   locate_var

 use mod_sparse, only: &
   mod_sparse_initialized, &
   t_col, t_tri, t_pm_sk, &
   tri2col, col2tri, col_aij, new_tri, clear

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_octave_io_sparse_constructor, &
   mod_octave_io_sparse_destructor,  &
   mod_octave_io_sparse_initialized, &
   write_octave, read_octave

 private

!-----------------------------------------------------------------------

! Module types and parameters

! Module variables

 ! public members
 logical, protected ::               &
   mod_octave_io_sparse_initialized = .false.

 character(len=*), parameter :: &
   this_mod_name = 'mod_octave_io_sparse'

 interface write_octave
   module procedure write_col, write_tri, write_pm_sk
 end interface

 interface read_octave
   module procedure read_col
 end interface

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_octave_io_sparse_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
       (mod_kinds_initialized.eqv..false.)    .or. &
       (mod_octave_io_initialized.eqv..false.).or. &
       (mod_sparse_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_octave_io_sparse_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_octave_io_sparse_initialized = .true.
 end subroutine mod_octave_io_sparse_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_octave_io_sparse_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_octave_io_sparse_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_octave_io_sparse_initialized = .false.
 end subroutine mod_octave_io_sparse_destructor

!-----------------------------------------------------------------------
 
 subroutine write_col(m,var_name,fu)
  integer, intent(in) :: fu
  type(t_col), intent(in) :: m
  character(len=*), intent(in) :: var_name
 
  integer :: j,p
  character(len=8+len(real_format)) :: out_format
  character(len=*), parameter :: &
    this_sub_name = 'write_col'

   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: sparse matrix'
   write(fu,'(a,i15)')  '# nnz: '    ,m%nz
   write(fu,'(a,i7)')   '# rows: '   ,m%n
   write(fu,'(a,i7)')   '# columns: ',m%m

   write(out_format,'(a,a,a)') '(i7,i7,', real_format, ')'
   coldo: do j=1,m%m ! column loop
     rowdo: do p=m%ap(j)+1,m%ap(j+1)
       write(fu,out_format) m%ai(p)+1 , j , m%ax(p)
     enddo rowdo
   enddo coldo
 
 end subroutine write_col
 
!-----------------------------------------------------------------------
 
 subroutine write_tri(t,var_name,fu)
  integer, intent(in) :: fu
  type(t_tri), intent(in) :: t
  character(len=*), intent(in) :: var_name
 
  character(len=*), parameter :: &
    this_sub_name = 'write_tri'

   call write_octave(tri2col(t),var_name,fu)
 
 end subroutine write_tri
 
!-----------------------------------------------------------------------

 !> Write a matrix skeleton
 !!
 !! A matrix skeleton provides two kinds of logical indexing:
 !! <ul>
 !!  <li> from a skeleton index \c l to a position within the
 !!  compressed matrix storage provided by
 !!  \field_fref{mod_sparse,t_col,ax} 
 !!  <li> from a skeleton index \c l to an entry \f$i,j\f$ within a
 !!  sparse matrix.
 !! </ul>
 !! The first kind of indexing is directly represented by
 !! \field_fref{mod_sparse,t_pm_sk,t2c}, while the second one, despite
 !! being uniquely defined by the information stored in a matrix
 !! skeleton, is not directly available given the fields of \c
 !! \fref{mod_sparse,t_pm_sk}.
 !!
 !! On top of this, there are here two complications:
 !! <ul>
 !!  <li> \fref{mod_sparse,t_pm_sk} refers to a <em>partitioned</em>
 !!  matrix
 !!  <li> because of this, \fref{mod_sparse,t_pm_sk} uses pointers,
 !!  which have no octave counterpart.
 !! </ul>
 !!
 !! In this subroutine, concerning the firsk kind of indexing, the
 !! pointers contained in \field_fref{mod_sparse,t_pm_sk,t2c} are
 !! converted into integer indexes, where the matrices stored in
 !! \field_fref{mod_sparse,t_pm_sk,m} are traversed in column major
 !! order, i.e. the first indexes refer to the <tt>sk\%m(1,1)\%nz</tt>
 !! entries of <tt>sk\%m(1,1)</tt>, then the following indexes refer
 !! to the nonzero entries of <tt>sk\%m(2,1)</tt>, and so on.
 !!
 !! Concerning the second kind of indexing, i.e. recovering the
 !! indexes \f$i,j\f$ within each submatrix, an additional field \c
 !! t2ij is included in the octave output, which has no direct
 !! counterpart in the matrix skeleton but can be uniquely derived
 !! given such skeleton. This field has four rows and
 !! <tt>sk\%n_in</tt> columns; given the index \c l, the entry
 !! <tt>sk\%t2c(l)</tt> of the skeleton corresponds to the following
 !! submatrix entry:
 !! <ul>
 !!  <li> submatrix <tt>sk\%m(im,jm)</tt> with <tt>im,jm =
 !!  t2ij(1:2,l)</tt>
 !!  <li> local entry <tt>i,j</tt> with <tt>i,j = t2ij(3:4,l)</tt>
 !!  (these last indexes are zero based, since they are row/column
 !!  indexes of a sparse matrix).
 !! </ul>
 !!
 !! \warning This subroutine might requires a significant amount of
 !! memory and disk storage.
 subroutine write_pm_sk(sk,var_name,fu)
  integer, intent(in) :: fu
  type(t_pm_sk), intent(inout), target :: sk
  character(len=*), intent(in) :: var_name
 
  type :: t_matax
   real(wp), allocatable :: ax(:)
   ! additional fields, used to derive the t2ij correspondence
   integer, allocatable :: ii(:), jj(:)
  end type t_matax
  type(t_matax), allocatable :: matax(:,:)

  integer :: i,j,l, prev
  integer, allocatable :: t2ij(:,:)
  character(len=*), parameter :: &
    this_sub_name = 'write_pm_sk'
   
   ! To write the pointers, we write integer values in data array of
   ! the matrix and use them to translate the pointers into integer
   ! indexes. Such indexes are 1-based, since they index an array, not
   ! a sparse matrix.

   ! After writing the data, the original matrix entries are restored.

   ! Make a temporary copy of the matrix entries and set the integer
   ! indexes used to convert the pointers
   allocate( matax( size(sk%m,1) , size(sk%m,2) ) )
   prev = 0
   do j=1,size(sk%m,2)
     do i=1,size(sk%m,1)
       associate( nz => sk%m(i,j)%nz )
       allocate( matax(i,j)%ax( nz ) )
       matax(i,j)%ax = sk%m(i,j)%ax
       sk%m(i,j)%ax = (/ (real(l,wp), l=prev+1,prev+nz ) /)
       prev = prev + nz
       end associate
       ! used to recover the i,j indexes of each matrix
       call col_aij( matax(i,j)%ii , matax(i,j)%jj , sk%m(i,j) )
     enddo
   enddo

   ! Write the skeleton

   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: scalar struct'
   write(fu,'(a)')      '# length: 4' ! number of fields

   ! field 01 : n_in
   call write_octave(sk%n_in,'n_in',fu)

   ! field 02 : t2c
   call write_octave( (/( int(sk%t2c(l)%p) , l=1,sk%n_in )/) , &
                      'r','t2c',fu)

   ! Add some more information to recover the i,j indexes
   allocate( t2ij(4,sk%n_in) )

   do j=1,size(sk%m,2)
     do i=1,size(sk%m,1)
       sk%m(i,j)%ax = real(i,wp) ! submatrix index i
     enddo
   enddo
   t2ij(1,:) = (/( int(sk%t2c(l)%p) , l=1,sk%n_in )/)
   do j=1,size(sk%m,2)
     do i=1,size(sk%m,1)
       sk%m(i,j)%ax = real(j,wp) ! submatrix index j
     enddo
   enddo
   t2ij(2,:) = (/( int(sk%t2c(l)%p) , l=1,sk%n_in )/)
   do j=1,size(sk%m,2) ! local index i
     do i=1,size(sk%m,1)
       sk%m(i,j)%ax = real(matax(i,j)%ii,wp)
     enddo
   enddo
   t2ij(3,:) = (/( int(sk%t2c(l)%p) , l=1,sk%n_in )/)
   do j=1,size(sk%m,2) ! local index j
     do i=1,size(sk%m,1)
       sk%m(i,j)%ax = real(matax(i,j)%jj,wp)
     enddo
   enddo
   t2ij(4,:) = (/( int(sk%t2c(l)%p) , l=1,sk%n_in )/)

   ! field 03 : m
   call write_octave( t2ij , 't2ij',fu)
   deallocate( t2ij )

   ! Restore the original values
   do j=1,size(sk%m,2)
     do i=1,size(sk%m,1)
       sk%m(i,j)%ax = matax(i,j)%ax
     enddo
   enddo
   deallocate( matax )

   ! field 04 : m
   write(fu,'(a)')      '# name: m'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# ndims: 2'
   write(fu,'(i0,a,i0)') size(sk%m,1)," ",size(sk%m,2)
   do j=1,size(sk%m,2)
     do i=1,size(sk%m,1)
       call write_octave(sk%m(i,j),'<cell-element>',fu)
     enddo
   enddo

 end subroutine write_pm_sk

!-----------------------------------------------------------------------

 subroutine read_tri(t,var_name,fu)
  integer, intent(in) :: fu
  type(t_tri), intent(out) :: t
  character(len=*), intent(in) :: var_name
 
  integer :: ierr, innz, inrow, incol, k
  integer, allocatable :: ii(:), jj(:)
  real(wp), allocatable :: xx(:)
  character(len=100) :: message
  character(len=*), parameter :: &
    this_sub_name = 'read_tri'

   call locate_var(fu,var_name,ierr)
   if(ierr.ne.0) then
     write(message,'(A,A,A,I3)') &
       'Problems locating "',var_name,'": iostat = ',ierr
     call warning(this_sub_name,this_mod_name,message)
   else
     read(fu, '(A)'     ) message ! # type: sparse matrix
     read(fu, '(A6,I10)') message, innz  ! "# nnz:"
     read(fu, '(A7,I10)') message, inrow ! "# rows:"
     read(fu,'(A10,I10)') message, incol ! "# columns:"
     allocate( ii(innz) , jj(innz) , xx(innz) )
     do k=1,innz
       read(fu,*) ii(k), jj(k), xx(k)
       ! switch to zero based indexes
       ii(k) = ii(k)-1
       jj(k) = jj(k)-1
     enddo
     t = new_tri(inrow,incol,ii,jj,xx)
     deallocate( ii , jj , xx )
   endif

 end subroutine read_tri
 
!-----------------------------------------------------------------------

 subroutine read_col(m,var_name,fu)
  integer, intent(in) :: fu
  type(t_col), intent(out) :: m
  character(len=*), intent(in) :: var_name

  type(t_tri) :: t

   call read_tri(t,var_name,fu)
   m = tri2col(t)
   call clear(t)
 end subroutine read_col

!-----------------------------------------------------------------------

end module mod_octave_io_sparse

