!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!> \brief
!!
!! Finite element projections
!!
!! \n
!!
!! This module collects some finite element projections. These
!! projections relies on the local finite element bases defined in \c
!! mod_h2d_base.
!!
!! \section Raviart-Thomas projection
!!
!! We consider here the following problem: given \f${\bf v}_h,d_h\f$
!! such that
!! \f{displaymath}{
!!  \| {\bf v} - {\bf v}_h \|_{L^2(\Omega)} \to 0, \qquad
!!  \| \nabla\cdot{\bf v} - d_h \|_{L^2(\Omega)} \to 0,
!! \f}
!! find a vector field \f${\bf w}_h\f$ such that
!! \f{displaymath}{
!!  \| {\bf v} - {\bf w}_h \|_{H({\rm div},\Omega)} \to 0, \qquad
!!  {\bf w}_h|_K \in \mathbb{RT}_0(K).
!! \f}
!! The construction relies on side bubble functions \f$b_e\f$ such
!! that \f${\rm supp}(b_e) = K^-\cup K^+\f$, where \f$e=K^-\cap
!! K^+\f$, with \f$b_e\in H^1(\Omega)\f$.
!<----------------------------------------------------------------------
module mod_h2d_feproj
!General comments:
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------

 use mod_used, only: &

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_h2d_feproj_constructor, &
   mod_h2d_feproj_destructor,  &
   mod_h2d_feproj_initialized

 private

!-----------------------------------------------------------------------

! Module types and parameters

 ! public members
 logical, parameter ::             &
 integer, parameter ::             &
 ! private members
 real, parameter ::                &

! Module variables

 ! public members
 logical, protected ::               &
   mod_h2d_feproj_initialized = .false.
 integer ::               &
 ! private members
 real ::                  &

 character(len=*), parameter :: &
   this_mod_name = 'mod_h2d_feproj'

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_h2d_feproj_constructor()
  integer, intent(in) :: &
  real, intent(in) ::    &

  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_used1.eqv..false.) .or. &
       (mod_used2.eqv..false.) ) then
   endif
   if(mod_h2d_feproj_initialized.eqv..true.) then
   endif
   !----------------------------------------------

   mod_h2d_feproj_initialized = .true.
 end subroutine mod_h2d_feproj_constructor

!-----------------------------------------------------------------------
 
 subroutine mod_h2d_feproj_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_h2d_feproj_initialized.eqv..false.) then
   endif
   !----------------------------------------------

   mod_h2d_feproj_initialized = .false.
 end subroutine mod_h2d_feproj_destructor

!-----------------------------------------------------------------------

end module mod_h2d_feproj

