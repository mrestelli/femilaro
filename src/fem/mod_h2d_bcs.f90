!! Copyright (C) 2009,2010,2011,2012  Marco Restelli
!!
!! This file is part of:
!!   FEMilaro -- Finite Element Method toolkit
!!
!! FEMilaro is free software; you can redistribute it and/or modify it
!! under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 3 of the License, or
!! (at your option) any later version.
!!
!! FEMilaro is distributed in the hope that it will be useful, but
!! WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
!! General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
!!
!! author: Marco Restelli                   <marco.restelli@gmail.com>

!>\brief
!!
!! 2D version of \c mod_bcs for hybrid grids.
!!
!! \n
!!
!! This module is very similar to \c mod_bcs, except that it relies on
!! mod_h2d_grid instead of \c mod_grid.
!!
!! Since in 2D the element sides are segments, regardless of the
!! element shape, this module does not poses any specific difficulty
!! and one can refer to the documentation of \c mod_bcs.
!!
!! \todo All the part corresponding to the domain decomposition as
!! well as the ghost cells is not yet implemented.
!<----------------------------------------------------------------------
module mod_h2d_bcs

!-----------------------------------------------------------------------

 use mod_messages, only: &
   mod_messages_initialized, &
   error,   &
   warning, &
   info

 use mod_kinds, only: &
   mod_kinds_initialized, &
   wp

 use mod_mpi_utils, only: &
   mod_mpi_utils_initialized, &
   mpi_integer, wp_mpi,  &
   mpi_alltoallv

 use mod_octave_io, only: &
   mod_octave_io_initialized, &
   write_octave

 use mod_perms, only: &
   mod_perms_initialized, &
   operator(*),     &
   operator(**),    &
   idx

 use mod_h2d_grid, only: &
   mod_h2d_grid_initialized, &
   t_2dv, t_2ds, t_2de, &
   t_h2d_grid, t_ddc_h2d_grid

!-----------------------------------------------------------------------
 
 implicit none

!-----------------------------------------------------------------------

! Module interface

 public :: &
   mod_h2d_bcs_constructor, &
   mod_h2d_bcs_destructor,  &
   mod_h2d_bcs_initialized, &
   t_h2d_bcs,                       &
   t_b_2dv,   t_b_2ds,   t_b_2de,   &
   p_t_b_2dv, p_t_b_2ds, p_t_b_2de, &
   b_dir, b_neu, b_ddc, b_null,     &
   new_bcs, clear,          &
   write_octave,            &
   ! Only for testing ----
   mod_h2d_bcs_constructor_test

 private

!-----------------------------------------------------------------------

! Module types and parameters

 ! public members

 ! module constants
 integer, parameter :: &
   b_dir = 1, &
   b_neu = 2, &
   b_ddc = 3, &
   ! The next value is defined for convenience and it is different
   ! from any defined b_* parameter.
   b_null = -1

 ! pointer arrays
 type p_t_b_2dv
   type(t_b_2dv), pointer :: p => null()
 end type p_t_b_2dv
 type p_t_b_2ds
   type(t_b_2ds), pointer :: p => null()
 end type p_t_b_2ds
 type p_t_b_2de
   type(t_b_2de), pointer :: p => null()
 end type p_t_b_2de

 !> boundary vertex
 type t_b_2dv
   type(t_2dv), pointer :: v => null() !< corresponding vertex object
   integer :: bc !< boundary condition: \c b_dir, \c b_neu or \c b_ddc
   integer :: btype !< the user provided index is copied here
   integer :: breg  !< boundary region
   logical :: ddc = .false. !< shared vertex
 end type t_b_2dv

 !> boundary side
 type t_b_2ds
   type(t_2ds), pointer :: s => null() !< corresponding side object
   integer :: bc !< boundary condition: \c b_dir, \c b_neu or \c b_ddc
   integer :: btype !< the user provided index is copied here
   logical :: ddc = .false. !< shared side
   !type(t_2ds) :: s_copy !< side copy with pointer to ghost element
 end type t_b_2ds

 !> boundary element
 type t_b_2de
   type(t_2de), pointer :: e => null() !< corresponding element object
   integer :: nbv !< number of boundary vertices
   type(p_t_b_2dv), allocatable :: bv(:) !< boundary vertices
   integer :: nbs !< number of boundary sides
   type(p_t_b_2ds), allocatable :: bs(:) !< boundary sides
 end type t_b_2de

 !> Boundary conditions
 !!
 !! See the documentation of \fref{mod_bcs,t_bcs}.
 type t_h2d_bcs
   integer :: nbreg   !< number of boundary regions
   integer :: nb_vert !< size of \c b_vert
   integer :: nb_side !< size of \c b_side
   integer :: nb_elem !< size of \c b_elem
   type(t_b_2dv), allocatable :: b_vert(:)
   type(t_b_2ds), allocatable :: b_side(:)
   type(t_b_2de), allocatable :: b_elem(:)
   type(p_t_b_2dv), allocatable :: b_v2bv(:) !< \f$v \to v_b\f$
   type(p_t_b_2ds), allocatable :: b_s2bs(:) !< \f$s \to s_b\f$
   type(p_t_b_2de), allocatable :: b_e2be(:) !< \f$e \to e_b\f$
   ! ghost entities (todo)
   !type(t_e), allocatable, private :: ghost_elements(:)
 end type t_h2d_bcs

! Module variables

 ! public members
 logical, protected ::               &
   mod_h2d_bcs_initialized = .false.

 ! private members
 character(len=*), parameter :: &
   this_mod_name = 'mod_h2d_bcs'

 interface new_bcs
   module procedure new_h2d_bcs
 end interface

 interface clear
   module procedure clear_h2d_bcs
 end interface

 interface write_octave
   module procedure write_h2d_bcs_struct
 end interface write_octave

!-----------------------------------------------------------------------

contains

!-----------------------------------------------------------------------

 subroutine mod_h2d_bcs_constructor()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
          (mod_kinds_initialized.eqv..false.) .or. &
      (mod_mpi_utils_initialized.eqv..false.) .or. &
      (mod_octave_io_initialized.eqv..false.) .or. &
          (mod_perms_initialized.eqv..false.) .or. &
       (mod_h2d_grid_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_h2d_bcs_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_h2d_bcs_initialized = .true.
 end subroutine mod_h2d_bcs_constructor

 subroutine mod_h2d_bcs_constructor_test()
  character(len=*), parameter :: &
    this_sub_name = 'constructor'

   !Consistency checks ---------------------------
   if( (mod_messages_initialized.eqv..false.) .or. &
          (mod_kinds_initialized.eqv..false.) .or. &
      (mod_mpi_utils_initialized.eqv..false.) .or. &
   ! not needed if we avoid write -> mod_octave_io_initialized
          (mod_perms_initialized.eqv..false.) .or. &
       (mod_h2d_grid_initialized.eqv..false.) ) then
     call error(this_sub_name,this_mod_name, &
                'Not all the required modules are initialized.')
   endif
   if(mod_h2d_bcs_initialized.eqv..true.) then
     call warning(this_sub_name,this_mod_name, &
                  'Module is already initialized.')
   endif
   !----------------------------------------------

   mod_h2d_bcs_initialized = .true.
 end subroutine mod_h2d_bcs_constructor_test

!-----------------------------------------------------------------------
 
 subroutine mod_h2d_bcs_destructor()
  character(len=*), parameter :: &
    this_sub_name = 'destructor'
   
   !Consistency checks ---------------------------
   if(mod_h2d_bcs_initialized.eqv..false.) then
     call error(this_sub_name,this_mod_name, &
                'This module is not initialized.')
   endif
   !----------------------------------------------

   mod_h2d_bcs_initialized = .false.
 end subroutine mod_h2d_bcs_destructor

!-----------------------------------------------------------------------

 !> See the documentation of \fref{mod_bcs,new_bcs}.
 subroutine new_h2d_bcs(bcs,grid,bc_type , ddc_grid,comm)
  type(t_h2d_bcs), target, intent(out) :: bcs  !< boundary condition
  type(t_h2d_grid), target, intent(in) :: grid !< grid
  integer, intent(in) :: bc_type(:,:) !< boundary cnd. type and kind
  !> domain decomposition grid
  type(t_ddc_h2d_grid), intent(in), optional :: ddc_grid
  integer, intent(in), optional :: comm

  integer :: iv, ivb, ivl, is, isb, isl, ie, ieb, breg, bc
  integer :: ddc_marker
  character(len=1000) :: message
  type(t_b_2dv), target :: bv_dummy
  type(t_b_2de), target :: be_dummy
  type(p_t_b_2dv) :: bv_temp(maxval(grid%e%poly))
  type(p_t_b_2ds) :: bs_temp(maxval(grid%e%poly))
  character(len=*), parameter :: &
    this_sub_name = 'new_h2d_bcs'

  ! ddc specific variables
  logical :: ddc_present
  integer, parameter :: dim1 = 4
  integer :: ierr
  integer, allocatable :: sendbuf(:,:), recvbuf(:,:), ioff(:)

  ! To avoid cluttering this function, it's better to define here some
  ! pointers
  integer, pointer :: nb_vert
  integer, pointer :: nb_side
  integer, pointer :: nb_elem
  type(t_b_2dv), pointer :: b_vert(:)
  type(t_b_2ds), pointer :: b_side(:)
  type(t_b_2de), pointer :: b_elem(:)
  type(p_t_b_2dv), pointer :: b_v2bv(:)
  type(p_t_b_2ds), pointer :: b_s2bs(:)
  type(p_t_b_2de), pointer :: b_e2be(:)
   nb_vert => bcs%nb_vert
   nb_side => bcs%nb_side
   nb_elem => bcs%nb_elem

   ! Number of boundary regions
   bcs%nbreg = size(bc_type,2)

   ! Boundary sides: immediately derived by the grid (last ones)
   allocate(bcs%b_s2bs(grid%ni+1:grid%ns)); b_s2bs => bcs%b_s2bs
   nb_side = grid%nb
   allocate(bcs%b_side(nb_side)); b_side => bcs%b_side

   ! Boundary vertex
   allocate(bcs%b_v2bv(grid%nv)); b_v2bv => bcs%b_v2bv

   ! Boundary elements
   allocate(bcs%b_e2be(grid%ne)); b_e2be => bcs%b_e2be

   ! Whether to check or not ddc stuff
   ddc_present = present(ddc_grid)
   if(ddc_present) then
     ddc_marker = ddc_grid%ddc_marker
   else
     ddc_marker = -1 ! ddc_marker is significant only if ddc_present
   endif

   ! fill the side structure and count nb_vert and nb_elem
   nb_vert = 0
   nb_elem = 0
   do is=grid%ni+1,grid%ns
     isb = is-grid%ni
     b_s2bs(is)%p => b_side(isb)
     b_side(isb)%s => grid%s(is)
     breg = -grid%s(is)%ie(2) ! boundary region (notice: breg>0 )
     if(ddc_present.and.(breg.eq.ddc_marker)) then ! found a ddc side
       b_side(isb)%bc = b_ddc
       ! b_side(isb)%btype left uninitialized
       b_side(isb)%ddc = .true.
     else
       bc = bc_type(1,breg)
       btype: select case(bc)
        case(b_dir)
         b_side(isb)%bc = b_dir
        case(b_neu)
         b_side(isb)%bc = b_neu
        case default
         write(message,'(A,I4,A,I7,A,I4,A)') &
           'Unknown boundary condition type "',bc,'" on side ',is, &
           ' on boundary region ',breg,'.'
         call error(this_sub_name,this_mod_name,message)
       end select btype
       b_side(isb)%btype = bc_type(2,breg)
       ! ddc left to .false.
     endif
     ! mark the vertexes
     do ivl=1,2
       if(.not.associated(b_v2bv(grid%s(is)%iv(ivl))%p)) then
         b_v2bv(grid%s(is)%iv(ivl))%p => bv_dummy
         nb_vert = nb_vert+1
       endif
     enddo
     ! mark the element
     if(.not.associated(b_e2be(grid%s(is)%ie(1))%p)) then
       b_e2be(grid%s(is)%ie(1))%p => be_dummy
       nb_elem = nb_elem+1
     endif
   enddo

   ! now fill the vertexes
   allocate(bcs%b_vert(nb_vert)); b_vert => bcs%b_vert
   ivb = 0
   do iv=1,grid%nv
     boundary_vert_if: if(associated(b_v2bv(iv)%p)) then ! bnd vertex

       ivb = ivb+1
       b_v2bv(iv)%p => b_vert(ivb)
       b_vert(ivb)%v => grid%v(iv)
       b_vert(ivb)%bc = b_null
       ! now check the boundary data by looping on the connected
       ! boundary sides
       do isl=1,grid%v(iv)%ns
         is = grid%v(iv)%is(isl)
         if(is.gt.grid%ni) then ! found a connected boundary side
           call update_bv( b_vert(ivb) , b_s2bs(is)%p%bc , &
                    b_s2bs(is)%p%btype , -grid%s(is)%ie(2) )
         endif
       enddo

       ! A vertex can have ddc information even if it is not of b_ddc
       ! type (this happens because b_dir and b_neu take precedence
       ! over b_ddc)
       if(ddc_present) then ! otherwise no need to check
         b_vert(ivb)%ddc = ddc_grid%gv(iv)%ni.gt.0
       endif

     endif boundary_vert_if
   enddo

   ! Now synchronize the subdomains concerning boundary vertexes
   if(ddc_present) then
     ! It's safer to avoid call to mpi_alltoallv with empty arrays
     !
     ! TODO: this is a problem in case there is one subdomain
     ! completely isolated, since then mpi_alltoallv would not be
     ! called by all the processors in the communicator. One should
     ! find a better solution.
     if(sum(ddc_grid%nnv_id).gt.0) then
       allocate( sendbuf(dim1,sum(ddc_grid%nnv_id)) , &
                 recvbuf(dim1,sum(ddc_grid%nnv_id)) )
       allocate(ioff(0:ddc_grid%nd-1));
       ioff(0) = 0
       do ie=1,ddc_grid%nd-1
         ioff(ie) = ioff(ie-1) + ddc_grid%nnv_id(ie-1)
       enddo
       ! fill sendbuff
       do ivb=1,ddc_grid%nnv ! loop on the ddc vertexes
         do is=1,ddc_grid%nv(ivb)%nd
           ie = ddc_grid%nv(ivb)%id(is) ! as temporary: neigh. subd.
           ioff(ie) = ioff(ie) + 1
           sendbuf(1,ioff(ie)) = ddc_grid%nv(ivb)%in(is) ! vertex index
           sendbuf(2,ioff(ie)) = b_v2bv( ddc_grid%nv(ivb)%i )%p%bc
           sendbuf(3,ioff(ie)) = b_v2bv( ddc_grid%nv(ivb)%i )%p%btype
           sendbuf(4,ioff(ie)) = b_v2bv( ddc_grid%nv(ivb)%i )%p%breg
         enddo
       enddo
       ! reset the offsets
       ioff(0) = 0
       do ie=1,ddc_grid%nd-1
         ioff(ie) = ioff(ie-1) + ddc_grid%nnv_id(ie-1)
       enddo
       call mpi_alltoallv(                                           &
              sendbuf, dim1*ddc_grid%nnv_id, dim1*ioff, mpi_integer, &
              recvbuf, dim1*ddc_grid%nnv_id, dim1*ioff, mpi_integer, &
                          comm,ierr)
       ! now make the necessary corrections
       do ivb=1,size(recvbuf,2)
         call update_bv( b_v2bv(recvbuf(1,ivb))%p , recvbuf(2,ivb) , &
                                recvbuf(3,ivb)    , recvbuf(4,ivb) )
       enddo
       deallocate( sendbuf , recvbuf , ioff )
     endif
   endif

   ! now fill the elements
   allocate(bcs%b_elem(nb_elem)); b_elem => bcs%b_elem
   ieb = 0
   do ie=1,grid%ne
     if(associated(b_e2be(ie)%p)) then ! boundary element
       ieb = ieb+1
       b_e2be(ie)%p => b_elem(ieb)
       b_elem(ieb)%e => grid%e(ie)
       ! boundary connectivity
       b_elem(ieb)%nbv = 0
       do ivl=1,grid%e(ie)%poly ! loop on the vertexes
         iv = grid%e(ie)%iv(ivl)
         if(associated(b_v2bv(iv)%p)) then
           b_elem(ieb)%nbv = b_elem(ieb)%nbv+1
           bv_temp(b_elem(ieb)%nbv)%p => b_v2bv(iv)%p
         endif
       enddo
       allocate(b_elem(ieb)%bv(b_elem(ieb)%nbv))
       b_elem(ieb)%bv = bv_temp(1:b_elem(ieb)%nbv)

       b_elem(ieb)%nbs = 0
       do isl=1,grid%e(ie)%poly ! loop on the sides
         is = grid%e(ie)%is(isl)
         if(is.gt.grid%ni) then
           b_elem(ieb)%nbs = b_elem(ieb)%nbs+1
           bs_temp(b_elem(ieb)%nbs)%p => b_s2bs(is)%p
         endif
       enddo
       allocate(b_elem(ieb)%bs(b_elem(ieb)%nbs))
       b_elem(ieb)%bs = bs_temp(1:b_elem(ieb)%nbs)
     endif
   enddo

!   ! Finally the ghost cells
!   if(ddc_present) &
!     call define_ghost_entities(bcs,grid,ddc_grid,comm)

 contains

  pure subroutine update_bv(bv,bc,btype,breg)
  ! Make sure bv%bc is set on input
   type(t_b_2dv), intent(inout) :: bv
   integer, intent(in) :: bc, btype, breg

    select case(bc)

     case(b_dir)
      if(bv%bc.ne.b_dir) then ! first Dirichlet condition
        bv%bc    = b_dir
        bv%btype = btype
        bv%breg  = breg
        ! nothing to do for the ddc fields
      else ! already of dir. type
        if(bv%breg.gt.breg) then
          bv%btype = btype
          bv%breg  = breg
        endif
      endif

     case(b_neu)
      if(bv%bc.ne.b_dir) then ! Dir. sides take precedence
        if(bv%bc.ne.b_neu) then ! first Neumann side
          bv%bc    = b_neu
          bv%btype = btype
          bv%breg  = breg
          ! nothing to do for the ddc fields
        else ! already of neu. type
          if(bv%breg.gt.breg) then
            bv%btype = btype
            bv%breg  = breg
          endif
        endif
      endif

     case(b_ddc)
      if((bv%bc.ne.b_dir).and.(bv%bc.ne.b_neu)) then
        if(bv%bc.ne.b_ddc) then ! first ddc side
          bv%bc    = b_ddc
          ! bv%btype left uninitialized
          bv%breg  = breg ! = ddc_marker
          bv%ddc   = .true.
        else ! already of ddc type
          ! Notice that in practice the following will never be
          ! executed, since breg is always ddc_marker
          if(bv%breg.gt.breg) then
            ! bv%btype left uninitialized
            bv%breg  = breg ! = ddc_marker
          endif
        endif
      endif
      
    end select

  end subroutine update_bv

 end subroutine new_h2d_bcs

!-----------------------------------------------------------------------

 pure subroutine clear_h2d_bcs( bcs )
  type(t_h2d_bcs), intent(out) :: bcs
 end subroutine clear_h2d_bcs
 
!-----------------------------------------------------------------------

 subroutine write_h2d_bcs_struct(bcs,var_name,fu)
 ! octave output for the boundary conditions
  integer, intent(in) :: fu
  type(t_h2d_bcs), intent(in) :: bcs
  character(len=*), intent(in) :: var_name
 
  integer :: i
  integer, allocatable :: tmp(:,:)
  character(len=*), parameter :: &
    this_sub_name = 'write_h2d_bcs_struct'
 
   write(fu,'(a,a)')    '# name: ',var_name
   write(fu,'(a)')      '# type: struct'
   write(fu,'(a)')      '# length: 6' ! number of fields

   ! field 01 : nbreg
   write(fu,'(a)')      '# name: nbreg'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(bcs%nbreg,'<cell-element>',fu)

   ! field 02 : nb_vert
   write(fu,'(a)')      '# name: nb_vert'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(bcs%nb_vert,'<cell-element>',fu)

   ! field 03 : nb_side
   write(fu,'(a)')      '# name: nb_side'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(bcs%nb_side,'<cell-element>',fu)

   ! field 04 : nb_elem
   write(fu,'(a)')      '# name: nb_elem'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   call write_octave(bcs%nb_elem,'<cell-element>',fu)

   ! field 05 : b_vert
   write(fu,'(a)')      '# name: b_vert'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   allocate(tmp(5,bcs%nb_vert))
   do i=1,bcs%nb_vert
     tmp(1,i) = bcs%b_vert(i)%v%i
     tmp(2,i) = bcs%b_vert(i)%bc
     tmp(3,i) = bcs%b_vert(i)%btype
     tmp(4,i) = bcs%b_vert(i)%breg
     if(bcs%b_vert(i)%ddc) then
       tmp(5,i) = 1
     else
       tmp(5,i) = 0
     endif
   enddo
   call write_octave(tmp,'<cell-element>',fu)
   deallocate(tmp)

   ! field 06 : b_side
   write(fu,'(a)')      '# name: b_side'
   write(fu,'(a)')      '# type: cell'
   write(fu,'(a)')      '# rows: 1'
   write(fu,'(a)')      '# columns: 1'
   allocate(tmp(4,bcs%nb_side))
   do i=1,bcs%nb_side
     tmp(1,i) = bcs%b_side(i)%s%i
     tmp(2,i) = bcs%b_side(i)%bc
     tmp(3,i) = bcs%b_side(i)%btype
     if(bcs%b_side(i)%ddc) then
       tmp(4,i) = 1
     else
       tmp(4,i) = 0
     endif
   enddo
   call write_octave(tmp,'<cell-element>',fu)
   deallocate(tmp)

 end subroutine write_h2d_bcs_struct

!-----------------------------------------------------------------------

end module mod_h2d_bcs

