"""
Python copy of mod_cgdofs.f90
"""

import numpy as np
import utilities as ut

#----------------------------------------------------------------------

class t_cgdofs():
  def __init__(self,data):
    self.d     = data['d']
    self.m     = data['m']
    self.ndofs = data['ndofs']

    d  = self.d
    m  = self.m

    ut.copy_array_field(self,'ndofsd'   ,data,(  d+1,    ))
    ut.copy_array_field(self,'dofs'     ,data,(  d+1,-1  ), shift=-1 )
    ut.copy_array_field(self,'x'        ,data,(  d  ,-1  ))
    ut.copy_array_field(self,'dir_dofs' ,data,(  3  ,-1  ))
    ut.copy_array_field(self,'idir_dofs',data,(  2  ,-1  ), shift=-1 )
    ut.copy_array_field(self,'nat_dofs' ,data,(      -1, ), shift=-1 )

#----------------------------------------------------------------------

