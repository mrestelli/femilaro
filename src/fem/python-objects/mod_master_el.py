"""
Python copy of mod_master_el.f90
"""

import numpy as np
import utilities as ut

#----------------------------------------------------------------------

class t_perm():
  def __init__(self,data):
    self.d = data['d']
    self.p = data['p']
    ut.copy_array_field( self , 'i' , data , (self.d,) , shift=-1 )

class t_simpl_list():
  def __init__(self,data):
    self.d  = data['d']
    self.ns = data['ns']
    ut.copy_array_field( self , 's' , data , (self.d+1,-1) , shift=-1 )

#----------------------------------------------------------------------

class t_me():
  def __init__(self,data):
    d = data['d'] # to simplify the following code

    self.d        = data['d']
    ut.copy_array_field( self , 'isv' , data , (d,-1) , shift=-1 )
    self.children = ut.make_np_array( t_simpl_list , data["children"] )
    self.pi_tab   = ut.make_np_array( t_perm       , data["pi_tab"]   )
    ut.copy_array_field( self , 'xv'  , data , (d,-1) )
    ut.copy_array_field( self , 'n'   , data , (d,-1) )
    ut.copy_array_field( self , 'a'   , data , (-1,) )
    self.vol      = data['vol']
    self.voldm1   = data['voldm1']

#----------------------------------------------------------------------

