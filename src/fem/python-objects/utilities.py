"""
This module collects some utilities which are useful in the conversion
from fortran data to python objects.
"""

import numpy as np

#----------------------------------------------------------------------

def copy_array_field(obj,f,data,dims,shift=0):
  """
  Add an array field to an object, taking care of:
  1) ensures the correct number of indices (see read_octxt.py)
  2) ensures that new objects are build, rather than references
  3) if necessary, corrects the indexes passing to zero based

  obj:   the object to which the field must be added
  f:     string with the name of the field to be added
  data:  provides the source data in data[f]
  dims:  dimensions of the array which must be created, one dimension
         can be set to -1 to compute it automatically
  shift: added to data[f], typically to switch to 0-based indexes
  """

  setattr( obj , f , np.reshape( data[f]+shift , dims ) )

#----------------------------------------------------------------------

def make_np_array(tc,data):
  """
  Map a type constructor tc over the elements of an array and pack the
  results into an np.array
  """
  return np.array( list( map( tc , data ) ) )

#----------------------------------------------------------------------

