# Copyright (C) 2009,2010,2011,2012  Marco Restelli
#
# This file is part of:
#   FEMilaro -- Finite Element Method toolkit
#
# FEMilaro is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# FEMilaro is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
#
# author: Marco Restelli                   <marco.restelli@gmail.com>

#---------------------------------------------------------
# General comments
#---------------------------------------------------------
# Set the general options for the compilation of FEMilaro. This file
# will be included by the main makefile, which could be the FEMilaro
# makefile or a user makefile linking the FEMilaro library. For this
# reason, one should not use here commands like $(shell pwd), which
# would give different results depending on where this file is
# included. For the same reason, relative paths should be also
# avoided.


#---------------------------------------------------------
# Initializations (do not change these settings!)
#---------------------------------------------------------
FEMILARO_CC_INCLUDE:=
FEMILARO_LDFLAGS:=
FEMILARO_LIBS:=


#-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-
#X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X
#
# Configuration section
#
#X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X
#-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-

#---------------------------------------------------------
# Main Directory
#---------------------------------------------------------
FEMILARO_DIR:=$(HOME)/GIT/FEMilaro

#---------------------------------------------------------
# Compiler
#---------------------------------------------------------
# F90 Compiler
FEMILARO_FC:=ifort
# How to tell the compiler where to look for .mod files
FEMILARO_MODFLAG:=-I
# C Compiler
FEMILARO_CC=icc

#---------------------------------------------------------
# MPI compiler (only necessary if MPI is used)
#---------------------------------------------------------
FEMILARO_MPI_IMPL:=intel
FEMILARO_MPIF90:=mpif90

#---------------------------------------------------------
# Compiler flags
#---------------------------------------------------------

# ifort flags
FEMILARO_FFLAGS:= -g -O0 -check all -stand f03 -warn all -fpe0 -traceback -ftrapuv -fp-stack-check
#FEMILARO_FFLAGS:= -g -O0
#FEMILARO_FFLAGS:= -O3 -g -funroll-loops -stand f03 -ipo
FEMILARO_FFLAGS:=$(FEMILARO_FFLAGS) -heap-arrays
FEMILARO_FFLAGS:=$(FEMILARO_FFLAGS) -g
FEMILARO_FFLAGS:=$(FEMILARO_FFLAGS) -assume byterecl -assume minus0 -assume noold_maxminloc -assume noold_unit_star -assume noold_xor -assume std_mod_proc_name
FEMILARO_FFLAGS:=$(FEMILARO_FFLAGS) -standard-semantics

# C compiler flags
FEMILARO_CFLAGS:= -DDLONG

#---------------------------------------------------------
# Linker
#---------------------------------------------------------
FEMILARO_LD:=$(FEMILARO_FC)

#---------------------------------------------------------
# Archive
#---------------------------------------------------------
FEMILARO_AR:=xiar
# With the Intel compiler, xiar can be also used. This indeed is the
# only way to get ipo in the library.
#  FEMILARO_AR:=$(realpath $(FEMILARO_FC))
#  FEMILARO_AR:=$(dir $(FEMILARO_AR))xiar

#---------------------------------------------------------
# Octave
#---------------------------------------------------------
FEMILARO_USING_OCTAVE:=No
ifeq ($(strip $(FEMILARO_USING_OCTAVE)),Yes)
  ifndef FEMILARO_OCTAVE
    FEMILARO_OCTAVE:=$(shell which octave)
    ifeq ($(strip $(FEMILARO_OCTAVE)),) 
      FEMILARO_USING_OCTAVE:=No
    endif
  endif
  ifndef MKOCTFILE
    MKOCTFILE:=$(shell which mkoctfile)
    ifeq ($(strip $(MKOCTFILE)),) 
      FEMILARO_USING_OCTAVE:=No 
    endif
  endif
else
  FEMILARO_USING_OCTAVE:=No
endif
ifeq ($(strip $(FEMILARO_USING_OCTAVE)),Yes)
  OCTLIBS:=$(shell $(MKOCTFILE) -p LFLAGS) \
           $(shell $(MKOCTFILE) -p OCTAVE_LIBS)     
endif

#---------------------------------------------------------
# OpenMP
#---------------------------------------------------------
# Note: select the number of threads with
#   export OMP_NUM_THREADS=n
# and optionally set the stack size of each thread with
#   export OMP_STACKSIZE=16M
FEMILARO_USING_OMP:=No
ifeq ($(strip $(FEMILARO_USING_OMP)),Yes)
  FEMILARO_FFLAGS:=$(FEMILARO_FFLAGS) -openmp
  FEMILARO_LDFLAGS:=$(FEMILARO_LDFLAGS) -openmp
else
  # make sure FEMILARO_USING_OMP is set to either "Yes" or "No", to
  # simplify subsequent checks
  FEMILARO_USING_OMP:=No
endif

#---------------------------------------------------------
# MPI
#---------------------------------------------------------
FEMILARO_USING_MPI:=Yes
ifeq ($(strip $(FEMILARO_USING_MPI)),Yes)
  # The Fortran compiler is redefined to the MPI wrapper
  ifeq ($(strip $(FEMILARO_MPI_IMPL)),intel)
    FEMILARO_FC:=$(FEMILARO_MPIF90) -fc=$(FEMILARO_FC)
  else
    $(error "Please add options for your MPI implementation.")
  endif
  FEMILARO_LD:=$(FEMILARO_FC)
else
  # make sure FEMILARO_USING_MPI is set to either "Yes" or "No", to
  # simplify subsequent checks
  FEMILARO_USING_MPI:=No
endif

#---------------------------------------------------------
# UMFPACK
#---------------------------------------------------------
FEMILARO_USING_UMFPACK:=No
ifeq ($(strip $(USING_UMFPACK)),Yes)
  FEMILARO_LIBS:=$(FEMILARO_LIBS) -lumfpack -lamd -latllapack -lgfortran
  FEMILARO_LDFLAGS:=$(FEMILARO_LDFLAGS) -L/usr/lib64/gcc/x86_64-pc-linux-gnu/4.5.3
else
  # make sure FEMILARO_USING_UMFPACK is set to either "Yes" or "No", to
  # simplify subsequent checks
  FEMILARO_USING_UMFPACK:=No
endif

#---------------------------------------------------------
# MUMPS
#---------------------------------------------------------
FEMILARO_USING_MUMPS:=Yes
ifeq ($(strip $(FEMILARO_USING_MUMPS)),Yes)
  ifneq ($(FEMILARO_USING_MPI),Yes)
    $(error "To use MUMPS you need MPI.")
  endif
  MUMPS_HOME=/u/system/SLES11/soft/mumps/4.10.0
  SCOTCH_HOME=/u/system/SLES11/soft/scotch/5.1.12
  METIS_HOME=/u/system/SLES11/soft/ParMetis/3.2.0

  # It is easier if we use static linking.
  #
  # Note: the MKL line has been obtained from
  # https://software.intel.com/en-us/articles/intel-mkl-link-line-advisor
  # and must be consistent with the one used to compile MUMPS; this
  # requires setting MKLROOT before compiling, typically with
  #   module load mkl/11.2
  FEMILARO_LIBS:=$(FEMILARO_LIBS) \
    -ldmumps -lmumps_common -lpord \
    -lscotch -lscotcherr -lesmumps -lptscotch \
    -lparmetis -lmetis \
    ${MKLROOT}/lib/intel64/libmkl_scalapack_lp64.a \
    -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a \
                      ${MKLROOT}/lib/intel64/libmkl_core.a \
                      ${MKLROOT}/lib/intel64/libmkl_sequential.a \
                      ${MKLROOT}/lib/intel64/libmkl_blacs_intelmpi_lp64.a \
    -Wl,--end-group -lpthread -lm -ldl
  # assuming that most libraries are in standard locations
  FEMILARO_LDFLAGS:=$(FEMILARO_LDFLAGS) \
    -L$(MUMPS_HOME)/lib -L$(SCOTCH_HOME)/lib -L$(METIS_HOME)/lib
  FEMILARO_MUMPS_INCLUDE:= -I$(MUMPS_HOME)/include
     
else
  # make sure FEMILARO_USING_MUMPS is set to either "Yes" or "No", to
  # simplify subsequent checks
  FEMILARO_USING_MUMPS:=No
endif

#---------------------------------------------------------
# PaStiX
#---------------------------------------------------------
FEMILARO_USING_PASTIX:=No
ifeq ($(strip $(FEMILARO_USING_PASTIX)),Yes)
  ifneq ($(FEMILARO_USING_MPI),Yes)
    $(error "To use PaStiX you need MPI.")
  endif
  FEMILARO_LIBS:=$(FEMILARO_LIBS) -lpastix_murge -lpastix \
   -lptscotch -lscotch -lhwloc \
   `pkg-config --libs blas` -lpthread
  FEMILARO_LDFLAGS:=$(FEMILARO_LDFLAGS) -L/home/marco/PROGI/lib
  FEMILARO_PASTIX_INCLUDE:= \
    -I/home/marco/PROGI/include
else
  # make sure FEMILARO_USING_PASTIX is set to either "Yes" or "No", to
  # simplify subsequent checks
  FEMILARO_USING_PASTIX:=No
endif

#-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-
#X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X
#
# End of configuration section
#
#X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X
#-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-X-


# Subdirectories
FEMILARO_SRCDIR  :=$(FEMILARO_DIR)/src
FEMILARO_BUILDDIR:=$(FEMILARO_DIR)/build
FEMILARO_LIBDIR  :=$(FEMILARO_DIR)/lib
FEMILARO_BINDIR  :=$(FEMILARO_DIR)/bin

