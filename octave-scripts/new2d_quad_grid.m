function [p,e,t] = new2d_quad_grid(N,limits)
% [p,e,t] = new2d_quad_grid(N,limits)
%
% Similar to new2d_grid but builds a quadrilateral grid with the
% format required by mod_h2d_grid.f90.

 nv = (N(1)+1)*(N(2)+1);
 p = zeros(2,nv);
 for i=1:N(1)+1
   for j=1:N(2)+1
     ij = (i-1)*(N(2)+1) + j;
     p(:,ij) = [i,j]' - 1;
   end
 end
 for i=1:2
   j = (i-1)*2 + 1;
   k = j+1;
   p(i,:) = ((limits(k)-limits(j))/N(i))*p(i,:) + limits(j);
 end

 ne = N(1)*N(2);
 it = zeros(4,ne);
 e = [];
 for i=1:N(1)
   for j=1:N(2)
     ie = (i-1)*N(2) + j; % element index
     ip = i+1;
     jp = j+1;
     it(1,ie) = (i -1)*(N(2)+1) + j;
     it(2,ie) = (ip-1)*(N(2)+1) + j;
     it(3,ie) = (ip-1)*(N(2)+1) + jp;
     it(4,ie) = (i -1)*(N(2)+1) + jp;
     if(i==1)
       e = [e, [it(4,ie);it(1,ie);0;0;4] ];
     endif
     if(i==N(1))
       e = [e, [it(2,ie);it(3,ie);0;0;2] ];
     endif
     if(j==1)
       e = [e, [it(1,ie);it(2,ie);0;0;1] ];
     endif
     if(j==N(2))
       e = [e, [it(3,ie);it(4,ie);0;0;3] ];
     endif
   end
 end

 t{1}.ie = [1:ne];
 t{1}.it = it;

return

