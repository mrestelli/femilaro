function [grids,p_fixed] = grid_partition_h2d(p,e,t,idx,varargin)
%
% grids = grid_partition_h2d(p,e,t,idx)
% grids = grid_partition_h2d(p,e,t,idx,periodic_faces)
% grids = grid_partition_h2d(p,e,t,idx,periodic_faces,data_names,data)
% [grids,p_fixed] = grid_partition_h2d(...)
%
% Partition the mesh (p,e,t) into length(idx) subgrids using the
% indexes in idx.
%
% Notice: idx can also be a function handle, in which case it is
% supposed to receive the coordinates of the element vertexes and
% return the partition index idx>=0.
%
% See the documentation of grid_partition for further details.

 %---------------------------------------------------------------------
 % Check the input arguments

 % periodicity
 if((nargin>4)&&(~isempty(varargin{1})))
   sideper = varargin{1};
   for i=1:size(sideper,2)
     VM{i} = [];
     VS{i} = [];
   end
   toll = 1e-10;
 else
   sideper = zeros(2,0);
 end
 % nodal data
 have_noda_data = 0;
 if(nargin>5)
   have_noda_data = 1;
   node_data_names = varargin{2};
   node_data       = varargin{3};
   if(numel(node_data_names)~=columns(node_data))
     error(['The number of data names is not consistent ' ...
            'with the number of data fields']);
   end
 end
 % partitioning could be provided as a function handle
 if(is_function_handle(idx))
   fh = idx;
   idx = 0; % count the elements
   for iit=1:length(t)
     idx = idx + length(t{iit}.ie);
   end
   idx = zeros(idx,1);
   for iit=1:length(t) % fill idx using the function handle
     for iie=1:size(t{iit}.it,2)
       idx(t{iit}.ie(iie)) = fh( p(:,t{iit}.it(:,iie)) );
     end
   end
 end
 %---------------------------------------------------------------------

 % initialize working arrays
 N = max(idx)+1;  % number of subgrids (idx starts from 0)
 dim = size(p,1); % space dimension
 iv_ren = zeros(N,size(p,2)); % local renumbering for each subgrid
 nv = zeros(N,1); % number of nodes for each subgrid
 nt = zeros(N,length(t)); % number of elements for each subgrid

 % loop over the elements to count the local nodes and elements
 for iit=1:length(t)
   nvit = size(t{iit}.it,1); % number of vertices
   for iie=1:size(t{iit}.it,2)
     ie = t{iit}.ie(iie);
     isg = idx(ie)+1; % index of the subgrid (starting from 1)
     nt(isg,iit) = nt(isg,iit) + 1;
     for l=1:nvit % loop on the nodes
       iv = t{iit}.it(l,iie);
       if(iv_ren(isg,iv)==0) % new local vertex
         nv(isg) = nv(isg) + 1;
         iv_ren(isg,iv) = nv(isg);
       end
     end
   end
 end

 % allocate the local grids
 for isg=1:N
   grids(isg) = struct( ...
     'p', zeros(dim,nv(isg)), ...
     'e', zeros(size(e)), ... % will be reshaped
     't', [], ...
     'ivl2ivn', [] , ...
     'vmap_l2g', zeros(1,nv(isg)) );
   for iit=1:length(t)
     nvit = size(t{iit}.it,1);
     grids(isg).t{iit} = struct( ...
         'ie', zeros(  1 ,nt(isg,iit)),...
         'it', zeros(nvit,nt(isg,iit)) );
   end
   grids(isg).ivl2ivn(1:nv(isg)) = struct( ...
     'nd', 0, ...
     'id', [], ...
     'iv', [] );
   % since iv_ren will not change, vmap_l2g can be already computed
   iv_g_on_i = find(iv_ren(isg,:)!=0); % global verts on local grid
   grids(isg).vmap_l2g(iv_ren(isg,iv_g_on_i)) = iv_g_on_i;
 end

 % fill grids.p
 for iv=1:size(p,2)
   for isg=1:N
     if(iv_ren(isg,iv)!=0) % vertex iv belongs to subgrid isg
       grids(isg).p(:,iv_ren(isg,iv)) = p(:,iv);
     end
   end
 end

 % fill grids.t
 nt(:,:) = 0; % reset the counter
 for iit=1:length(t)
   nvit = size(t{iit}.it,1);
   for iie=1:size(t{iit}.it,2)
     ie = t{iit}.ie(iie);
     isg = idx(ie)+1; % index of the subgrid
     nt(isg,iit) = nt(isg,iit) + 1;

     % insert the nodes
     iv = t{iit}.it(1:nvit,iie);               % global indexes
     grids(isg).t{iit}.ie(  nt(isg,iit)) = sum(nt(isg,:)); % local el
     grids(isg).t{iit}.it(:,nt(isg,iit)) = iv_ren(isg,iv); % local indexes
   end
 end

 % fill grids.ivl2ivn (periodicity information will be added later)
 for iv=1:size(p,2) % loop on the vertex of the full grid
   nsg = sum( (iv_ren(:,iv)!=0) ); % iv belongs to nsg subgrids
   if(nsg>1) % the node belongs to more than one grid
     isg = find( (iv_ren(:,iv)!=0) ); % subgrids which the node belongs to
     for i=1:length(isg)
       ign = setdiff(isg,isg(i)); % neighboring subgrids
       grids(isg(i)).ivl2ivn(iv_ren(isg(i),iv)) = struct( ...
         'nd', nsg-1, ... % number of subgrids sharing the node
         'id', ign-1, ... % subdomains are numbered starting from 0
         'iv', iv_ren(ign,iv)' ); % local indexes on neigh. subgrids
     end
   end
 end

 % partition e and include periodicity
 % This is done in the following steps:
 %  1) identify the boundary nodes
 %  2) define a vertex to elements pointer for the boundary nodes
 %  3) assign the boundary sides to the subgrids
 v2bv = zeros(size(p,2),1); % vertex to boundary vertex
 nbv  = 0;                  % number of boundary vertexes
 for ibs=1:size(e,2)
   % Check whether the face to which the boundary side belongs is
   % included in the list of master/slave periodic faces. We assume
   % that a face can appear in sideper only once.
   % i_ms: row index of sideper (i_ms=1->master, i_ms=2->slave)
   % i_sp: column index of sideper (index of side pair)
   [i_ms,i_sp] = find(sideper(1:2,:)==e(dim+2+1,ibs));
   if(isempty(i_ms)) % side is not periodic
     for ivl=1:dim
       iv = e(ivl,ibs);
       if(v2bv(iv)==0) % found a new boundary vertex
         nbv = nbv + 1;
         v2bv(iv) = nbv;
       end
     end
   else % side is periodic: add the connectivity information
     if(i_ms==1) % side belongs to the master face
       VM{i_sp} = [VM{i_sp},e(1:dim,ibs)']; % list of nodes (master)
     else % side belongs to the slave face
       VS{i_sp} = [VS{i_sp},e(1:dim,ibs)']; % list of nodes (slave)
     end
     % mark the side, so that it will be no longer considered a
     % boundary side
     e(1,ibs) = -1;
   end
 end
 if(nbv>0)
   bv2e{nbv} = []; % boundary vertex -> connected elements
 end
 for iit=1:length(t)
   nvit = size(t{iit}.it,1); % number of vertices
   for iie=1:size(t{iit}.it,2)
     ie = t{iit}.ie(iie);
     for ivl=1:nvit
       ibv = v2bv(t{iit}.it(ivl,iie));
       if(ibv!=0)
         bv2e{ibv} = [bv2e{ibv} , ie];
       end
     end
   end
 end
 nbs = zeros(N,1);
 for ibs=1:size(e,2)
   if(e(1,ibs)!=-1) % side is not marked
     % identify the connected element
     ie = bv2e{v2bv(e(1,ibs))};
     for ivl=2:dim
       ie = intersect(ie,bv2e{v2bv(e(ivl,ibs))});
     end
     if(length(ie)>1) % we should never get here
       error(['The boundary side ',num2str(ibs), ...
              ' belongs to more than one element: ',num2str(ie)]);
     end
     % now we know that the boundary side belongs to ie
     bs = e(:,ibs);
     isg = idx(ie) + 1; % subgrid index
     bs(1:dim,1) = iv_ren(isg,bs(1:dim,1)); % local node numbering
     nbs(isg) = nbs(isg) + 1;
     grids(isg).e(:,nbs(isg)) = bs; % assign the boundary side to isg
   end
 end
 for isg=1:N
   grids(isg).e = grids(isg).e(:,1:nbs(isg)); % trim grids.e
 end

 % fix the periodicity information
 p_fixed = p;
 for i=1:size(sideper,2)  % loop on the master faces
   vm = unique(VM{i}); % master nodes for this face (global numbering)
   vs = unique(VS{i}); % slave nodes for this face (global numbering)
   found_slave = zeros(size(vs));
   for ivm=1:length(vm)   % loop on the master nodes
     xvm = p(:,vm(ivm)) - sideper(3:end,i); % periodic translation
     found_master = 0; dist = inf;
     for ivs=1:length(vs) % loop on the slave nodes
       distn = norm(p(:,vs(ivs))-xvm);
       if(distn<=toll) % found
         found_master = 1;
         found_slave(ivs) = 1;

         % domains on the master side
         dom_ms = find(iv_ren(:,vm(ivm))!=0); % subdomain(s)
         num_ms = iv_ren(dom_ms,vm(ivm));     % local indexes

         % domains on the slave side
         dom_ss = find(iv_ren(:,vs(ivs))!=0); % subdomain(s)
         num_ss = iv_ren(dom_ss,vs(ivs));     % local indexes

         if(not(isempty(intersect(dom_ms,dom_ss))))
           error( [ ...
  'Error in the periodic boundary conditions:',sprintf('\n'),...
  '  subdomain ',num2str(intersect(dom_ms,dom_ss)), ...
  ' intersects with itself at nodes ',num2str([vm(ivm),vs(ivs)]) ...
           ]);
         endif

         % add data
         for iv=1:length(dom_ms)
           grids(dom_ms(iv)).ivl2ivn(num_ms(iv)).nd = ...
             grids(dom_ms(iv)).ivl2ivn(num_ms(iv)).nd + length(dom_ss);
           grids(dom_ms(iv)).ivl2ivn(num_ms(iv)).id = ...
             [grids(dom_ms(iv)).ivl2ivn(num_ms(iv)).id , dom_ss'-1];
           grids(dom_ms(iv)).ivl2ivn(num_ms(iv)).iv = ...
             [grids(dom_ms(iv)).ivl2ivn(num_ms(iv)).iv , num_ss'];
           % Periodicity must be updated recursively (one level)
           for k=1:length(dom_ss)
             for kk=1:grids(dom_ss(k)).ivl2ivn(num_ss(k)).nd
               if(grids(dom_ss(k)).ivl2ivn(num_ss(k)).id(kk)~=dom_ms(iv)-1)
                 if(isempty(find( ...
                   grids(dom_ms(iv)).ivl2ivn(num_ms(iv)).id == ...
                        grids(dom_ss(k)).ivl2ivn(num_ss(k)).id(kk) ...
                   ))) % found a new neigbour
                   grids(dom_ms(iv)).ivl2ivn(num_ms(iv)).nd = ...
                     grids(dom_ms(iv)).ivl2ivn(num_ms(iv)).nd + 1;
                   grids(dom_ms(iv)).ivl2ivn(num_ms(iv)).id(end+1) = ...
                     grids(dom_ss(k)).ivl2ivn(num_ss(k)).id(kk);
                   grids(dom_ms(iv)).ivl2ivn(num_ms(iv)).iv(end+1) = ...
                     grids(dom_ss(k)).ivl2ivn(num_ss(k)).iv(kk);
                 end
               end
             end
           end
         end
         for iv=1:length(dom_ss)
           grids(dom_ss(iv)).ivl2ivn(num_ss(iv)).nd = ...
             grids(dom_ss(iv)).ivl2ivn(num_ss(iv)).nd + length(dom_ms);
           grids(dom_ss(iv)).ivl2ivn(num_ss(iv)).id = ...
             [grids(dom_ss(iv)).ivl2ivn(num_ss(iv)).id , dom_ms'-1];
           grids(dom_ss(iv)).ivl2ivn(num_ss(iv)).iv = ...
             [grids(dom_ss(iv)).ivl2ivn(num_ss(iv)).iv , num_ms'];
           for k=1:length(dom_ms)
             for kk=1:grids(dom_ms(k)).ivl2ivn(num_ms(k)).nd
               if(grids(dom_ms(k)).ivl2ivn(num_ms(k)).id(kk)~=dom_ss(iv)-1)
                 if(isempty(find( ...
                   grids(dom_ss(iv)).ivl2ivn(num_ss(iv)).id == ...
                        grids(dom_ms(k)).ivl2ivn(num_ms(k)).id(kk) ...
                   ))) % found a new neigbour
                   grids(dom_ss(iv)).ivl2ivn(num_ss(iv)).nd = ...
                     grids(dom_ss(iv)).ivl2ivn(num_ss(iv)).nd + 1;
                   grids(dom_ss(iv)).ivl2ivn(num_ss(iv)).id(end+1) = ...
                     grids(dom_ms(k)).ivl2ivn(num_ms(k)).id(kk);
                   grids(dom_ss(iv)).ivl2ivn(num_ss(iv)).iv(end+1) = ...
                     grids(dom_ms(k)).ivl2ivn(num_ms(k)).iv(kk);
                 end
               end
             end
           end
         end

         break
       else
         if(distn<dist)
           dist = distn;
           i_nearest = vs(ivs);
         end
       end
     end
     if(~found_master)
       warning([ ...
         'Unable to find a corresponding node for iv=', ...
                                              num2str(vm(ivm)), ...
         ', belonging to boundary face ',num2str(sideper(1,i)), ...
         ', periodic with boundary face ',num2str(sideper(2,i)),...
         '; nearest node: ',num2str(i_nearest), ...
         ', distance: ',num2str(dist)]);
       p_fixed(:,i_nearest) = p(:,vm(ivm)) - sideper(3:end,i);
     end
   end
   if(~isempty(find(found_slave==0)))
     warning([ ...
     'Unable to find a corresponding node for the following nodes: ',...
     num2str(vs(find(found_slave==0))), ...
     ', belonging to boundary face ',num2str(sideper(2,i)), ...
     ', periodic with boundary face ',num2str(sideper(1,i)) ]);
   end
 end

 % partition nodal data
 if(have_noda_data)
   for isg=1:N % loop on subgrids
     for id=1:numel(node_data_names) % loop on data fields
       [~,globa_idx,local_idx] = find(iv_ren(isg,:));
       grids(isg).(node_data_names{id})(local_idx) = ...
                              node_data(globa_idx,id);
     end
   end
 end

return

