function [gamma,omega] = analyze_damped_oscillations(T,Y,t1,t2)
%
% Given a discrete time series Y, corresponding to times T, compute
% the damping coefficient and the dominating frequency. Only times
% from t1 to t2 are considered.
%
% The analysis works as follows:
% - compute all the local maxima
% - fit a damping coefficient gamma in the least square sense to the
%   local maxima
% - multiply the time series by exp(-gamma*T)
% - iterate the previous steps
% - apply a Hann window
% - compute the fft of the time series to find the dominating
%   frequency

 % 0) Select the relevant data
 i1 = min(find(T>=t1));
 i2 = max(find(T<=t2));
 T = T(i1:i2);
 Y = Y(i1:i2);
 Y0 = Y;

 dgamma = Inf;
 gamma = 0;
 k = 0;
 while(abs(dgamma)>1.0e-10)
   k = k+1;

   % 1) Find the local maxima
   delta = Y(2:end)-Y(1:end-1);
   ilocmax = find((delta(1:end-1)>=0).*(delta(2:end)<=0)) + 1;
   % Discard negative local maxima
   ilocmax = ilocmax(find(Y(ilocmax)>0));
   if(length(ilocmax)<2)
     error(["There must be at least two local maxima; found ",...
             num2str(length(ilocmax)) ]);
   endif
   Tlocmax = T(ilocmax);
   Ylocmax = Y(ilocmax);

   % 2) Least square fitting
   M = zeros(length(Tlocmax),2);
   b = zeros(length(Tlocmax),1);
   M(:,1) = 1;
   M(:,2) = Tlocmax(:);
   b(:) = log(Ylocmax(:));
   x = M\b;
   C = exp(x(1));
   dgamma = x(2);
   gamma = gamma + dgamma;
   Y = exp(-gamma*T).*Y0;

   disp(["Iteration ",num2str(k),", gamma = ",num2str(gamma)]);
 
 end

 plot(T,Y);

 % Hann window
 Y = Y - mean(Y); % eliminate the average
 Y = 0.5*(1-cos( 2*pi*T/max(T) )).*Y;

 % Determine the dominant frequency
 F = fft(Y);
 F = F(1:ceil(length(F)/2));
 [fm,im] = max(abs(F));
 omega = 2*pi/(max(T)-min(T))*(im-1);
 disp(["omega = ",num2str(omega),", frequency resolution = ", ...
       num2str( 2*pi/(max(T)-min(T)) )]);

return

