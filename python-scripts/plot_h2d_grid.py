# Copyright (C) 2009,2010,2011,2012  Marco Restelli
#
# This file is part of:
#   FEMilaro -- Finite Element Method toolkit
#
# FEMilaro is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# FEMilaro is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FEMilaro; If not, see <http://www.gnu.org/licenses/>.
#
# author: Marco Restelli                   <marco.restelli@gmail.com>

"""
Plot a scalar field defined on a hybrid 2D grid

Some remarks:

"""

import numpy as np
import scipy as sp
import proj_h2dxi2nodes
#import mayavi as mv # maybe one could use this...
#import mayavi.mlab
import matplotlib.pyplot as plt
import matplotlib.font_manager


# --- Set some plot parameters ---------------------------------------

# Fonts (family can be serif|sans-serif|cursive|fantasy|monospace
font = {'family' : 'monospace',
        'weight' : 'normal',
        'size'   : 19}
matplotlib.rc('font', **font)

# Define the axis limits
xlim = (45.0,285.0)
ylim = (-120.0,120.0)

# Countours of filled contours?
filled_contours = True

contour_opts = { \
  "levels" : np.linspace( 0.5e9 , 1.5e13 , 23 ) , \
  #"levels" : np.linspace( -0.8 , 0.05 , 23 ) , \
  "linewidths" : (2,) , \
  "zorder" : 5 \
}
quiver_opts = { \
  "scale"      : 1.0e+7, \
  #, zorder=3, color='blue',
  "width"      : 0.0007 , \
  "headwidth"  : 3.0 , \
  "headlength" : 4.0 \
}

# Show the grid?
show_grid = False

# --- XXX XXXX XXXX XXXXXXXXXX ---------------------------------------


def plot_h2d_nodalfield(GRID,BASE,F , dont_show=False):
  """
  Plot the field F defined on "GRID" with with respect to "BASE",
  where GRID and BASE are lists of class mod_h2d_grid.t_h2d_grid and
  mod_h2d_base.t_h2d_base, respectively.
  """

  plt.figure()
  plt.hold(True)
  for G,B,f in zip(GRID,BASE,F):

    # Define a small function to map elements to triangles
    el2tri = [ None , None , \
       lambda e: [ e.iv ] ,                        \
       lambda e: [ e.iv[[0,1,2]] , e.iv[[0,2,3]] ] \
      ]

    # Build the triangles
    T = np.concatenate( [ el2tri[e.poly-1](e) for e in G.e ] , 0 )

    # Nodal coordinates
    XY = np.array( [ v.x for v in G.v ] )
      
    # Set-up the plot
    plt.gca().set_aspect('equal')
    plt.xlim(xlim)
    plt.ylim(ylim)

    # scalar/vector plot
    if len( f.shape ) == 1:
      # scalar -> contour plot
      if filled_contours:
        cnt_function = plt.tricontourf
      else:
        cnt_function = plt.tricontour
      cnt_function( XY[:,0] , XY[:,1] , T , f , **contour_opts )
    else:
      # vector -> quiver plot
      plt.quiver( XY[:,0] , XY[:,1] , f[:,0] , f[:,1] , \
                  **quiver_opts )

    # Grid
    if show_grid:
      plt.triplot( XY[:,0], XY[:,1] , T ,         \
                   color='0.1',alpha=0.6,zorder=10)

  if not dont_show:
    plt.show()
  

def plot_h2d_xifield(GRID,BASE,F , key , dont_show=False):

  data = proj_h2dxi2nodes.proj_h2d_xi2nodes(GRID,BASE,F , key)
  if data[0].shape[1] == 1:
    data = [ np.reshape( d , d.size ) for d in data ]

  plot_h2d_nodalfield(GRID,BASE, data , dont_show=dont_show)

